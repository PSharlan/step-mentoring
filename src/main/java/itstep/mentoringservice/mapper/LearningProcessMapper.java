package itstep.mentoringservice.mapper;

import itstep.mentoringservice.dto.learningProcess.LearningProcessCreateDto;
import itstep.mentoringservice.dto.learningProcess.LearningProcessFullDto;
import itstep.mentoringservice.entity.LearningProcessEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = {WeeklyReportMapper.class})
public interface LearningProcessMapper {

    LearningProcessMapper LEARNING_PROCESS_MAPPER = Mappers.getMapper(LearningProcessMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "mentor", ignore = true)
    @Mapping(target = "plan", ignore = true)
    @Mapping(target = "weeklyReports", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "startDate", ignore = true)
    LearningProcessEntity toEntity(LearningProcessCreateDto createDto);

    LearningProcessFullDto toFullDto(LearningProcessEntity entity);

    List<LearningProcessFullDto> toFullDtoList(List<LearningProcessEntity> entities);
}
