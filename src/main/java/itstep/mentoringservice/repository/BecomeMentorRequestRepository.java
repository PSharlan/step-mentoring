package itstep.mentoringservice.repository;

import itstep.mentoringservice.entity.BecomeMentorRequestEntity;
import itstep.mentoringservice.entity.PlanEntity;
import itstep.mentoringservice.entity.enums.RequestStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BecomeMentorRequestRepository extends JpaRepository<BecomeMentorRequestEntity, Integer> {

    BecomeMentorRequestEntity findOneById(Integer id);

    List<BecomeMentorRequestEntity> findAllByStatus(RequestStatus requestStatus);

    @Query("SELECT r FROM BecomeMentorRequestEntity r JOIN r.plans p WHERE p.id = ?#{#planParam}")
    List<BecomeMentorRequestEntity> findAllByPlan(@Param("planParam") Integer planId);

}
