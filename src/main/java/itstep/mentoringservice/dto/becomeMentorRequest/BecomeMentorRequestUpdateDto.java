package itstep.mentoringservice.dto.becomeMentorRequest;

import io.swagger.annotations.ApiModelProperty;
import itstep.mentoringservice.entity.enums.RequestStatus;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class BecomeMentorRequestUpdateDto {

    @ApiModelProperty(example = "1", notes = "Id of the current becomeMentorRequest")
    @NotNull(message = "becomeMentorRequestId can't be null!")
    private Integer id;

    @ApiModelProperty(example = "ACCEPTED", notes = "Status of becomeMentorRequest")
    @NotNull(message = "Status can't be null")
    private RequestStatus status;

}
