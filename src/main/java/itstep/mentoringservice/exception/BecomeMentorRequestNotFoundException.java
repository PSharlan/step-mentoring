package itstep.mentoringservice.exception;

public class BecomeMentorRequestNotFoundException extends EntityIsNotFoundException{

    public BecomeMentorRequestNotFoundException(Integer id) {
        super("BecomeMentorRequest ", id);
    }
}
