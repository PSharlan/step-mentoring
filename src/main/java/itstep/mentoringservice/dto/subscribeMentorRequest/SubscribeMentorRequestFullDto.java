package itstep.mentoringservice.dto.subscribeMentorRequest;

import io.swagger.annotations.ApiModelProperty;
import itstep.mentoringservice.dto.mentor.MentorFullDto;
import itstep.mentoringservice.dto.plan.PlanFullDto;
import itstep.mentoringservice.entity.enums.RequestStatus;
import lombok.Data;

@Data
public class SubscribeMentorRequestFullDto {

    @ApiModelProperty(example = "1", notes = "Id of the current subscribeMentorRequest")
    private Integer id;

    @ApiModelProperty(example = "ACCEPTED", notes = "Status of subscribeMentorRequest")
    private RequestStatus status;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    private Integer userId;

    @ApiModelProperty(notes = "Plan for subscribeMentorRequest")
    private PlanFullDto plan;

}
