package itstep.mentoringservice.dto.learningProcess;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class LearningProcessCreateDto {

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    @NotNull(message = "mentorId can't be null!")
    private Integer mentorId;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    @NotNull(message = "planId can't be null!")
    private Integer planId;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    @NotNull(message = "userId can't be null!")
    private Integer userId;

}
