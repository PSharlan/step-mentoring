package itstep.mentoringservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import itstep.mentoringservice.dto.learningProcess.LearningProcessCreateDto;
import itstep.mentoringservice.dto.learningProcess.LearningProcessFullDto;
import itstep.mentoringservice.dto.learningProcess.LearningProcessUpdateDto;
import itstep.mentoringservice.entity.LearningProcessEntity;
import itstep.mentoringservice.entity.MentorEntity;
import itstep.mentoringservice.entity.PlanEntity;
import itstep.mentoringservice.entity.enums.LearningProcessStatus;
import itstep.mentoringservice.integration.AbstractIntegrationTest;
import itstep.mentoringservice.mapper.MapperGenerationUtils;
import itstep.mentoringservice.repository.LearningProcessRepository;
import itstep.mentoringservice.repository.MentorRepository;
import itstep.mentoringservice.repository.PlanRepository;
import itstep.mentoringservice.service.LearningProcessService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static itstep.mentoringservice.EntityGenerationUtils.*;
import static itstep.mentoringservice.EntityGenerationUtils.generatePlan;
import static itstep.mentoringservice.entity.enums.LearningProcessStatus.PROCESS;
import static itstep.mentoringservice.mapper.MapperGenerationUtils.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class LearningProcessControllerTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() throws Exception {
        // given
        LearningProcessEntity saved = learningProcessRepository.save(generateLearningProcessWithMentorAndPlan());

        // when
        MvcResult result = mockMvc.perform(get("/learningProcesses/{id}", saved.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        LearningProcessFullDto foundDto = objectMapper.readValue(bytes, LearningProcessFullDto.class);

        // then
        Assertions.assertNotNull(foundDto);
        Assertions.assertNotNull(foundDto.getId());
        Assertions.assertEquals(saved.getId(), foundDto.getId());
        Assertions.assertEquals(saved.getStatus(), foundDto.getStatus());
        Assertions.assertEquals(saved.getUserId(), foundDto.getUserId());
        Assertions.assertEquals(saved.getPlan().getId(), foundDto.getPlan().getId());
    }

    @Test
    void findById_whenNotFound() throws Exception {
        // given
        Integer notExistingId = 10000;

        // when
        mockMvc.perform(get("/learningProcesses/{id}", notExistingId))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        // then
    }

    @Test
    void findAll_happyPath() throws Exception {
        // given
        List<LearningProcessEntity> saved = learningProcessRepository.saveAll(generateLearningProcessesWithMentorAndPlan());

        // when
        mockMvc.perform(get("/learningProcesses?page=0&size=3"))
                .andExpect(status().isOk())
                .andReturn();

        // then
    }

    @Test
    void findAllByStatus_happyPath() throws Exception {
        // given
        List<LearningProcessEntity> toSave = generateLearningProcessesWithMentorAndPlan();
        toSave.get(0).setStatus(PROCESS);

        List<LearningProcessEntity> saved = learningProcessRepository.saveAll(toSave);

        // when
        mockMvc.perform(get("/learningProcesses/status/PROCESS"))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    void create_happyPath() throws Exception {
        // given
        LearningProcessCreateDto createDto = generateLearningProcessCreateDtoWithMentorAndPlan();

        // when
        MvcResult result = mockMvc.perform(post("/learningProcesses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        LearningProcessFullDto saved = objectMapper.readValue(bytes, LearningProcessFullDto.class);

        // then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
        Assertions.assertEquals(saved.getPlan().getId(), createDto.getPlanId());
        Assertions.assertEquals(saved.getUserId(), createDto.getUserId());
    }

    @Test
    void update_happyPath() throws Exception {
        // given
        LearningProcessEntity saved = learningProcessRepository.save(generateLearningProcessWithMentorAndPlan());

        LearningProcessUpdateDto update = generateLearningProcessUpdateDto();
        update.setId(saved.getId());

        // when
        MvcResult result = mockMvc.perform(put("/learningProcesses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(update)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        LearningProcessFullDto updated = objectMapper.readValue(bytes, LearningProcessFullDto.class);

        // then
        Assertions.assertNotNull(updated);
        Assertions.assertEquals(saved.getId(), updated.getId());
        Assertions.assertEquals(saved.getPlan().getId(), updated.getPlan().getId());
        Assertions.assertNotEquals(saved.getStatus(), updated.getStatus());
    }

    @Test
    void update_whenNotFound() throws Exception {
        // given
        LearningProcessUpdateDto updateDto = generateLearningProcessUpdateDto();
        updateDto.setId(0);

        // when
        mockMvc.perform(put("/learningProcesses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isNotFound());

        // then
    }

    @Test
    void delete_happyPath() throws Exception {
        // given
        LearningProcessEntity saved = learningProcessRepository.save(generateLearningProcessWithMentorAndPlan());

        // when
        mockMvc.perform(delete("/learningProcesses/{id}", saved.getId()))
                .andExpect(status().isOk());

        // then
        mockMvc.perform(get("/learningProcesses/{id}", saved.getId()))
                .andExpect(status().isNotFound());
    }

    @Test
    void delete_whenNotFound() throws Exception {
        // given
        Integer notExistingId = 0;

        // when
        mockMvc.perform(delete("/learningProcesses/{id}", notExistingId))
                .andExpect(status().isNotFound());

        // then
    }

    @Test
    void create_whenMentorIdNull() throws Exception {
        // given
        LearningProcessCreateDto createDto = generateLearningProcessCreateDtoWithMentorAndPlan();
        createDto.setMentorId(null);

        // when
        mockMvc.perform(post("/learningProcesses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    @Test
    void create_whenPlanIdNull() throws Exception {
        // given
        LearningProcessCreateDto createDto = generateLearningProcessCreateDtoWithMentorAndPlan();
        createDto.setPlanId(null);

        // when
        mockMvc.perform(post("/learningProcesses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    @Test
    void create_whenUserIdNull() throws Exception {
        // given
        LearningProcessCreateDto createDto = generateLearningProcessCreateDtoWithMentorAndPlan();
        createDto.setUserId(null);

        // when
        mockMvc.perform(post("/learningProcesses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    @Test
    void update_whenStatusNull() throws Exception {
        // given
        LearningProcessEntity saved = learningProcessRepository.save(generateLearningProcessWithMentorAndPlan());

        LearningProcessUpdateDto updateDto = generateLearningProcessUpdateDto();
        updateDto.setId(saved.getId());
        updateDto.setStatus(null);

        // when
        mockMvc.perform(put("/learningProcesses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    private LearningProcessEntity generateLearningProcessWithMentorAndPlan() {
        LearningProcessEntity entity = generateLearningProcess();
        entity.setMentor(mentorRepository.save(generateMentor()));
        entity.setPlan(planRepository.save(generatePlan()));

        return entity;
    }

    private List<LearningProcessEntity> generateLearningProcessesWithMentorAndPlan() {
        List<LearningProcessEntity> learningProcesses = generateLearningProcesses();

        MentorEntity savedMentor = mentorRepository.save(generateMentor());
        PlanEntity savedPlan = planRepository.save(generatePlan());

        for(LearningProcessEntity learningProcess : learningProcesses){
            learningProcess.setMentor(savedMentor);
            learningProcess.setPlan(savedPlan);
        }

        return learningProcesses;
    }

    private LearningProcessCreateDto generateLearningProcessCreateDtoWithMentorAndPlan() {
        LearningProcessCreateDto createDto = new LearningProcessCreateDto();

        createDto.setUserId(1);
        createDto.setMentorId(mentorRepository.save(generateMentor()).getId());
        createDto.setPlanId(planRepository.save(generatePlan()).getId());

        return createDto;
    }
}
