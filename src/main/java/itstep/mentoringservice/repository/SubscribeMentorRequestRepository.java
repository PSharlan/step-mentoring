package itstep.mentoringservice.repository;

import itstep.mentoringservice.entity.PlanEntity;
import itstep.mentoringservice.entity.SubscribeMentorRequestEntity;
import itstep.mentoringservice.entity.enums.RequestStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubscribeMentorRequestRepository extends JpaRepository<SubscribeMentorRequestEntity, Integer> {

    SubscribeMentorRequestEntity findOneById(Integer id);

    List<SubscribeMentorRequestEntity> findAllByStatus(RequestStatus status);
}
