package itstep.mentoringservice.service;

import itstep.mentoringservice.dto.plan.PlanCreateDto;
import itstep.mentoringservice.dto.plan.PlanFullDto;
import itstep.mentoringservice.dto.plan.PlanUpdateDto;
import org.springframework.data.domain.Page;

import java.util.List;

public interface PlanService {

    PlanFullDto findById(Integer id);

    Page<PlanFullDto> findAll(Integer page, Integer size);

    PlanFullDto create(PlanCreateDto createDto);

    PlanFullDto update(PlanUpdateDto updateDto);

    void deleteById(Integer id);

}
