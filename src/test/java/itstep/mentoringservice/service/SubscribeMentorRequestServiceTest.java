package itstep.mentoringservice.service;

import itstep.mentoringservice.dto.subscribeMentorRequest.SubscribeMentorRequestCreateDto;
import itstep.mentoringservice.dto.subscribeMentorRequest.SubscribeMentorRequestFullDto;
import itstep.mentoringservice.dto.subscribeMentorRequest.SubscribeMentorRequestUpdateDto;
import itstep.mentoringservice.entity.LearningProcessEntity;
import itstep.mentoringservice.entity.MentorEntity;
import itstep.mentoringservice.entity.PlanEntity;
import itstep.mentoringservice.entity.SubscribeMentorRequestEntity;
import itstep.mentoringservice.entity.enums.RequestStatus;
import itstep.mentoringservice.exception.MentorNotFoundException;
import itstep.mentoringservice.exception.PlanNotFoundException;
import itstep.mentoringservice.exception.SubscribeMentorRequestNotFoundException;
import itstep.mentoringservice.integration.AbstractIntegrationTest;
import itstep.mentoringservice.repository.MentorRepository;
import itstep.mentoringservice.repository.PlanRepository;
import itstep.mentoringservice.repository.SubscribeMentorRequestRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;

import java.util.List;

import static itstep.mentoringservice.EntityGenerationUtils.*;
import static itstep.mentoringservice.mapper.MapperGenerationUtils.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SubscribeMentorRequestServiceTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() {
        // given
        SubscribeMentorRequestEntity entity = generateSubscribeMentorRequestWithMentorAndPlan();

        SubscribeMentorRequestEntity saved = subscribeMentorRequestRepository.save(entity);

        // when
        SubscribeMentorRequestFullDto fullDto = subscribeMentorRequestService.findById(saved.getId());

        // then
        Assertions.assertNotNull(fullDto);
        Assertions.assertNotNull(fullDto.getStatus());
        Assertions.assertNotNull(fullDto.getPlan());
        Assertions.assertEquals(fullDto.getStatus(), RequestStatus.PENDING);
        Assertions.assertEquals(saved.getId(), fullDto.getId());
        Assertions.assertEquals(saved.getUserId(), fullDto.getUserId());
    }

    @Test
    void findAll_happyPath() {
        // given
        List<SubscribeMentorRequestEntity> toSave = generateSubscribeMentorRequestsWithMentorAndPlan();

        List<SubscribeMentorRequestEntity> saved = subscribeMentorRequestRepository.saveAll(toSave);

        // when
        Page<SubscribeMentorRequestFullDto> foundPage = subscribeMentorRequestService.findAll(0, 10);
        List<SubscribeMentorRequestFullDto> subscribeMentorRequests = foundPage.getContent();

        // then
        Assertions.assertNotNull(foundPage);
        Assertions.assertEquals(saved.size(), subscribeMentorRequests.size());
    }

    @Test
    void create_happyPath() {
        // given
        SubscribeMentorRequestCreateDto createDto = generateSubscribeMentorRequestCreateDtoWithMentorAndPlan();

        // when
        SubscribeMentorRequestFullDto saved = subscribeMentorRequestService.create(createDto);

        // then
        Assertions.assertNotNull(saved.getId());
        Assertions.assertNotNull(saved.getStatus());
        Assertions.assertEquals(saved.getUserId(), createDto.getUserId());
        Assertions.assertEquals(saved.getPlan().getId(), createDto.getPlanId());
        Assertions.assertEquals(saved.getStatus(), RequestStatus.PENDING);
    }

    @Test
    void update_happyPath() {
        // given
        SubscribeMentorRequestCreateDto createDto = generateSubscribeMentorRequestCreateDtoWithMentorAndPlan();

        SubscribeMentorRequestFullDto saved = subscribeMentorRequestService.create(createDto);

        SubscribeMentorRequestUpdateDto updateDto = generateSubscribeMentorRequestUpdateDto();

        updateDto.setId(saved.getId());
        updateDto.setMentorId(mentorRepository.save(generateMentor()).getId());

        // when
        SubscribeMentorRequestFullDto updated = subscribeMentorRequestService.update(updateDto);

        // then
        Assertions.assertNotNull(updated);
        Assertions.assertEquals(saved.getId(), updated.getId());
        Assertions.assertEquals(saved.getPlan(), updated.getPlan());
        Assertions.assertEquals(saved.getUserId(), updated.getUserId());
        Assertions.assertNotEquals(saved.getStatus(), updated.getStatus());
    }

    @Test
    void deleteById_happyPath() {
        // given
        SubscribeMentorRequestFullDto saved = subscribeMentorRequestService.create(generateSubscribeMentorRequestCreateDtoWithMentorAndPlan());

        // when
        subscribeMentorRequestService.deleteById(saved.getId());

        Exception exception = assertThrows(SubscribeMentorRequestNotFoundException.class,
                () -> subscribeMentorRequestService.findById(saved.getId()));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(saved.getId())));
    }

    @Test
    void findById_whenNotFound() {
        // given
        Integer notExistingId = 100;

        // when
        Exception exception = assertThrows(SubscribeMentorRequestNotFoundException.class,
                () -> subscribeMentorRequestService.findById(notExistingId));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void create_whenMentorNotFound() {
        // given
        SubscribeMentorRequestCreateDto createDto = generateSubscribeMentorRequestCreateDtoWithMentorAndPlan();

        createDto.setMentorId(0);

        // when
        Exception exception = assertThrows(MentorNotFoundException.class,
                () -> subscribeMentorRequestService.create(createDto));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(createDto.getMentorId())));
    }

    @Test
    void create_whenPlanNotFound() {
        // given
        SubscribeMentorRequestCreateDto createDto = generateSubscribeMentorRequestCreateDtoWithMentorAndPlan();

        createDto.setPlanId(0);

        // when
        Exception exception = assertThrows(PlanNotFoundException.class,
                () -> subscribeMentorRequestService.create(createDto));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(createDto.getPlanId())));
    }

    @Test
    void update_whenNotFound() {
        // given
        SubscribeMentorRequestUpdateDto updateDto = generateSubscribeMentorRequestUpdateDto();

        Integer notExistingId = 100;
        updateDto.setId(notExistingId);

        // when
        Exception exception = Assertions.assertThrows(SubscribeMentorRequestNotFoundException.class,
                () -> subscribeMentorRequestService.update(updateDto));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(updateDto.getId())));
    }

    @Test
    void delete_whenNotFound() {
        // given
        Integer notExistingId = 100;

        // when
        Exception exception = Assertions.assertThrows(SubscribeMentorRequestNotFoundException.class,
                () -> subscribeMentorRequestService.deleteById(notExistingId));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    private SubscribeMentorRequestEntity generateSubscribeMentorRequestWithMentorAndPlan() {
        SubscribeMentorRequestEntity subscribeMentorRequest = generateSubscribeMentorRequest();

        subscribeMentorRequest.setMentor(mentorRepository.save(generateMentor()));
        subscribeMentorRequest.setPlan(planRepository.save(generatePlan()));

        return subscribeMentorRequest;
    }

    private List<SubscribeMentorRequestEntity> generateSubscribeMentorRequestsWithMentorAndPlan() {
        List<SubscribeMentorRequestEntity> subscribeMentorRequests = generateSubscribeMentorRequests();

        MentorEntity savedMentor = mentorRepository.save(generateMentor());
        PlanEntity savedPlan = planRepository.save(generatePlan());

        for (SubscribeMentorRequestEntity subscribeMentorRequest : subscribeMentorRequests) {
            subscribeMentorRequest.setMentor(savedMentor);
            subscribeMentorRequest.setPlan(savedPlan);
        }

        return subscribeMentorRequests;
    }

    private SubscribeMentorRequestCreateDto generateSubscribeMentorRequestCreateDtoWithMentorAndPlan() {
        SubscribeMentorRequestCreateDto createDto = generateSubscribeMentorRequestCreateDto();
        createDto.setMentorId(mentorRepository.save(generateMentor()).getId());
        createDto.setPlanId(planRepository.save(generatePlan()).getId());

        return createDto;
    }

}
