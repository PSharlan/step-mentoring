package itstep.mentoringservice.dto.learningProcess;

import io.swagger.annotations.ApiModelProperty;
import itstep.mentoringservice.entity.enums.LearningProcessStatus;
import itstep.mentoringservice.entity.enums.RequestStatus;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class LearningProcessUpdateDto {

    @ApiModelProperty(example = "1", notes = "Id of the current learningProcess")
    @NotNull(message = "learningProcessId can't be null!")
    private Integer id;

    @ApiModelProperty(example = "PROCESS", notes = "Status of learningProcess")
    @NotNull(message = "Status can't be null!")
    private LearningProcessStatus status;

}
