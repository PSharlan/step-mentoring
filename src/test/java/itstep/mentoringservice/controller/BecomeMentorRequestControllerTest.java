package itstep.mentoringservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import itstep.mentoringservice.EntityGenerationUtils;
import itstep.mentoringservice.dto.becomeMentorRequest.BecomeMentorRequestCreateDto;
import itstep.mentoringservice.dto.becomeMentorRequest.BecomeMentorRequestFullDto;
import itstep.mentoringservice.dto.becomeMentorRequest.BecomeMentorRequestUpdateDto;
import itstep.mentoringservice.entity.BecomeMentorRequestEntity;
import itstep.mentoringservice.entity.PlanEntity;
import itstep.mentoringservice.entity.enums.RequestStatus;
import itstep.mentoringservice.integration.AbstractIntegrationTest;
import itstep.mentoringservice.mapper.MapperGenerationUtils;
import itstep.mentoringservice.repository.BecomeMentorRequestRepository;
import itstep.mentoringservice.repository.PlanRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static itstep.mentoringservice.EntityGenerationUtils.*;
import static itstep.mentoringservice.entity.enums.RequestStatus.*;
import static itstep.mentoringservice.mapper.MapperGenerationUtils.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class BecomeMentorRequestControllerTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() throws Exception {
        // given
        BecomeMentorRequestEntity saved = becomeMentorRequestRepository.save(generateBecomeMentorRequest());

        // when
        MvcResult result = mockMvc.perform(get("/becomeMentorRequests/{id}", saved.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        BecomeMentorRequestFullDto foundDto = objectMapper.readValue(bytes, BecomeMentorRequestFullDto.class);

        // then
        Assertions.assertNotNull(foundDto);
        Assertions.assertNotNull(foundDto.getId());
        Assertions.assertEquals(saved.getId(), foundDto.getId());
        Assertions.assertEquals(saved.getStatus(), foundDto.getStatus());
        Assertions.assertEquals(saved.getUserId(), foundDto.getUserId());
    }

    @Test
    void findById_whenNotFound() throws Exception {
        // given
        Integer notExistingId = 10000;

        // when
        mockMvc.perform(get("/becomeMentorRequests/{id}", notExistingId))
                .andExpect(status().isNotFound());

        // then
    }

    @Test
    void findAll_happyPath() throws Exception {
        // given
        List<BecomeMentorRequestEntity> saved = becomeMentorRequestRepository.saveAll(generateBecomeMentorRequests());

        // when
        mockMvc.perform(get("/becomeMentorRequests?page=0&size=10"))
                .andExpect(status().isOk())
                .andReturn();

        // then
    }

    @Test
    void findAllByStatus_happyPath() throws Exception {
        // given
        List<BecomeMentorRequestEntity> toSave = generateBecomeMentorRequests();
        toSave.get(0).setStatus(ACCEPTED);

        List<BecomeMentorRequestEntity> saved = becomeMentorRequestRepository.saveAll(toSave);

        // when
        mockMvc.perform(get("/becomeMentorRequests/status/ACCEPTED"))
                .andExpect(status().isOk())
                .andReturn();

        // then
    }

    @Test
    void findAllByPlanId_happyPath() throws Exception {
        // given
        List<BecomeMentorRequestEntity> saved = becomeMentorRequestRepository.saveAll(generateBecomeMentorRequestWithPlans());

        // when
        mockMvc.perform(get("/becomeMentorRequests/plans/1"))
                .andExpect(status().isOk())
                .andReturn();

        // then
    }

    @Test
    void create_happyPath() throws Exception {
        // given
        BecomeMentorRequestCreateDto createDto = generateBecomeMentorRequestDtoWithPlans();

        // when
        MvcResult result = mockMvc.perform(post("/becomeMentorRequests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        BecomeMentorRequestFullDto saved = objectMapper.readValue(bytes, BecomeMentorRequestFullDto.class);

        // then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
        Assertions.assertEquals(saved.getUserId(), createDto.getUserId());
    }

    @Test
    void update_happyPath() throws Exception {
        // given
        BecomeMentorRequestEntity saved = becomeMentorRequestRepository.save(generateBecomeMentorRequest());

        BecomeMentorRequestUpdateDto update = generateBecomeMentorRequestUpdateDto();
        update.setId(saved.getId());

        // when
        MvcResult result = mockMvc.perform(put("/becomeMentorRequests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(update)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        BecomeMentorRequestFullDto updated = objectMapper.readValue(bytes, BecomeMentorRequestFullDto.class);

        // then
        Assertions.assertNotNull(updated);
        Assertions.assertEquals(saved.getId(), updated.getId());
        Assertions.assertEquals(saved.getUserId(), updated.getUserId());
        Assertions.assertNotEquals(saved.getStatus(), updated.getStatus());
    }

    @Test
    void update_whenNotFound() throws Exception {
        // given
        BecomeMentorRequestUpdateDto updateDto = generateBecomeMentorRequestUpdateDto();
        updateDto.setId(0);

        // when
        mockMvc.perform(put("/becomeMentorRequests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isNotFound());

        // then
    }

    @Test
    void delete_happyPath() throws Exception {
        // given
        BecomeMentorRequestEntity saved = becomeMentorRequestRepository.save(generateBecomeMentorRequest());

        // when
        mockMvc.perform(delete("/becomeMentorRequests/{id}", saved.getId()))
                .andExpect(status().isOk());

        // then
        mockMvc.perform(get("/becomeMentorRequests/{id}", saved.getId()))
                .andExpect(status().isNotFound());
    }

    @Test
    void delete_whenNotFound() throws Exception {
        // given
        Integer notExistingId = 0;

        // when
        mockMvc.perform(delete("/becomeMentorRequests/{id}", notExistingId))
                .andExpect(status().isNotFound());

        // then
    }

    @Test
    void create_whenPlanIdsNull() throws Exception {
        // given
        BecomeMentorRequestCreateDto createDto = generateBecomeMentorRequestCreateDto();
        createDto.setPlanIds(null);

        // when
        mockMvc.perform(post("/becomeMentorRequests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    @Test
    void create_whenUserIdNull() throws Exception {
        // given
        BecomeMentorRequestCreateDto createDto = generateBecomeMentorRequestDtoWithPlans();
        createDto.setUserId(null);

        // when
        mockMvc.perform(post("/becomeMentorRequests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    @Test
    void update_whenStatusNull() throws Exception {
        // given
        BecomeMentorRequestEntity saved = becomeMentorRequestRepository.save(generateBecomeMentorRequest());

        BecomeMentorRequestUpdateDto updateDto = generateBecomeMentorRequestUpdateDto();
        updateDto.setId(saved.getId());
        updateDto.setStatus(null);

        // when
        mockMvc.perform(put("/becomeMentorRequests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    private List<BecomeMentorRequestEntity> generateBecomeMentorRequestWithPlans() {
        List<BecomeMentorRequestEntity> becomeMentorRequests = generateBecomeMentorRequests();

        List<PlanEntity> plans = planRepository.saveAll(generatePlans());

        for (BecomeMentorRequestEntity entity : becomeMentorRequests) {
            entity.setPlans(plans);
        }

        return becomeMentorRequests;
    }

    private BecomeMentorRequestCreateDto generateBecomeMentorRequestDtoWithPlans() {
        BecomeMentorRequestCreateDto createDto = generateBecomeMentorRequestCreateDto();

        List<PlanEntity> plans = planRepository.saveAll(generatePlans());

        for (PlanEntity entity : plans) {
            List<Integer> plansIds = new ArrayList<>();
            plansIds.add(entity.getId());
            createDto.setPlanIds(plansIds);
        }
        return createDto;
    }
}
