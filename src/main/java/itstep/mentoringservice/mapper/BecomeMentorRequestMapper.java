package itstep.mentoringservice.mapper;

import itstep.mentoringservice.dto.becomeMentorRequest.BecomeMentorRequestCreateDto;
import itstep.mentoringservice.dto.becomeMentorRequest.BecomeMentorRequestFullDto;
import itstep.mentoringservice.entity.BecomeMentorRequestEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = PlanMapper.class)
public interface BecomeMentorRequestMapper {

    BecomeMentorRequestMapper BECOME_MENTOR_REQUEST_MAPPER = Mappers.getMapper(BecomeMentorRequestMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "plans", ignore = true)
    @Mapping(target = "status", ignore = true)
    BecomeMentorRequestEntity toEntity(BecomeMentorRequestCreateDto createDto);

    BecomeMentorRequestFullDto toFullDto(BecomeMentorRequestEntity entity);

    List<BecomeMentorRequestFullDto> toFullDtoList(List<BecomeMentorRequestEntity> entities);

}
