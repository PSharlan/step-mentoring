package itstep.mentoringservice.service.impl;

import itstep.mentoringservice.dto.subscribeMentorRequest.SubscribeMentorRequestCreateDto;
import itstep.mentoringservice.dto.subscribeMentorRequest.SubscribeMentorRequestFullDto;
import itstep.mentoringservice.dto.subscribeMentorRequest.SubscribeMentorRequestUpdateDto;
import itstep.mentoringservice.entity.MentorEntity;
import itstep.mentoringservice.entity.PlanEntity;
import itstep.mentoringservice.entity.SubscribeMentorRequestEntity;
import itstep.mentoringservice.entity.enums.RequestStatus;
import itstep.mentoringservice.exception.MentorNotFoundException;
import itstep.mentoringservice.exception.PlanNotFoundException;
import itstep.mentoringservice.exception.SubscribeMentorRequestNotFoundException;
import itstep.mentoringservice.repository.MentorRepository;
import itstep.mentoringservice.repository.PlanRepository;
import itstep.mentoringservice.repository.SubscribeMentorRequestRepository;
import itstep.mentoringservice.service.SubscribeMentorRequestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static itstep.mentoringservice.entity.enums.RequestStatus.PENDING;
import static itstep.mentoringservice.mapper.SubscribeMentorRequestMapper.SUBSCRIBE_MENTOR_REQUEST_MAPPER;

@Slf4j
@Service
public class SubscribeMentorRequestServiceImpl implements SubscribeMentorRequestService {

    @Autowired
    private SubscribeMentorRequestRepository subscribeMentorRequestRepository;

    @Autowired
    private MentorRepository mentorRepository;

    @Autowired
    private PlanRepository planRepository;

    @Override
    @Transactional(readOnly = true)
    public SubscribeMentorRequestFullDto findById(Integer id) {
        SubscribeMentorRequestEntity foundEntity = subscribeMentorRequestRepository.findById(id)
                .orElseThrow(() -> new SubscribeMentorRequestNotFoundException(id));

        log.info("SubscribeMentorRequestServiceImpl -> found subscribeMentorRequest {} by id: {}", foundEntity, id);
        return SUBSCRIBE_MENTOR_REQUEST_MAPPER.toFullDto(foundEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<SubscribeMentorRequestFullDto> findAll(Integer page, Integer size) {
        Page<SubscribeMentorRequestEntity> foundEntities = subscribeMentorRequestRepository.findAll(PageRequest.of(page, size));

        Page<SubscribeMentorRequestFullDto> dto = foundEntities.map(entity -> SUBSCRIBE_MENTOR_REQUEST_MAPPER.toFullDto(entity));

        log.info("SubscribeMentorRequestServiceImpl -> found {} subscribeMentorRequests", dto.getNumberOfElements());
        return dto;
    }

    @Override
    @Transactional
    public List<SubscribeMentorRequestFullDto> findAllByStatus(RequestStatus status) {
        List<SubscribeMentorRequestEntity> foundEntities = subscribeMentorRequestRepository.findAllByStatus(status);

        log.info("SubscribeMentorRequestServiceImpl -> found {} subscribeMentorRequests by status", foundEntities.size());
        return SUBSCRIBE_MENTOR_REQUEST_MAPPER.toFullDtoList(foundEntities);
    }

    @Override
    @Transactional
    public SubscribeMentorRequestFullDto create(SubscribeMentorRequestCreateDto createDto) {
        SubscribeMentorRequestEntity entityToSave = SUBSCRIBE_MENTOR_REQUEST_MAPPER.toEntity(createDto);

        if (createDto.getMentorId() != null) {
            MentorEntity mentor = mentorRepository.findById(createDto.getMentorId())
                    .orElseThrow(() -> new MentorNotFoundException(createDto.getMentorId()));
            entityToSave.setMentor(mentor);
        }

        PlanEntity plan = planRepository.findById(createDto.getPlanId())
                .orElseThrow(() -> new PlanNotFoundException(createDto.getPlanId()));

        entityToSave.setPlan(plan);
        entityToSave.setStatus(PENDING);

        SubscribeMentorRequestEntity savedEntity = subscribeMentorRequestRepository.save(entityToSave);

        log.info("SubscribeMentorRequestServiceImpl -> subscribeMentorRequest {} successfully saved", savedEntity);
        return SUBSCRIBE_MENTOR_REQUEST_MAPPER.toFullDto(savedEntity);
    }

    @Override
    @Transactional
    public SubscribeMentorRequestFullDto update(SubscribeMentorRequestUpdateDto updateDto) {
        SubscribeMentorRequestEntity entityToUpdate = subscribeMentorRequestRepository.findById(updateDto.getId())
                .orElseThrow(() -> new SubscribeMentorRequestNotFoundException(updateDto.getId()));

        MentorEntity mentor = mentorRepository.findById(updateDto.getMentorId())
                .orElseThrow(() -> new MentorNotFoundException(updateDto.getMentorId()));

        entityToUpdate.setMentor(mentor);
        entityToUpdate.setStatus(updateDto.getStatus());

        SubscribeMentorRequestEntity updatedEntity = subscribeMentorRequestRepository.save(entityToUpdate);

        log.info("SubscribeMentorRequestServiceImpl -> subscribeMentorRequest {} successfully updated", updatedEntity);
        return SUBSCRIBE_MENTOR_REQUEST_MAPPER.toFullDto(updatedEntity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        subscribeMentorRequestRepository.findById(id)
                .orElseThrow(() -> new SubscribeMentorRequestNotFoundException(id));

        subscribeMentorRequestRepository.deleteById(id);
    }
}
