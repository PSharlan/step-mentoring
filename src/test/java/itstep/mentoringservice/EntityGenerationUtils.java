package itstep.mentoringservice;

import itstep.mentoringservice.entity.*;
import itstep.mentoringservice.entity.enums.RequestStatus;
import itstep.mentoringservice.repository.MentorRepository;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class EntityGenerationUtils {

    public static <T extends Enum<?>> T randomEnum(Class<T> enumClass){
        int index = new Random().nextInt(enumClass.getEnumConstants().length);
        return enumClass.getEnumConstants()[index];
    }

    public static BecomeMentorRequestEntity generateBecomeMentorRequest() {
        BecomeMentorRequestEntity entity = new BecomeMentorRequestEntity();
        entity.setStatus(RequestStatus.PENDING);
        entity.setUserId(1);

        return entity;
    }

    public static List<BecomeMentorRequestEntity> generateBecomeMentorRequests() {
        List<BecomeMentorRequestEntity> becomeMentorRequests = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            BecomeMentorRequestEntity entity = new BecomeMentorRequestEntity();
            entity.setStatus(randomEnum(RequestStatus.class));
            entity.setUserId(i+1);
            becomeMentorRequests.add(entity);
        }
        return becomeMentorRequests;
    }


    public static PlanEntity generatePlan() {
        PlanEntity plan = new PlanEntity();
        plan.setDescription("desc");
        plan.setEstimatedHours(1);
        plan.setFileUrl("url");
        return plan;
    }

    public static List<PlanEntity> generatePlans() {
        PlanEntity plan1 = new PlanEntity();
        plan1.setDescription("qwe");
        plan1.setEstimatedHours(123);
        plan1.setFileUrl("zxc");

        PlanEntity plan2 = new PlanEntity();
        plan2.setDescription("asd");
        plan2.setEstimatedHours(321);
        plan2.setFileUrl("qwe");

        PlanEntity plan3 = new PlanEntity();
        plan3.setDescription("qwerty");
        plan3.setEstimatedHours(1234);
        plan3.setFileUrl("zxc");

        return new ArrayList<>(Arrays.asList(plan1, plan2, plan3));
    }

    public static LearningProcessEntity generateLearningProcess(){
        LearningProcessEntity entity = new LearningProcessEntity();
        entity.setUserId((int)(Math.random() * 100000));

        return entity;
    }

    public static List<LearningProcessEntity> generateLearningProcesses(){
        LearningProcessEntity process1 = new LearningProcessEntity();
        process1.setUserId((int)(Math.random() * 100000));

        LearningProcessEntity process2 = new LearningProcessEntity();
        process2.setUserId((int)(Math.random() * 100000));

        LearningProcessEntity process3 = new LearningProcessEntity();
        process3.setUserId((int)(Math.random() * 100000));

        return  new ArrayList<>(Arrays.asList(process1, process2,  process3));
    }

    public static MentorEntity generateMentor(){
        MentorEntity entity = new MentorEntity();
        entity.setUserId((int)(Math.random() * 100));
        return entity;
    }

    public static List<MentorEntity> generateMentors(){
        MentorEntity entity1 = new MentorEntity();
        entity1.setUserId((int)(Math.random() * 100000));

        MentorEntity entity2 = new MentorEntity();
        entity2.setUserId((int)(Math.random() * 100000));

        MentorEntity entity3 = new MentorEntity();
        entity3.setUserId((int)(Math.random() * 100000));

        return new ArrayList<>(Arrays.asList(entity1, entity2, entity3));
    }


    public static List<WeeklyReportEntity> generateWeeklyReports(){
        WeeklyReportEntity report1 = new WeeklyReportEntity();

        report1.setCreateTime(Instant.now());
        report1.setCommentForMentor("GODD");
        report1.setCommentForStudent("BAD");
        report1.setMarkForMentor(8);
        report1.setMarkForStudent(4);

        WeeklyReportEntity report2 = new WeeklyReportEntity();

        report2.setCreateTime(Instant.now());
        report2.setCommentForMentor("BAD");
        report2.setCommentForStudent("COOL");
        report2.setMarkForMentor(5);
        report2.setMarkForStudent(10);

        return new ArrayList<>(Arrays.asList(report1, report2));
    }

    public static SubscribeMentorRequestEntity generateSubscribeMentorRequest(){
        SubscribeMentorRequestEntity request = new SubscribeMentorRequestEntity();
        request.setUserId((int)(Math.random() * 100000));
        request.setStatus(RequestStatus.PENDING);
        return  request;
    }

    public static List<SubscribeMentorRequestEntity> generateSubscribeMentorRequests(){
        List<SubscribeMentorRequestEntity> requests = new ArrayList<>();
        for(int i = 0; i < 10; ++i){
            SubscribeMentorRequestEntity request = new SubscribeMentorRequestEntity();
            request.setStatus(randomEnum(RequestStatus.class));
            request.setUserId(i + 1);
            requests.add(request);
        }
        return requests;
    }
    
    public static WeeklyReportEntity generateWeeklyReport(){
        WeeklyReportEntity report = new WeeklyReportEntity();
        report.setCreateTime(Instant.now());
        report.setMarkForStudent(9);
        report.setMarkForMentor(9);
        report.setCommentForStudent("NOT BAD");
        report.setCommentForMentor("COOL");
        return report;
    }

}
