package itstep.mentoringservice.mapper;

import itstep.mentoringservice.dto.mentor.MentorCreateDto;
import itstep.mentoringservice.dto.mentor.MentorFullDto;
import itstep.mentoringservice.entity.MentorEntity;
import itstep.mentoringservice.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;

import static itstep.mentoringservice.mapper.MapperGenerationUtils.*;
import static itstep.mentoringservice.mapper.MentorMapper.MENTOR_MAPPER;

public class MentorMapperTest extends AbstractIntegrationTest {

    @Test
    void mapToEntity_happyPath() {
        // given
        MentorCreateDto createDto = generateMentorCreateDto();

        // when
        MentorEntity entity = MENTOR_MAPPER.toEntity(createDto);

        // then
        Assertions.assertNull(entity.getId());
        Assertions.assertEquals(createDto.getUserId(), entity.getUserId());
    }

    @Test
    void mapToFullDto_happyPath() {
        // given
        MentorEntity mentor = generateMentorWithId();
        mentor.setPlans(Collections.singletonList(generatePlanWithId()));

        // when
        MentorFullDto mentorFullDto = MENTOR_MAPPER.toFullDto(mentor);

        // then
        Assertions.assertEquals(mentor.getId(), mentorFullDto.getId());
        Assertions.assertEquals(mentor.getUserId(), mentorFullDto.getUserId());
        Assertions.assertEquals(mentor.getPlans().get(0).getId(), mentorFullDto.getPlans().get(0).getId());
        Assertions.assertEquals(mentor.getPlans().get(0).getDescription(), mentorFullDto.getPlans().get(0).getDescription());
        Assertions.assertEquals(mentor.getPlans().get(0).getEstimatedHours(), mentorFullDto.getPlans().get(0).getEstimatedHours());
        Assertions.assertEquals(mentor.getPlans().get(0).getFileUrl(), mentorFullDto.getPlans().get(0).getFileUrl());

    }


}
