package itstep.mentoringservice.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import itstep.mentoringservice.dto.plan.PlanCreateDto;
import itstep.mentoringservice.dto.plan.PlanFullDto;
import itstep.mentoringservice.dto.plan.PlanUpdateDto;
import itstep.mentoringservice.service.PlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(description = "Controller dedicated to manage plans")
@RequestMapping("/plans")
@RestController
public class PlanController {

    @Autowired
    private PlanService planService;

    @GetMapping("/{id}")
    @ApiOperation(value = "Find one plan by id", notes = "Existing id must be specified")
    public PlanFullDto getById(@PathVariable Integer id) {
        return planService.findById(id);
    }

    @GetMapping("")
    @ApiOperation(value = "Find all plans")
    public Page<PlanFullDto> findALl(@RequestParam Integer page, @RequestParam Integer size) {
        return planService.findAll(page, size);
    }

    @PostMapping("")
    @ApiOperation(value = "Create plan")
    public PlanFullDto create(@Valid @RequestBody PlanCreateDto createDto) {
        return planService.create(createDto);
    }

    @PutMapping("")
    @ApiOperation(value = "Update plan")
    public PlanFullDto update(@Valid @RequestBody PlanUpdateDto updateDto) {
        return planService.update(updateDto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete plan by id", notes = "Existing id must be specified")
    public void delete(@PathVariable Integer id) {
        planService.deleteById(id);
    }

}
