package itstep.mentoringservice.mapper;

import itstep.mentoringservice.dto.plan.PlanCreateDto;
import itstep.mentoringservice.dto.plan.PlanFullDto;
import itstep.mentoringservice.entity.PlanEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = {LearningProcessMapper.class})
public interface PlanMapper {

    PlanMapper PLAN_MAPPER = Mappers.getMapper(PlanMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "mentors", ignore = true)
    @Mapping(target = "learningProcesses", ignore = true)
    @Mapping(target = "becomeMentorRequests", ignore = true)
    PlanEntity toEntity(PlanCreateDto createDto);

    PlanFullDto toFullDto(PlanEntity entity);

    List<PlanFullDto> toFullDtoList(List<PlanEntity> entities);

}
