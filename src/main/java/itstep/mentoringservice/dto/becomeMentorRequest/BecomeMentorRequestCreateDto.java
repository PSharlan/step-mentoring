package itstep.mentoringservice.dto.becomeMentorRequest;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class BecomeMentorRequestCreateDto {

    @ApiModelProperty(notes = "List of plans for becomeMentorRequests")
    @NotNull(message = "planId's can't be null!")
    private List<Integer> planIds;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    @NotNull(message = "userId can't be null!")
    private Integer userId;

}
