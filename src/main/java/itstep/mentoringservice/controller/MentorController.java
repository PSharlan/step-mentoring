package itstep.mentoringservice.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import itstep.mentoringservice.dto.mentor.MentorCreateDto;
import itstep.mentoringservice.dto.mentor.MentorFullDto;
import itstep.mentoringservice.service.MentorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(description = "Controller dedicated to manage mentors")
@RequestMapping("/mentors")
@RestController
public class MentorController {

    @Autowired
    private MentorService mentorService;

    @GetMapping("/{id}")
    @ApiOperation(value = "Find one mentor by id", notes = "Existing id must be specified")
    public MentorFullDto getById(@PathVariable Integer id) {
        return mentorService.findById(id);
    }

    @GetMapping("")
    @ApiOperation(value = "Find all mentors")
    public Page<MentorFullDto> findAll(@RequestParam Integer page, @RequestParam Integer size) {
        return mentorService.findAll(page, size);
    }

    @PostMapping("")
    @ApiOperation(value = "Create mentor")
    public MentorFullDto create(@Valid @RequestBody MentorCreateDto createDto) {
        return mentorService.create(createDto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete mentor by id", notes = "Existing id must be specified")
    public void delete(@PathVariable Integer id) {
        mentorService.deleteById(id);
    }
}
