package itstep.mentoringservice.service;

import itstep.mentoringservice.dto.learningProcess.LearningProcessCreateDto;
import itstep.mentoringservice.dto.learningProcess.LearningProcessFullDto;
import itstep.mentoringservice.dto.learningProcess.LearningProcessUpdateDto;
import itstep.mentoringservice.entity.LearningProcessEntity;
import itstep.mentoringservice.entity.enums.LearningProcessStatus;
import org.springframework.data.domain.Page;

import java.util.List;

public interface LearningProcessService {

    LearningProcessFullDto findById(Integer id);

    Page<LearningProcessFullDto> findAll(Integer page, Integer size);

    List<LearningProcessFullDto> findAllByStatus(LearningProcessStatus learningProcessStatus);

    LearningProcessFullDto create(LearningProcessCreateDto createDto);

    LearningProcessFullDto update(LearningProcessUpdateDto updateDto);

    void deleteById(Integer id);

    void closedFinishedProcesses();

}
