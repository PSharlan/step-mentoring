package itstep.mentoringservice.service;

import itstep.mentoringservice.dto.mentor.MentorCreateDto;
import itstep.mentoringservice.dto.mentor.MentorFullDto;
import org.springframework.data.domain.Page;

import java.util.List;

public interface MentorService {

    MentorFullDto findById(Integer id);

    Page<MentorFullDto> findAll(Integer page, Integer size);

    MentorFullDto create(MentorCreateDto createDto);

    void deleteById(Integer id);

}
