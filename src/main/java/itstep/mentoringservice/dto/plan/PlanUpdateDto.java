package itstep.mentoringservice.dto.plan;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class PlanUpdateDto {

    @ApiModelProperty(example = "1", notes = "Id of the current plan")
    @NotNull(message = "planId can't be null!")
    private Integer id;

    @ApiModelProperty(notes = "Description for plan")
    @NotEmpty(message = "Description can't be empty!")
    private String description;

    @ApiModelProperty(example = "1", notes = "EstimatedHours for plan")
    @NotNull(message = "EstimatedHours can't be null!")
    private Integer estimatedHours;

    @ApiModelProperty(example = "http://file", notes = "Link to a file with a detailed plan")
    @NotEmpty(message = "FileUrl can't be empty!")
    private String fileUrl;

}
