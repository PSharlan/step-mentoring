package itstep.mentoringservice.repository;

import itstep.mentoringservice.entity.LearningProcessEntity;
import itstep.mentoringservice.entity.MentorEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MentorRepository extends JpaRepository<MentorEntity, Integer> {

    MentorEntity findOneById(Integer id);

    List<MentorEntity> findAllByUserId(Integer userId);

}
