package itstep.mentoringservice.service;

import itstep.mentoringservice.EntityGenerationUtils;
import itstep.mentoringservice.dto.learningProcess.LearningProcessCreateDto;
import itstep.mentoringservice.dto.learningProcess.LearningProcessFullDto;
import itstep.mentoringservice.dto.learningProcess.LearningProcessUpdateDto;
import itstep.mentoringservice.entity.LearningProcessEntity;
import itstep.mentoringservice.entity.MentorEntity;
import itstep.mentoringservice.entity.PlanEntity;
import itstep.mentoringservice.entity.enums.LearningProcessStatus;
import itstep.mentoringservice.entity.enums.RequestStatus;
import itstep.mentoringservice.exception.LearningProcessNotFoundException;
import itstep.mentoringservice.exception.MentorNotFoundException;
import itstep.mentoringservice.exception.PlanNotFoundException;
import itstep.mentoringservice.integration.AbstractIntegrationTest;
import itstep.mentoringservice.mapper.MapperGenerationUtils;
import itstep.mentoringservice.repository.LearningProcessRepository;
import itstep.mentoringservice.repository.MentorRepository;
import itstep.mentoringservice.repository.PlanRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static itstep.mentoringservice.EntityGenerationUtils.*;
import static itstep.mentoringservice.mapper.MapperGenerationUtils.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class LearningProcessServiceTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() {
        //given
        LearningProcessEntity entity = generateLearningProcessWithMentorAndPlan();

        LearningProcessEntity saved = learningProcessRepository.save(entity);

        // when
        LearningProcessFullDto fullDto = learningProcessService.findById(saved.getId());

        // then
        Assertions.assertNotNull(fullDto);
    }

    @Test
    void findAll_happyPath() {
        // given
        List<LearningProcessEntity> entities = generateLearningProcessesWithMentorAndPlan();

        List<LearningProcessEntity> saved = learningProcessRepository.saveAll(entities);

        // when
        Page<LearningProcessFullDto> foundPage = learningProcessService.findAll(0, 3);
        List<LearningProcessFullDto> learningProcesses = foundPage.getContent();

        // then
        Assertions.assertNotNull(foundPage);
        Assertions.assertEquals(saved.size(), learningProcesses.size());
    }

    @Test
    void findAllByStatus_happyPath() {
        // given
        List<LearningProcessEntity> entities = generateLearningProcessesWithMentorAndPlan();
        entities.get(0).setStatus(LearningProcessStatus.FINISHED);
        entities.get(1).setStatus(LearningProcessStatus.FINISHED);
        entities.get(2).setStatus(LearningProcessStatus.PROCESS);

        List<LearningProcessEntity> saved = learningProcessRepository.saveAll(entities);

        // when
        List<LearningProcessFullDto> foundDto = learningProcessService.findAllByStatus(LearningProcessStatus.FINISHED);

        // then
        Assertions.assertNotNull(foundDto);
        Assertions.assertEquals(saved.get(0).getStatus(), foundDto.get(0).getStatus());
        Assertions.assertEquals(saved.get(1).getStatus(), foundDto.get(1).getStatus());
        Assertions.assertNotEquals(foundDto.size(), saved.size());
    }

    @Test
    void create_happyPath() {
        // given
        LearningProcessCreateDto createDto = generateLearningProcessCreateDtoWithMentorAndPlan();

        // when
        LearningProcessFullDto saved = learningProcessService.create(createDto);

        // then
        Assertions.assertNotNull(saved.getId());

        Assertions.assertNotNull(saved.getStatus());
        Assertions.assertNotNull(saved.getStartDate());
        Assertions.assertEquals(saved.getUserId(), createDto.getUserId());
        Assertions.assertEquals(saved.getPlan().getId(), createDto.getPlanId());
        Assertions.assertEquals(saved.getStatus(), LearningProcessStatus.PROCESS);
    }

    @Test
    void update_happyPath() {
        // given
        LearningProcessCreateDto createDto = generateLearningProcessCreateDtoWithMentorAndPlan();

        LearningProcessFullDto saved = learningProcessService.create(createDto);

        LearningProcessUpdateDto updateDto = generateLearningProcessUpdateDto();
        updateDto.setId(saved.getId());

        // when
        LearningProcessFullDto updated = learningProcessService.update(updateDto);

        // then
        Assertions.assertNotNull(updated);
        Assertions.assertEquals(saved.getId(), updated.getId());
        Assertions.assertEquals(saved.getUserId(), updated.getUserId());
        Assertions.assertEquals(saved.getPlan(), updated.getPlan());
        Assertions.assertNotEquals(saved.getStatus(), updated.getStatus());
        Assertions.assertEquals(updated.getStatus(), LearningProcessStatus.FINISHED);
    }

    @Test
    void deleteById_happyPath() {
        // given
        LearningProcessFullDto saved = learningProcessService.create(generateLearningProcessCreateDtoWithMentorAndPlan());

        // when
        learningProcessService.deleteById(saved.getId());

        Exception exception = assertThrows(LearningProcessNotFoundException.class,
                () -> learningProcessService.findById(saved.getId()));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(saved.getId())));
    }

    @Test
    void closedFinishedProcesses_happyPath() {
        // given
        LearningProcessEntity entity = generateLearningProcessWithMentorAndPlan();
        entity.getPlan().setEstimatedHours(1);
        entity.setStatus(LearningProcessStatus.PROCESS);
        entity.setStartDate(Instant.now().minus(12, ChronoUnit.DAYS));

        LearningProcessEntity saved = learningProcessRepository.save(entity);

        // when
        learningProcessService.closedFinishedProcesses();
        LearningProcessFullDto updated = learningProcessService.findById(saved.getId());

        // then
        Assertions.assertEquals(updated.getStatus(), LearningProcessStatus.FINISHED);
    }

    @Test
    void findById_whenNotFound() {
        // given
        Integer notExistingId = 100;

        // when
        Exception exception = assertThrows(LearningProcessNotFoundException.class,
                () -> learningProcessService.findById(notExistingId));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }


    @Test
    void create_whenMentorNotFound() {
        // given
        LearningProcessCreateDto createDto = generateLearningProcessCreateDtoWithMentorAndPlan();

        createDto.setMentorId(0);

        // when
        Exception exception = assertThrows(MentorNotFoundException.class,
                () -> learningProcessService.create(createDto));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(createDto.getMentorId())));
    }

    @Test
    void create_whenPlanNotFound() {
        // given
        LearningProcessCreateDto createDto = generateLearningProcessCreateDtoWithMentorAndPlan();

        createDto.setPlanId(0);

        // when
        Exception exception = assertThrows(PlanNotFoundException.class,
                () -> learningProcessService.create(createDto));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(createDto.getPlanId())));
    }

    @Test
    void update_whenNotFound() {
        // given
        LearningProcessUpdateDto updateDto = generateLearningProcessUpdateDto();

        Integer notExistingId = 100;
        updateDto.setId(notExistingId);

        // when
        Exception exception = Assertions.assertThrows(LearningProcessNotFoundException.class,
                () -> learningProcessService.update(updateDto));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(updateDto.getId())));
    }

    @Test
    void delete_whenNotFound() {
        // given
        Integer notExistingId = 100;

        // when
        Exception exception = Assertions.assertThrows(LearningProcessNotFoundException.class,
                () -> learningProcessService.deleteById(notExistingId));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    private LearningProcessEntity generateLearningProcessWithMentorAndPlan() {
        LearningProcessEntity entity = generateLearningProcess();
        entity.setMentor(mentorRepository.save(generateMentor()));
        entity.setPlan(planRepository.save(generatePlan()));

        return entity;
    }

    private List<LearningProcessEntity> generateLearningProcessesWithMentorAndPlan() {
        List<LearningProcessEntity> learningProcesses = generateLearningProcesses();

        MentorEntity savedMentor = mentorRepository.save(generateMentor());
        PlanEntity savedPlan = planRepository.save(generatePlan());

        for(LearningProcessEntity learningProcess : learningProcesses){
            learningProcess.setMentor(savedMentor);
            learningProcess.setPlan(savedPlan);
        }

        return learningProcesses;
    }

    private LearningProcessCreateDto generateLearningProcessCreateDtoWithMentorAndPlan() {
        LearningProcessCreateDto createDto = new LearningProcessCreateDto();

        createDto.setUserId(1);
        createDto.setMentorId(mentorRepository.save(generateMentor()).getId());
        createDto.setPlanId(planRepository.save(generatePlan()).getId());

        return createDto;
    }
}
