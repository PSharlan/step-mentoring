package itstep.mentoringservice.dto.becomeMentorRequest;

import io.swagger.annotations.ApiModelProperty;
import itstep.mentoringservice.dto.plan.PlanFullDto;
import itstep.mentoringservice.entity.enums.RequestStatus;
import lombok.Data;

import java.util.List;

@Data
public class BecomeMentorRequestFullDto {

    @ApiModelProperty(example = "1", notes = "Id of the current becomeMentorRequest")
    private Integer id;

    @ApiModelProperty(notes = "List of plans for becomeMentorRequests")
    private List<PlanFullDto> plans;

    @ApiModelProperty(example = "ACCEPTED", notes = "Status of becomeMentorRequest")
    private RequestStatus status;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    private Integer userId;

}
