package itstep.mentoringservice.entity;

import itstep.mentoringservice.entity.enums.RequestStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "become_mentor_request")
public class BecomeMentorRequestEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToMany(mappedBy = "becomeMentorRequests")
    private List<PlanEntity> plans = new ArrayList<>();

    @Column(name = "status")
    @Enumerated(value = EnumType.STRING)
    private RequestStatus status;

    @Column(name = "user_id", nullable = false)
    private Integer userId;

}
