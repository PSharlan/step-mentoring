package itstep.mentoringservice.service.impl;

import itstep.mentoringservice.dto.learningProcess.LearningProcessCreateDto;
import itstep.mentoringservice.dto.learningProcess.LearningProcessFullDto;
import itstep.mentoringservice.dto.learningProcess.LearningProcessUpdateDto;
import itstep.mentoringservice.entity.LearningProcessEntity;
import itstep.mentoringservice.entity.MentorEntity;
import itstep.mentoringservice.entity.PlanEntity;
import itstep.mentoringservice.entity.enums.LearningProcessStatus;
import itstep.mentoringservice.exception.LearningProcessNotFoundException;
import itstep.mentoringservice.exception.MentorNotFoundException;
import itstep.mentoringservice.exception.PlanNotFoundException;
import itstep.mentoringservice.repository.LearningProcessRepository;
import itstep.mentoringservice.repository.MentorRepository;
import itstep.mentoringservice.repository.PlanRepository;
import itstep.mentoringservice.service.LearningProcessService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static itstep.mentoringservice.mapper.LearningProcessMapper.*;

@Slf4j
@Service
public class LearningProcessServiceImpl implements LearningProcessService {

    @Autowired
    private LearningProcessRepository learningProcessRepository;

    @Autowired
    private PlanRepository planRepository;

    @Autowired
    private MentorRepository mentorRepository;


    @Override
    @Transactional(readOnly = true)
    public LearningProcessFullDto findById(Integer id) {
        LearningProcessEntity foundEntity = learningProcessRepository.findById(id)
                .orElseThrow(() -> new LearningProcessNotFoundException(id));

        log.info("LearningProcessServiceImpl -> found learningProcess {} by id: {}", foundEntity, id);
        return LEARNING_PROCESS_MAPPER.toFullDto(foundEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<LearningProcessFullDto> findAll(Integer page, Integer size) {
        Page<LearningProcessEntity> foundEntities = learningProcessRepository.findAll(PageRequest.of(page, size));

        Page<LearningProcessFullDto> dto = foundEntities.map(entity -> LEARNING_PROCESS_MAPPER.toFullDto(entity));

        log.info("LearningProcessServiceImpl -> found {} learningProcesses", dto.getNumberOfElements());
        return dto;
    }

    @Override
    @Transactional(readOnly = true)
    public List<LearningProcessFullDto> findAllByStatus(LearningProcessStatus learningProcessStatus) {
        List<LearningProcessEntity> foundEntities = learningProcessRepository.findAllByStatus(learningProcessStatus);

        log.info("LearningProcessServiceImpl -> found {} learningProcesses by status", foundEntities.size());
        return LEARNING_PROCESS_MAPPER.toFullDtoList(foundEntities);
    }

    @Override
    @Transactional
    public LearningProcessFullDto create(LearningProcessCreateDto createDto) {
        LearningProcessEntity entityToSave = LEARNING_PROCESS_MAPPER.toEntity(createDto);

        MentorEntity mentor = mentorRepository.findById(createDto.getMentorId())
                .orElseThrow(() -> new MentorNotFoundException(createDto.getMentorId()));

        PlanEntity plan = planRepository.findById(createDto.getPlanId())
                .orElseThrow(() -> new PlanNotFoundException(createDto.getPlanId()));

        entityToSave.setMentor(mentor);
        entityToSave.setPlan(plan);
        entityToSave.setStatus(LearningProcessStatus.PROCESS);
        entityToSave.setStartDate(Instant.now());

        LearningProcessEntity savedEntity = learningProcessRepository.save(entityToSave);

        log.info("LearningProcessServiceImpl -> learningProcess {} successfully saved", savedEntity);
        return LEARNING_PROCESS_MAPPER.toFullDto(savedEntity);
    }

    @Override
    @Transactional
    public LearningProcessFullDto update(LearningProcessUpdateDto updateDto) {
        LearningProcessEntity entityToUpdate = learningProcessRepository.findById(updateDto.getId())
                .orElseThrow(() -> new LearningProcessNotFoundException(updateDto.getId()));

        entityToUpdate.setStatus(updateDto.getStatus());

        LearningProcessEntity updatedEntity = learningProcessRepository.save(entityToUpdate);

        log.info("LearningProcessServiceImpl -> becomeMentorRequest {} successfully updated", updatedEntity);
        return LEARNING_PROCESS_MAPPER.toFullDto(updatedEntity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        learningProcessRepository.findById(id)
                .orElseThrow(() -> new LearningProcessNotFoundException(id));

        learningProcessRepository.deleteById(id);
    }

    @Override
    @Transactional
    @Scheduled(cron = "${scheduler.finish-process-scanning-interval}")
    public void closedFinishedProcesses() {
        Instant currentDate = Instant.now();

        List<LearningProcessEntity> learningProcesses = learningProcessRepository.findAll();

        for (LearningProcessEntity entity : learningProcesses) {
            Instant endTime = entity.getStartDate().plus(entity.getPlan().getEstimatedHours(), ChronoUnit.HOURS);

            if (currentDate.isAfter(endTime)) {
                entity.setStatus(LearningProcessStatus.FINISHED);
                learningProcessRepository.save(entity);
            }
        }
    }
}
