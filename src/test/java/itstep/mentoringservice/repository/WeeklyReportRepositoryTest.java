package itstep.mentoringservice.repository;

import itstep.mentoringservice.EntityGenerationUtils;
import itstep.mentoringservice.entity.LearningProcessEntity;
import itstep.mentoringservice.entity.MentorEntity;
import itstep.mentoringservice.entity.PlanEntity;
import itstep.mentoringservice.entity.WeeklyReportEntity;
import itstep.mentoringservice.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static itstep.mentoringservice.EntityGenerationUtils.*;

public class WeeklyReportRepositoryTest extends AbstractIntegrationTest {

    @Test
    void create_HappyPath(){
        //given
        WeeklyReportEntity report = generateWeeklyReport();
        LearningProcessEntity process = generateLearningProcess();
        MentorEntity mentor = generateMentor();
        PlanEntity plan = generatePlan();

        PlanEntity savedPlan = planRepository.save(plan);
        MentorEntity savedMentor = mentorRepository.save(mentor);

        process.setPlan(savedPlan);
        process.setMentor(savedMentor);

        LearningProcessEntity savedProcess = learningProcessRepository.save(process);

        report.setLearningProcess(savedProcess);

        //when
        WeeklyReportEntity savedReport = weeklyReportRepository.save(report);

        //then
        Assertions.assertNotNull(savedReport.getId());
    }

    @Test
    void findById_happyPath(){
        //given
        WeeklyReportEntity savedReport = saveReportWithDependencies();

        //when
        WeeklyReportEntity foundReports = weeklyReportRepository.findOneById(savedReport.getId());

        //then
        Assertions.assertNotNull(foundReports.getId());
        Assertions.assertEquals(savedReport.getId(), foundReports.getId());
    }

    @Test
    void findAll_happyPath(){
        //given
        List<WeeklyReportEntity> reports = generateWeeklyReports();
        LearningProcessEntity process = generateLearningProcess();
        MentorEntity mentor = generateMentor();
        PlanEntity plan = generatePlan();

        PlanEntity savedPlan = planRepository.save(plan);
        MentorEntity savedMentor = mentorRepository.save(mentor);

        process.setPlan(savedPlan);
        process.setMentor(savedMentor);

        LearningProcessEntity savedProcess = learningProcessRepository.save(process);

        for(WeeklyReportEntity report : reports){
            report.setLearningProcess(savedProcess);
        }
        List<WeeklyReportEntity> savedReports = weeklyReportRepository.saveAll(reports);

        //when
        List<WeeklyReportEntity> foundReports = weeklyReportRepository.findAll();

        //then
        Assertions.assertNotNull(foundReports);
        Assertions.assertEquals(savedReports.size(), foundReports.size());
    }


    @Test
    void deleteById_happyPath_learningProcessWilRemain(){
        //given
        WeeklyReportEntity savedReport = saveReportWithDependencies();

        //when
        weeklyReportRepository.deleteById(savedReport.getId());

        //then
        Assertions.assertNull(weeklyReportRepository.findOneById(savedReport.getId()));
        Assertions.assertNotNull(learningProcessRepository.findOneById(savedReport.getLearningProcess().getId()));
    }

    @Test
    void deleteAll_happyPath_learningProcessWilRemain(){
        //given
        List<WeeklyReportEntity> reports = generateWeeklyReports();
        LearningProcessEntity process = generateLearningProcess();
        MentorEntity mentor = generateMentor();
        PlanEntity plan = generatePlan();

        PlanEntity savedPlan = planRepository.save(plan);
        MentorEntity savedMentor = mentorRepository.save(mentor);

        process.setPlan(savedPlan);
        process.setMentor(savedMentor);

        LearningProcessEntity savedProcess = learningProcessRepository.save(process);

        for(WeeklyReportEntity report : reports){
            report.setLearningProcess(savedProcess);
        }
        List<WeeklyReportEntity> savedReports = weeklyReportRepository.saveAll(reports);

        //when
        weeklyReportRepository.deleteAll();

        //then
        Assertions.assertEquals(0, weeklyReportRepository.findAll().size());
        for(WeeklyReportEntity report : savedReports){
            Assertions.assertNotNull(learningProcessRepository.findOneById(report.getLearningProcess().getId()));
        }
    }

    @Test
    void update_happyPath(){
        //given
        WeeklyReportEntity savedReport = saveReportWithDependencies();
        String newCommentForStudent = "updated";

        //when
        savedReport.setCommentForStudent(newCommentForStudent);
        WeeklyReportEntity updatedReport = weeklyReportRepository.save(savedReport);

        //then
        Assertions.assertEquals(updatedReport.getCommentForStudent(), savedReport.getCommentForStudent());
        Assertions.assertEquals(savedReport.getId(), updatedReport.getId());
    }

    private WeeklyReportEntity saveReportWithDependencies(){
        WeeklyReportEntity report = generateWeeklyReport();
        LearningProcessEntity process = generateLearningProcess();
        MentorEntity mentor = generateMentor();
        PlanEntity plan = generatePlan();

        PlanEntity savedPlan = planRepository.save(plan);
        MentorEntity savedMentor = mentorRepository.save(mentor);

        process.setPlan(savedPlan);
        process.setMentor(savedMentor);

        LearningProcessEntity savedProcess = learningProcessRepository.save(process);

        report.setLearningProcess(savedProcess);
        return weeklyReportRepository.save(report);
    }
}
