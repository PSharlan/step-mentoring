package itstep.mentoringservice.repository;

import itstep.mentoringservice.entity.LearningProcessEntity;
import itstep.mentoringservice.entity.MentorEntity;
import itstep.mentoringservice.entity.PlanEntity;
import itstep.mentoringservice.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static itstep.mentoringservice.EntityGenerationUtils.*;

public class PlanRepositoryTest extends AbstractIntegrationTest {

    @Test
    void findAll_happyPath() {
        // given
        List<PlanEntity> toSave = generatePlans();

        List<PlanEntity> saved = planRepository.saveAll(toSave);

        // when
        List<PlanEntity> found = planRepository.findAll();

        // then
        Assertions.assertEquals(found.size(), 3);
        Assertions.assertEquals(saved.size(), found.size());
    }

    @Test
    void findById_happyPath() {
        // given
        PlanEntity toSave = generatePlan();
        List<LearningProcessEntity> learningProcesses = generateLearningProcesses();
        MentorEntity mentor = generateMentor();

        toSave.setLearningProcesses(learningProcesses);
        mentor.setLearningProcesses(learningProcesses);

        for  (LearningProcessEntity entity : learningProcesses) {
            entity.setPlan(toSave);
            entity.setMentor(mentor);
        }

        PlanEntity saved = planRepository.save(toSave);
        mentorRepository.save(mentor);
        learningProcessRepository.saveAll(learningProcesses);

        // when
        PlanEntity found = planRepository.findOneById(saved.getId());

        // then
        Assertions.assertNotNull(found);
        Assertions.assertEquals(saved.getId(), found.getId());
    }

    @Test
    void save_happyPath() {
        // given
        PlanEntity toSave = generatePlan();

        // when
        PlanEntity saved = planRepository.save(toSave);

        // then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void save_withLearningProcess_happyPath() {
        // given
        PlanEntity toSave = generatePlan();
        List<LearningProcessEntity> learningProcesses = generateLearningProcesses();
        MentorEntity mentor = generateMentor();

        toSave.setLearningProcesses(learningProcesses);
        mentor.setLearningProcesses(learningProcesses);

        for  (LearningProcessEntity entity : learningProcesses) {
            entity.setPlan(toSave);
            entity.setMentor(mentor);
        }

        // when
        PlanEntity saved = planRepository.save(toSave);

        // then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
        Assertions.assertEquals(saved.getLearningProcesses().size(), 3);
    }

    @Test
    void update_happyPath() {
        // given
        PlanEntity toSave = generatePlan();
        PlanEntity saved = planRepository.save(toSave);
        PlanEntity found = planRepository.findOneById(saved.getId());

        // when
        saved.setFileUrl("qwe");
        planRepository.save(toSave);
        PlanEntity updated = planRepository.findOneById(saved.getId());

        // then
        Assertions.assertEquals(found.getId(), updated.getId());
        Assertions.assertNotEquals(found.getFileUrl(), updated.getFileUrl());
        Assertions.assertNotEquals(found, updated);
    }

    @Test
    void delete_happyPath() {
        // given
        PlanEntity toSave = generatePlan();
        PlanEntity saved = planRepository.save(toSave);

        // when
        planRepository.deleteById(saved.getId());
        PlanEntity foundDeleted = planRepository.findOneById(saved.getId());

        //then
        Assertions.assertNull(foundDeleted);
    }


}
