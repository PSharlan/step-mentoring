package itstep.mentoringservice.repository;

import itstep.mentoringservice.EntityGenerationUtils;
import itstep.mentoringservice.entity.MentorEntity;
import itstep.mentoringservice.entity.PlanEntity;
import itstep.mentoringservice.entity.SubscribeMentorRequestEntity;
import itstep.mentoringservice.entity.enums.RequestStatus;
import itstep.mentoringservice.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static itstep.mentoringservice.EntityGenerationUtils.*;

public class SubscribeMentorRequestEntityTest extends AbstractIntegrationTest {

    @Test
    void create_happyPath(){
        //given
        SubscribeMentorRequestEntity request = generateSubscribeMentorRequest();
        MentorEntity mentor = generateMentor();
        PlanEntity plan = generatePlan();

        MentorEntity savedMentor = mentorRepository.save(mentor);
        PlanEntity savedPlan = planRepository.save(plan);

        request.setMentor(savedMentor);
        request.setPlan(savedPlan);

        //when
        SubscribeMentorRequestEntity savedRequest = subscribeMentorRequestRepository.save(request);

        //then
        Assertions.assertNotNull(savedRequest.getId());
    }

    @Test
    void findById_happyPath(){
        //given
        SubscribeMentorRequestEntity request = generateSubscribeMentorRequest();
        MentorEntity mentor = generateMentor();
        PlanEntity plan = generatePlan();

        MentorEntity savedMentor = mentorRepository.save(mentor);
        PlanEntity savedPlan = planRepository.save(plan);
        request.setMentor(savedMentor);
        request.setPlan(savedPlan);

        SubscribeMentorRequestEntity savedRequest = subscribeMentorRequestRepository.save(request);

        //when
        SubscribeMentorRequestEntity foundRequest = subscribeMentorRequestRepository.findOneById(savedRequest.getId());

        //then
        Assertions.assertEquals(savedRequest.getId(), foundRequest.getId());
    }

    @Test
    void findAll_happyPath(){
        //given
        List<SubscribeMentorRequestEntity> requests = generateSubscribeMentorRequests();
        MentorEntity mentor = generateMentor();
        PlanEntity plan = generatePlan();

        PlanEntity savedPlan = planRepository.save(plan);
        MentorEntity savedMentor = mentorRepository.save(mentor);
        for(SubscribeMentorRequestEntity request : requests){
            request.setMentor(savedMentor);
            request.setPlan(plan);
        }

        List<SubscribeMentorRequestEntity> savedRequests = subscribeMentorRequestRepository.saveAll(requests);

        //when
        List<SubscribeMentorRequestEntity> foundRequests = subscribeMentorRequestRepository.findAll();

        //then
        Assertions.assertEquals(savedRequests.size(), foundRequests.size());
    }

    @Test
    void findAllByStatus_happyPath(){
        //given
        List<SubscribeMentorRequestEntity> requests = generateSubscribeMentorRequests();
        MentorEntity mentor = generateMentor();
        PlanEntity plan = generatePlan();

        MentorEntity savedMentor = mentorRepository.save(mentor);
        PlanEntity savedPlan = planRepository.save(plan);
        for(SubscribeMentorRequestEntity request : requests){
            request.setMentor(savedMentor);
            request.setPlan(savedPlan);
        }

        List<SubscribeMentorRequestEntity> savedRequests = subscribeMentorRequestRepository.saveAll(requests);

        //when
        List<SubscribeMentorRequestEntity> foundRequests = subscribeMentorRequestRepository.findAllByStatus(RequestStatus.ACCEPTED);

        //then
        for (SubscribeMentorRequestEntity request : foundRequests){
            Assertions.assertEquals(request.getStatus(), RequestStatus.ACCEPTED);
        }
    }

    @Test
    void deleteById_happyPath_mentorWillRemain(){
        //given
        SubscribeMentorRequestEntity request = generateSubscribeMentorRequest();
        MentorEntity mentor = generateMentor();
        PlanEntity plan = generatePlan();

        MentorEntity savedMentor = mentorRepository.save(mentor);
        PlanEntity savedPlan = planRepository.save(plan);

        request.setMentor(savedMentor);
        request.setPlan(savedPlan);

        SubscribeMentorRequestEntity savedRequest = subscribeMentorRequestRepository.save(request);

        //when
        subscribeMentorRequestRepository.deleteById(savedRequest.getId());

        //then
        Assertions.assertNull(subscribeMentorRequestRepository.findOneById(savedRequest.getId()));
        Assertions.assertNotNull(mentorRepository.findOneById(savedMentor.getId()));
    }

    @Test
    void deleteAll_happyPath_mentorWillRemain(){
        //given
        List<SubscribeMentorRequestEntity> requests = generateSubscribeMentorRequests();
        MentorEntity mentor = generateMentor();
        PlanEntity plan = generatePlan();

        PlanEntity savedPlan = planRepository.save(plan);
        MentorEntity savedMentor = mentorRepository.save(mentor);
        for(SubscribeMentorRequestEntity request : requests){
            request.setMentor(savedMentor);
            request.setPlan(savedPlan);
        }

        List<SubscribeMentorRequestEntity> savedRequests = subscribeMentorRequestRepository.saveAll(requests);

        //when
        subscribeMentorRequestRepository.deleteAll();

        //then
        Assertions.assertEquals(0, subscribeMentorRequestRepository.findAll().size());
        Assertions.assertNotNull(mentorRepository.findOneById(savedMentor.getId()));
    }

    @Test
    void update_happyPath(){
        //given
        SubscribeMentorRequestEntity request = generateSubscribeMentorRequest();
        MentorEntity mentor = generateMentor();
        PlanEntity plan = generatePlan();

        PlanEntity savedPlan = planRepository.save(plan);
        MentorEntity savedMentor = mentorRepository.save(mentor);

        request.setMentor(savedMentor);
        request.setPlan(savedPlan);

        SubscribeMentorRequestEntity savedRequest = subscribeMentorRequestRepository.save(request);
        Assertions.assertEquals(savedRequest.getStatus(), RequestStatus.PENDING);

        //when
        savedRequest.setStatus(RequestStatus.ACCEPTED);
        SubscribeMentorRequestEntity updatedRequest = subscribeMentorRequestRepository.save(savedRequest);

        //then
        Assertions.assertEquals(updatedRequest.getStatus(), RequestStatus.ACCEPTED);
        Assertions.assertEquals(savedRequest.getId(), updatedRequest.getId());
    }
}
