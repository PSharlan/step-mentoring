package itstep.mentoringservice.integration;

import itstep.mentoringservice.MentoringServiceApplication;
import itstep.mentoringservice.integration.initalizer.MySqlInitializer;
import itstep.mentoringservice.mapper.*;
import itstep.mentoringservice.repository.*;
import itstep.mentoringservice.service.*;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.junit.jupiter.Testcontainers;
import com.fasterxml.jackson.databind.ObjectMapper;

@ContextConfiguration(initializers = MySqlInitializer.class,
        classes = MentoringServiceApplication.class)
@SpringBootTest
@Testcontainers
@AutoConfigureMockMvc
public class AbstractIntegrationTest {

    @Autowired
    protected BecomeMentorRequestRepository becomeMentorRequestRepository;

    @Autowired
    protected LearningProcessRepository learningProcessRepository;

    @Autowired
    protected MentorRepository mentorRepository;

    @Autowired
    protected PlanRepository planRepository;

    @Autowired
    protected SubscribeMentorRequestRepository subscribeMentorRequestRepository;

    @Autowired
    protected WeeklyReportRepository weeklyReportRepository;

    @Autowired
    protected BecomeMentorRequestService becomeMentorRequestService;

    @Autowired
    protected LearningProcessService learningProcessService;

    @Autowired
    protected MentorService mentorService;

    @Autowired
    protected PlanService planService;

    @Autowired
    protected SubscribeMentorRequestService subscribeMentorRequestService;

    @Autowired
    protected WeeklyReportService weeklyReportService;

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    protected MockMvc mockMvc;

    @BeforeEach
    public void prepareDatabase() {
        becomeMentorRequestRepository.deleteAll();
        learningProcessRepository.deleteAll();
        mentorRepository.deleteAll();
        planRepository.deleteAll();
        subscribeMentorRequestRepository.deleteAll();
        weeklyReportRepository.deleteAll();
    }
}
