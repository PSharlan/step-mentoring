package itstep.mentoringservice.entity;

import itstep.mentoringservice.entity.enums.LearningProcessStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "learning_process")
public class LearningProcessEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "status")
    @Enumerated(value = EnumType.STRING)
    private LearningProcessStatus status;

    @Column(name = "start_date")
    private Instant startDate;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "mentor_id", nullable = false)
    private MentorEntity mentor;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "plan_id", nullable = false)
    private PlanEntity plan;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "learningProcess", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<WeeklyReportEntity> weeklyReports = new ArrayList<>();

    @Column(name = "user_id", nullable = false)
    private Integer userId;
}
