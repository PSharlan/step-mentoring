package itstep.mentoringservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import itstep.mentoringservice.EntityGenerationUtils;
import itstep.mentoringservice.dto.mentor.MentorCreateDto;
import itstep.mentoringservice.dto.mentor.MentorFullDto;
import itstep.mentoringservice.entity.MentorEntity;
import itstep.mentoringservice.entity.PlanEntity;
import itstep.mentoringservice.integration.AbstractIntegrationTest;
import itstep.mentoringservice.mapper.MapperGenerationUtils;
import itstep.mentoringservice.repository.MentorRepository;
import itstep.mentoringservice.repository.PlanRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static itstep.mentoringservice.EntityGenerationUtils.*;
import static itstep.mentoringservice.mapper.MapperGenerationUtils.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class MentorControllerTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() throws Exception {
        // given
        MentorEntity saved = mentorRepository.save(generateMentor());

        // when
        MvcResult result = mockMvc.perform(get("/mentors/{id}", saved.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        MentorFullDto foundDto = objectMapper.readValue(bytes, MentorFullDto.class);

        // then
        Assertions.assertNotNull(foundDto);
        Assertions.assertNotNull(foundDto.getId());
        Assertions.assertEquals(saved.getId(), foundDto.getId());
        Assertions.assertEquals(saved.getUserId(), foundDto.getUserId());
    }

    @Test
    void findById_whenNotFound() throws Exception {
        // given
        Integer notExistingId = 10000;

        // when
        mockMvc.perform(get("/mentors/{id}", notExistingId))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        // then
    }

    @Test
    void findAll_happyPath() throws Exception {
        // given
        List<MentorEntity> saved = mentorRepository.saveAll(generateMentors());

        // when
        mockMvc.perform(get("/mentors?page=0&size=3"))
                .andExpect(status().isOk())
                .andReturn();

        // then
    }

    @Test
    void create_happyPath() throws Exception {
        // given
        MentorCreateDto createDto = generateMentorCreateDtoWithPlans();

        // when
        MvcResult result = mockMvc.perform(post("/mentors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        MentorFullDto saved = objectMapper.readValue(bytes, MentorFullDto.class);

        // then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
        Assertions.assertEquals(saved.getUserId(), createDto.getUserId());
    }

    @Test
    void delete_happyPath() throws Exception {
        // given
        MentorEntity saved = mentorRepository.save(generateMentor());

        // when
        mockMvc.perform(delete("/mentors/{id}", saved.getId()))
                .andExpect(status().isOk());

        // then
        mockMvc.perform(get("/mentors/{id}", saved.getId()))
                .andExpect(status().isNotFound());
    }

    @Test
    void delete_whenNotFound() throws Exception {
        // given
        Integer notExistingId = 0;

        // when
        mockMvc.perform(delete("/mentors/{id}", notExistingId))
                .andExpect(status().isNotFound());

        // then
    }

    @Test
    void create_whenPlanIdsNull() throws Exception {
        // given
        MentorCreateDto createDto = generateMentorCreateDtoWithPlans();
        createDto.setPlanIds(null);

        // when
        mockMvc.perform(post("/mentors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    @Test
    void create_whenUserIdNull() throws Exception {
        // given
        MentorCreateDto createDto = generateMentorCreateDtoWithPlans();
        createDto.setUserId(null);

        // when
        mockMvc.perform(post("/mentors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    private MentorCreateDto generateMentorCreateDtoWithPlans() {
        MentorCreateDto createDto = generateMentorCreateDto();

        List<PlanEntity> plans = planRepository.saveAll(generatePlans());

        for (PlanEntity entity : plans) {
            List<Integer> plansIds = new ArrayList<>();
            plansIds.add(entity.getId());
            createDto.setPlanIds(plansIds);
        }
        return createDto;
    }
}
