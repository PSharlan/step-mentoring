package itstep.mentoringservice.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import itstep.mentoringservice.dto.weeklyReport.MentorWeeklyReportCreateDto;
import itstep.mentoringservice.dto.weeklyReport.StudentWeeklyReportCreateDto;
import itstep.mentoringservice.dto.weeklyReport.WeeklyReportFullDto;
import itstep.mentoringservice.service.WeeklyReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(description = "Controller dedicated to manager weeklyReports")
@RequestMapping("/weeklyReports")
@RestController
public class WeeklyReportController {

    @Autowired
    private WeeklyReportService weeklyReportService;

    @GetMapping("/{id}")
    @ApiOperation(value = "Find one weeklyReport by id", notes = "Existing id must be specified")
    public WeeklyReportFullDto getById(@PathVariable Integer id) {
        return weeklyReportService.findById(id);
    }

    @GetMapping("")
    @ApiOperation(value = "Find all weeklyReports")
    public Page<WeeklyReportFullDto> findAll(@RequestParam Integer page, @RequestParam Integer size) {
        return weeklyReportService.findAll(page, size);
    }

    @PostMapping("/mentor")
    @ApiOperation(value = "Create weeklyReport by mentor")
    public WeeklyReportFullDto create(@Valid @RequestBody MentorWeeklyReportCreateDto createDto) {
        return weeklyReportService.create(createDto);
    }

    @PostMapping("/student")
    @ApiOperation(value = "Create weeklyReport by student")
    public WeeklyReportFullDto create(@Valid @RequestBody StudentWeeklyReportCreateDto createDto) {
        return weeklyReportService.create(createDto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete weeklyReport by id", notes = "Existing id must be specified")
    public void delete(@PathVariable Integer id) {
        weeklyReportService.deleteById(id);
    }

}
