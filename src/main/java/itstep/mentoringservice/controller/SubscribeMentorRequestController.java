package itstep.mentoringservice.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import itstep.mentoringservice.dto.subscribeMentorRequest.SubscribeMentorRequestCreateDto;
import itstep.mentoringservice.dto.subscribeMentorRequest.SubscribeMentorRequestFullDto;
import itstep.mentoringservice.dto.subscribeMentorRequest.SubscribeMentorRequestUpdateDto;
import itstep.mentoringservice.entity.enums.RequestStatus;
import itstep.mentoringservice.service.SubscribeMentorRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(description = "Controller dedicated to manage subscribeMentorRequests")
@RequestMapping("/subscribeMentorRequests")
@RestController
public class SubscribeMentorRequestController {

    @Autowired
    private SubscribeMentorRequestService subscribeMentorRequestService;

    @GetMapping("/{id}")
    @ApiOperation(value = "Find one subscribeMentorRequest by id", notes = "Existing id must be specified")
    public SubscribeMentorRequestFullDto getById(@PathVariable Integer id) {
        return subscribeMentorRequestService.findById(id);
    }

    @GetMapping("")
    @ApiOperation(value = "Find all becomeMentorRequests")
    public Page<SubscribeMentorRequestFullDto> findAll(@RequestParam Integer page, @RequestParam Integer size) {
        return subscribeMentorRequestService.findAll(page, size);
    }

    @GetMapping("/status/{status}")
    @ApiOperation(value = "Find all becomeMentorRequests by status", notes = "Existing status must be specified")
    public List<SubscribeMentorRequestFullDto> findAllByStatus(@PathVariable RequestStatus status) {
        return subscribeMentorRequestService.findAllByStatus(status);
    }

    @PostMapping("")
    @ApiOperation(value = "Create subscribeMentorRequest")
    public SubscribeMentorRequestFullDto create(@Valid @RequestBody SubscribeMentorRequestCreateDto createDto) {
        return subscribeMentorRequestService.create(createDto);
    }

    @PutMapping("")
    @ApiOperation(value = "Update subscribeMentorRequest")
    public SubscribeMentorRequestFullDto update(@Valid @RequestBody SubscribeMentorRequestUpdateDto updateDto) {
        return subscribeMentorRequestService.update(updateDto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete subscribeMentorRequest by id", notes = "Existing id must be specified")
    public void delete(@PathVariable Integer id) {
        subscribeMentorRequestService.deleteById(id);
    }

}
