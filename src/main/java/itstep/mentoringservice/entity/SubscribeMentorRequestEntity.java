package itstep.mentoringservice.entity;

import itstep.mentoringservice.entity.enums.RequestStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import javax.persistence.*;

@Data
@Entity
@Table(name = "subscribe_mentor_request")
public class SubscribeMentorRequestEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "status")
    @Enumerated(value = EnumType.STRING)
    private RequestStatus status;

    @Column(name = "user_id", nullable = false)
    private Integer userId;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "mentor_id")
    private MentorEntity mentor;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "plan_id", nullable = false)
    private PlanEntity plan;

}
