package itstep.mentoringservice.exception;

public class LearningProcessNotFoundException extends EntityIsNotFoundException {

    public LearningProcessNotFoundException(Integer id) {
        super("LearningProcess ", id);
    }
}
