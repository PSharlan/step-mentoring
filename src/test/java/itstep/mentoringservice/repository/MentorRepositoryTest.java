package itstep.mentoringservice.repository;

import itstep.mentoringservice.entity.MentorEntity;
import itstep.mentoringservice.entity.SubscribeMentorRequestEntity;
import itstep.mentoringservice.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static itstep.mentoringservice.EntityGenerationUtils.*;

public class MentorRepositoryTest extends AbstractIntegrationTest {

    @Test
    void findAll_happyPath() {
        // given
        List<MentorEntity> toSave = generateMentors();

        List<MentorEntity> saved = mentorRepository.saveAll(toSave);

        // when
        List<MentorEntity> found = mentorRepository.findAll();

        // then
        Assertions.assertEquals(found.size(), 3);
        Assertions.assertEquals(saved.size(), found.size());
    }

    @Test
    void findById_happyPath() {
        // given
        MentorEntity toSave = generateMentor();

        MentorEntity saved = mentorRepository.save(toSave);

        // when
        MentorEntity found = mentorRepository.findOneById(saved.getId());

        // then
        Assertions.assertNotNull(found);
        Assertions.assertEquals(saved.getId(), found.getId());
    }

    @Test
    void findByUserUd_happyPath() {
        // given
        List<MentorEntity> toSave = generateMentors();

        List<MentorEntity> saved = mentorRepository.saveAll(toSave);

        // when
        List<MentorEntity> found = mentorRepository.findAllByUserId(2);

        // then
        Assertions.assertEquals(found.size(), 1);
    }

    @Test
    void save_happyPath() {
        // given
        MentorEntity toSave = generateMentor();

        // when
        MentorEntity saved = mentorRepository.save(toSave);

        // then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
        Assertions.assertEquals(toSave, saved);
    }

    @Test
    void update_happyPath() {
        // given
        MentorEntity toSave = generateMentor();
        MentorEntity saved = mentorRepository.save(toSave);
        MentorEntity found = mentorRepository.findOneById(saved.getId());

        // when
        saved.setUserId(2);
        mentorRepository.save(toSave);
        MentorEntity updated = mentorRepository.findOneById(saved.getId());

        // then
        Assertions.assertEquals(found.getId(), updated.getId());
        Assertions.assertNotEquals(found.getUserId(), updated.getUserId());
        Assertions.assertNotEquals(found, updated);
    }

    @Test
    void delete_happyPath() {
        // given
        MentorEntity toSave = generateMentor();
        MentorEntity saved = mentorRepository.save(toSave);

        // when
        mentorRepository.deleteById(saved.getId());
        MentorEntity foundDeleted = mentorRepository.findOneById(saved.getId());

        // then
        Assertions.assertNull(foundDeleted);
    }

}
