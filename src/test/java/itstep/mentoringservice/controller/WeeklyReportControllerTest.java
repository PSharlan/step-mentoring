package itstep.mentoringservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import itstep.mentoringservice.dto.weeklyReport.MentorWeeklyReportCreateDto;
import itstep.mentoringservice.dto.weeklyReport.StudentWeeklyReportCreateDto;
import itstep.mentoringservice.dto.weeklyReport.WeeklyReportFullDto;
import itstep.mentoringservice.entity.LearningProcessEntity;
import itstep.mentoringservice.entity.WeeklyReportEntity;
import itstep.mentoringservice.integration.AbstractIntegrationTest;
import itstep.mentoringservice.repository.LearningProcessRepository;
import itstep.mentoringservice.repository.MentorRepository;
import itstep.mentoringservice.repository.PlanRepository;
import itstep.mentoringservice.repository.WeeklyReportRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static itstep.mentoringservice.EntityGenerationUtils.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class WeeklyReportControllerTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() throws Exception {
        // given
        WeeklyReportEntity saved = weeklyReportRepository.save(generateWeeklyReportWithLearningProcess());

        // when
        MvcResult result = mockMvc.perform(get("/weeklyReports/{id}", saved.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        WeeklyReportFullDto foundDto = objectMapper.readValue(bytes, WeeklyReportFullDto.class);

        // then
        Assertions.assertNotNull(foundDto);
        Assertions.assertNotNull(foundDto.getId());
        Assertions.assertEquals(saved.getMarkForStudent(), foundDto.getMarkForStudent());
        Assertions.assertEquals(saved.getMarkForMentor(), foundDto.getMarkForMentor());
        Assertions.assertEquals(saved.getCommentForStudent(), foundDto.getCommentForStudent());
        Assertions.assertEquals(saved.getCommentForMentor(), foundDto.getCommentForMentor());
    }

    @Test
    void findById_whenNotFound() throws Exception {
        // given
        Integer notExistingId = 10000;

        // when
        mockMvc.perform(get("/weeklyReports/{id}", notExistingId))
                .andExpect(status().isNotFound());

        // then
    }

    @Test
    void findAll_happyPath() throws Exception {
        // given
        List<WeeklyReportEntity> saved = weeklyReportRepository.saveAll(generateWeeklyReportsWithLearningProcess());

        // when
        mockMvc.perform(get("/weeklyReports?page=0&size=3"))
                .andExpect(status().isOk())
                .andReturn();

        // then
    }

    @Test
    void createMentorWeeklyReport_happyPath() throws Exception {
        // given
        WeeklyReportEntity savedEntity = weeklyReportRepository.save(generateWeeklyReportWithLearningProcess());

        MentorWeeklyReportCreateDto createDto = generateMentorWeeklyCreateDtoWithLearningProcess();
        createDto.setId(savedEntity.getId());

        // when
        MvcResult result = mockMvc.perform(post("/weeklyReports/mentor")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getRequest().getContentAsByteArray();
        WeeklyReportFullDto saved = objectMapper.readValue(bytes, WeeklyReportFullDto.class);

        // then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
        Assertions.assertEquals(saved.getMarkForStudent(), createDto.getMarkForStudent());
        Assertions.assertEquals(saved.getCommentForStudent(), createDto.getCommentForStudent());
    }

    @Test
    void createStudentWeeklyReport_happyPath() throws Exception {
        // given
        WeeklyReportEntity savedEntity = weeklyReportRepository.save(generateWeeklyReportWithLearningProcess());

        StudentWeeklyReportCreateDto createDto = generateStudentWeeklyCreateDtoWithLearningProcess();
        createDto.setId(savedEntity.getId());

        // when
        MvcResult result = mockMvc.perform(post("/weeklyReports/student")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getRequest().getContentAsByteArray();
        WeeklyReportFullDto saved = objectMapper.readValue(bytes, WeeklyReportFullDto.class);

        // then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
        Assertions.assertEquals(saved.getMarkForMentor(), createDto.getMarkForMentor());
        Assertions.assertEquals(saved.getCommentForMentor(), createDto.getCommentForMentor());
    }

    @Test
    void delete_happyPath() throws Exception {
        // given
        WeeklyReportEntity saved = weeklyReportRepository.save(generateWeeklyReportWithLearningProcess());

        // when
        mockMvc.perform(delete("/weeklyReports/{id}", saved.getId()))
                .andExpect(status().isOk());

        // then
        mockMvc.perform(get("/weeklyReports/{id}", saved.getId()))
                .andExpect(status().isNotFound());

    }

    @Test
    void delete_whenNotFound() throws Exception {
        // given
        Integer notExistingId = 0;

        // when
        mockMvc.perform(delete("/weeklyReports/{id}", notExistingId))
                .andExpect(status().isNotFound());

        // then
    }

    @Test
    void createMentorWeeklyReport_whenIdNull() throws Exception {
        // given
        MentorWeeklyReportCreateDto createDto = generateMentorWeeklyCreateDtoWithLearningProcess();
        createDto.setId(null);

        // when
        mockMvc.perform(post("/weeklyReports/mentor")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    @Test
    void createMentorWeeklyReport_whenMarkForStudentNull() throws Exception {
        // given
        WeeklyReportEntity savedEntity = weeklyReportRepository.save(generateWeeklyReportWithLearningProcess());

        MentorWeeklyReportCreateDto createDto = generateMentorWeeklyCreateDtoWithLearningProcess();
        createDto.setId(savedEntity.getId());
        createDto.setMarkForStudent(null);

        // when
        mockMvc.perform(post("/weeklyReports/mentor")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    @Test
    void createMentorWeeklyReport_whenLearningProcessIdNull() throws Exception {
        // given
        WeeklyReportEntity savedEntity = weeklyReportRepository.save(generateWeeklyReportWithLearningProcess());

        MentorWeeklyReportCreateDto createDto = generateMentorWeeklyCreateDtoWithLearningProcess();
        createDto.setId(savedEntity.getId());
        createDto.setLearningProcessId(null);

        // when
        mockMvc.perform(post("/weeklyReports/mentor")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    @Test
    void createStudentWeeklyReport_whenIdNull() throws Exception {
        // given
        StudentWeeklyReportCreateDto createDto = generateStudentWeeklyCreateDtoWithLearningProcess();
        createDto.setId(null);

        // when
        mockMvc.perform(post("/weeklyReports/student")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    @Test
    void createStudentWeeklyReport_whenMarkForStudentNull() throws Exception {
        // given
        WeeklyReportEntity savedEntity = weeklyReportRepository.save(generateWeeklyReportWithLearningProcess());

        StudentWeeklyReportCreateDto createDto = generateStudentWeeklyCreateDtoWithLearningProcess();
        createDto.setId(savedEntity.getId());
        createDto.setMarkForMentor(null);

        // when
        mockMvc.perform(post("/weeklyReports/mentor")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    @Test
    void createStudentWeeklyReport_whenLearningProcessIdNull() throws Exception {
        // given
        WeeklyReportEntity savedEntity = weeklyReportRepository.save(generateWeeklyReportWithLearningProcess());

        StudentWeeklyReportCreateDto createDto = generateStudentWeeklyCreateDtoWithLearningProcess();
        createDto.setId(savedEntity.getId());
        createDto.setLearningProcessId(null);

        // when
        mockMvc.perform(post("/weeklyReports/mentor")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    private WeeklyReportEntity generateWeeklyReportWithLearningProcess() {
        LearningProcessEntity learningProcessEntity = generateLearningProcess();
        learningProcessEntity.setPlan(planRepository.save(generatePlan()));
        learningProcessEntity.setMentor(mentorRepository.save(generateMentor()));

        WeeklyReportEntity weeklyReport = generateWeeklyReport();
        weeklyReport.setLearningProcess(learningProcessRepository.save(learningProcessEntity));

        return weeklyReport;
    }

    private List<WeeklyReportEntity> generateWeeklyReportsWithLearningProcess() {
        LearningProcessEntity learningProcessEntity = generateLearningProcess();
        learningProcessEntity.setPlan(planRepository.save(generatePlan()));
        learningProcessEntity.setMentor(mentorRepository.save(generateMentor()));

        LearningProcessEntity savedLearningProcess = learningProcessRepository.save(learningProcessEntity);

        List<WeeklyReportEntity> weeklyReports = generateWeeklyReports();
        for (WeeklyReportEntity weeklyReport : weeklyReports) {
            weeklyReport.setLearningProcess(savedLearningProcess);
        }
        return weeklyReports;
    }

    private MentorWeeklyReportCreateDto generateMentorWeeklyCreateDtoWithLearningProcess() {
        LearningProcessEntity learningProcessEntity = generateLearningProcess();
        learningProcessEntity.setPlan(planRepository.save(generatePlan()));
        learningProcessEntity.setMentor(mentorRepository.save(generateMentor()));

        LearningProcessEntity savedLearningProcess = learningProcessRepository.save(learningProcessEntity);

        MentorWeeklyReportCreateDto createDto = new MentorWeeklyReportCreateDto();
        createDto.setMarkForStudent(10);
        createDto.setCommentForStudent("zxc");
        createDto.setLearningProcessId(savedLearningProcess.getId());

        return createDto;
    }

    private StudentWeeklyReportCreateDto generateStudentWeeklyCreateDtoWithLearningProcess() {
        LearningProcessEntity learningProcessEntity = generateLearningProcess();
        learningProcessEntity.setPlan(planRepository.save(generatePlan()));
        learningProcessEntity.setMentor(mentorRepository.save(generateMentor()));

        LearningProcessEntity savedLearningProcess = learningProcessRepository.save(learningProcessEntity);

        StudentWeeklyReportCreateDto createDto = new StudentWeeklyReportCreateDto();
        createDto.setMarkForMentor(10);
        createDto.setCommentForMentor("zxc");
        createDto.setLearningProcessId(savedLearningProcess.getId());

        return createDto;
    }

    private LearningProcessEntity generateLearningProcessWithMentorAndPlan() {
        LearningProcessEntity entity = generateLearningProcess();
        entity.setMentor(mentorRepository.save(generateMentor()));
        entity.setPlan(planRepository.save(generatePlan()));

        return entity;
    }

}
