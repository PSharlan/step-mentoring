package itstep.mentoringservice.dto.mentor;

import io.swagger.annotations.ApiModelProperty;
import itstep.mentoringservice.dto.learningProcess.LearningProcessFullDto;
import itstep.mentoringservice.dto.plan.PlanFullDto;
import itstep.mentoringservice.dto.subscribeMentorRequest.SubscribeMentorRequestFullDto;
import lombok.Data;

import java.util.List;

@Data
public class MentorFullDto {

    @ApiModelProperty(example = "1", notes = "Id of the current mentor")
    private Integer id;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    private Integer userId;

    @ApiModelProperty(notes = "List of plans for mentor")
    private List<PlanFullDto> plans;

    @ApiModelProperty(notes = "List of learningProcesses for mentor")
    private List<LearningProcessFullDto> learningProcesses;

    @ApiModelProperty(notes = "List of subscribeMentorRequests for mentor")
    private List<SubscribeMentorRequestFullDto> subscribeMentorRequests;

}