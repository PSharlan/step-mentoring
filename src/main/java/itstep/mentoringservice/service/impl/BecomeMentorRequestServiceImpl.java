package itstep.mentoringservice.service.impl;

import itstep.mentoringservice.dto.becomeMentorRequest.BecomeMentorRequestCreateDto;
import itstep.mentoringservice.dto.becomeMentorRequest.BecomeMentorRequestFullDto;
import itstep.mentoringservice.dto.becomeMentorRequest.BecomeMentorRequestUpdateDto;
import itstep.mentoringservice.dto.mentor.MentorCreateDto;
import itstep.mentoringservice.dto.mentor.MentorFullDto;
import itstep.mentoringservice.entity.BecomeMentorRequestEntity;
import itstep.mentoringservice.entity.MentorEntity;
import itstep.mentoringservice.entity.PlanEntity;
import itstep.mentoringservice.entity.enums.RequestStatus;
import itstep.mentoringservice.exception.BecomeMentorRequestNotFoundException;
import itstep.mentoringservice.exception.WrongNumbersOfPlanException;
import itstep.mentoringservice.repository.BecomeMentorRequestRepository;
import itstep.mentoringservice.repository.MentorRepository;
import itstep.mentoringservice.repository.PlanRepository;
import itstep.mentoringservice.service.BecomeMentorRequestService;
import itstep.mentoringservice.service.MentorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static itstep.mentoringservice.entity.enums.RequestStatus.*;
import static itstep.mentoringservice.entity.enums.RequestStatus.PENDING;
import static itstep.mentoringservice.mapper.BecomeMentorRequestMapper.*;

@Slf4j
@Service
public class BecomeMentorRequestServiceImpl implements BecomeMentorRequestService {

    @Autowired
    private BecomeMentorRequestRepository becomeMentorRequestRepository;

    @Autowired
    private PlanRepository planRepository;

    @Autowired
    private MentorService mentorService;

    @Autowired
    private MentorRepository mentorRepository;

    @Override
    @Transactional(readOnly = true)
    public BecomeMentorRequestFullDto findById(Integer id) {
        BecomeMentorRequestEntity foundEntity = becomeMentorRequestRepository.findById(id)
                .orElseThrow(() -> new BecomeMentorRequestNotFoundException(id));

        log.info("BecomeMentorRequestServiceImpl -> found becomeMentorRequest {} by id: {}", foundEntity, id);
        return BECOME_MENTOR_REQUEST_MAPPER.toFullDto(foundEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<BecomeMentorRequestFullDto> findAll(Integer page, Integer size) {
        Page<BecomeMentorRequestEntity> foundEntities = becomeMentorRequestRepository.findAll(PageRequest.of(page, size));

        Page<BecomeMentorRequestFullDto> dto = foundEntities.map(entity -> BECOME_MENTOR_REQUEST_MAPPER.toFullDto(entity));

        log.info("BecomeMentorRequestServiceImpl -> found {} becomeMentorRequests", dto.getNumberOfElements());
        return dto;
    }

    @Override
    @Transactional(readOnly = true)
    public List<BecomeMentorRequestFullDto> findAllByStatus(RequestStatus requestStatus) {
        List<BecomeMentorRequestEntity> foundEntities = becomeMentorRequestRepository.findAllByStatus(requestStatus);

        log.info("BecomeMentorRequestServiceImpl -> found {} becomeMentorRequests by status", foundEntities.size());
        return BECOME_MENTOR_REQUEST_MAPPER.toFullDtoList(foundEntities);
    }

    @Override
    @Transactional(readOnly = true)
    public List<BecomeMentorRequestFullDto> findAllByPlanId(Integer planId) {
        List<BecomeMentorRequestEntity> foundEntities = becomeMentorRequestRepository.findAllByPlan(planId);

        log.info("BecomeMentorRequestServiceImpl -> found {} becomeMentorRequests by plan", foundEntities.size());
        return BECOME_MENTOR_REQUEST_MAPPER.toFullDtoList(foundEntities);
    }

    @Override
    @Transactional
    public BecomeMentorRequestFullDto create(BecomeMentorRequestCreateDto createDto) {
        BecomeMentorRequestEntity entityToSave = BECOME_MENTOR_REQUEST_MAPPER.toEntity(createDto);

        List<PlanEntity> plans = planRepository.findAllById(createDto.getPlanIds());

        validateRequestedPlansExist(createDto);

        entityToSave.setStatus(PENDING);
        entityToSave.setPlans(plans);

        BecomeMentorRequestEntity savedEntity = becomeMentorRequestRepository.save(entityToSave);

        log.info("BecomeMentorRequestServiceImpl -> becomeMentorRequest {} successfully saved", savedEntity);
        return BECOME_MENTOR_REQUEST_MAPPER.toFullDto(savedEntity);
    }

    @Override
    @Transactional
    public BecomeMentorRequestFullDto update(BecomeMentorRequestUpdateDto updateDto) {
        BecomeMentorRequestEntity entityToUpdate = becomeMentorRequestRepository.findById(updateDto.getId())
                .orElseThrow(() -> new BecomeMentorRequestNotFoundException(updateDto.getId()));

        entityToUpdate.setStatus(updateDto.getStatus());

        BecomeMentorRequestEntity updatedEntity = becomeMentorRequestRepository.save(entityToUpdate);

        createMentorAfterAcceptedBecomeMentorRequest(updatedEntity);

        log.info("BecomeMentorRequestServiceImpl -> becomeMentorRequest {} successfully updated", updatedEntity);
        return BECOME_MENTOR_REQUEST_MAPPER.toFullDto(updatedEntity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        becomeMentorRequestRepository.findById(id)
                .orElseThrow(() -> new BecomeMentorRequestNotFoundException(id));

        becomeMentorRequestRepository.deleteById(id);
    }

    private void validateRequestedPlansExist(BecomeMentorRequestCreateDto createDto) {
        List<PlanEntity> existingPlans = planRepository.findAllById(createDto.getPlanIds());

        if (existingPlans.size() != createDto.getPlanIds().size()) {
            throw new WrongNumbersOfPlanException("Wrong numbers of plans, requested plans: " + createDto.getPlanIds().size() + " found plans: " + existingPlans.size());
        }
    }

    private void createMentorAfterAcceptedBecomeMentorRequest(BecomeMentorRequestEntity becomeMentorRequest) {
        if (becomeMentorRequest.getStatus() != ACCEPTED) {
            return;
        }

        List<MentorEntity> mentors = mentorRepository.findAllByUserId(becomeMentorRequest.getUserId());
        for (MentorEntity mentorEntity : mentors) {

            if (mentorEntity.getUserId().equals(becomeMentorRequest.getUserId())) {
                mentorEntity.getPlans().addAll(becomeMentorRequest.getPlans());
                mentorRepository.save(mentorEntity);

                log.info("BecomeMentorRequestServiceImpl -> mentor {} successfully updated after accepted becomeMentorRequest", mentorEntity);

            } else {
                MentorCreateDto mentorCreateDto = new MentorCreateDto();

                for (PlanEntity planEntity : becomeMentorRequest.getPlans()) {
                    List<Integer> plansIds = new ArrayList<>();
                    plansIds.add(planEntity.getId());

                    mentorCreateDto.setPlanIds(plansIds);
                    mentorCreateDto.setUserId(becomeMentorRequest.getUserId());
                }

                MentorFullDto mentor = mentorService.create(mentorCreateDto);
                log.info("BecomeMentorRequestServiceImpl -> mentor {} successfully saved after accepted becomeMentorRequest", mentor);
            }
        }
    }
}


