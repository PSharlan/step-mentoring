package itstep.mentoringservice.service;

import itstep.mentoringservice.EntityGenerationUtils;
import itstep.mentoringservice.dto.weeklyReport.MentorWeeklyReportCreateDto;
import itstep.mentoringservice.dto.weeklyReport.StudentWeeklyReportCreateDto;
import itstep.mentoringservice.dto.weeklyReport.WeeklyReportFullDto;
import itstep.mentoringservice.entity.LearningProcessEntity;
import itstep.mentoringservice.entity.MentorEntity;
import itstep.mentoringservice.entity.PlanEntity;
import itstep.mentoringservice.entity.WeeklyReportEntity;
import itstep.mentoringservice.entity.enums.LearningProcessStatus;
import itstep.mentoringservice.exception.LastWeeklyReportNotFoundException;
import itstep.mentoringservice.exception.LearningProcessNotFoundException;
import itstep.mentoringservice.exception.WeeklyReportNotFoundException;
import itstep.mentoringservice.integration.AbstractIntegrationTest;
import itstep.mentoringservice.repository.LearningProcessRepository;
import itstep.mentoringservice.repository.MentorRepository;
import itstep.mentoringservice.repository.PlanRepository;
import itstep.mentoringservice.repository.WeeklyReportRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import static itstep.mentoringservice.EntityGenerationUtils.*;
import static itstep.mentoringservice.EntityGenerationUtils.generatePlan;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class WeeklyReportServiceTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() {
        // given
        WeeklyReportEntity entity = generateWeeklyReportWithLearningProcess();

        WeeklyReportEntity saved = weeklyReportRepository.save(entity);

        // when
        WeeklyReportFullDto foundDto = weeklyReportService.findById(saved.getId());

        // then
        Assertions.assertNotNull(foundDto);
        Assertions.assertEquals(saved.getId(), foundDto.getId());
        Assertions.assertEquals(saved.getCommentForMentor(), foundDto.getCommentForMentor());
        Assertions.assertEquals(saved.getCommentForStudent(), foundDto.getCommentForStudent());
        Assertions.assertEquals(saved.getMarkForMentor(), foundDto.getMarkForMentor());
        Assertions.assertEquals(saved.getMarkForStudent(), foundDto.getMarkForStudent());
    }

    @Test
    void findAll_happyPath() {
        // given
        List<WeeklyReportEntity> toSave = generateWeeklyReportsWithLearningProcess();

        List<WeeklyReportEntity> saved = weeklyReportRepository.saveAll(toSave);

        // when
        Page<WeeklyReportFullDto> foundPage = weeklyReportService.findAll(0,3);
        List<WeeklyReportFullDto> weeklyReports = foundPage.getContent();

        // then
        Assertions.assertNotNull(foundPage);
        Assertions.assertEquals(saved.size(), weeklyReports.size());
    }

    @Test
    void createMentorWeeklyReport_happyPath() {
        // given
        WeeklyReportEntity savedEntity = weeklyReportRepository.save(generateWeeklyReportWithLearningProcess());

        MentorWeeklyReportCreateDto createDto = generateMentorWeeklyCreateDtoWithLearningProcess();
        createDto.setId(savedEntity.getId());

        // when
        WeeklyReportFullDto savedDto = weeklyReportService.create(createDto);

        // then
        Assertions.assertNotNull(savedDto.getId());
        Assertions.assertNotNull(savedDto.getCreateTime());
        Assertions.assertEquals(savedDto.getCommentForStudent(), createDto.getCommentForStudent());
        Assertions.assertEquals(savedDto.getMarkForStudent(), createDto.getMarkForStudent());
    }

    @Test
    void createStudentWeeklyReport_happyPath() {
        // given
        WeeklyReportEntity savedEntity = weeklyReportRepository.save(generateWeeklyReportWithLearningProcess());

        StudentWeeklyReportCreateDto createDto = generateStudentWeeklyCreateDtoWithLearningProcess();
        createDto.setId(savedEntity.getId());

        // when
        WeeklyReportFullDto savedDto = weeklyReportService.create(createDto);

        // then
        Assertions.assertNotNull(savedDto.getId());
        Assertions.assertNotNull(savedDto.getCreateTime());
        Assertions.assertEquals(savedDto.getCommentForMentor(), createDto.getCommentForMentor());
        Assertions.assertEquals(savedDto.getMarkForMentor(), createDto.getMarkForMentor());
    }

    @Test
    void deleteById_happyPath() {
        // given
        WeeklyReportEntity savedEntity = weeklyReportRepository.save(generateWeeklyReportWithLearningProcess());

        StudentWeeklyReportCreateDto createDto = generateStudentWeeklyCreateDtoWithLearningProcess();
        createDto.setId(savedEntity.getId());

        WeeklyReportFullDto savedDto = weeklyReportService.create(createDto);

        // when
        weeklyReportService.deleteById(savedDto.getId());

        Exception exception = assertThrows(WeeklyReportNotFoundException.class,
                () -> weeklyReportService.findById(savedDto.getId()));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(savedDto.getId())));
    }

    @Test
    void generateInitialReportsForActiveProcess_happyPath() {
        // given
        LearningProcessEntity learningProcessEntity = generateLearningProcessWithMentorAndPlan();
        learningProcessEntity.setStatus(LearningProcessStatus.PROCESS);
        LearningProcessEntity savedLearningProcess = learningProcessRepository.save(learningProcessEntity);

        List<WeeklyReportEntity> weeklyReportEntities = generateWeeklyReports();
        for (WeeklyReportEntity entity : weeklyReportEntities) {
            entity.setCreateTime(Instant.now().minus(9, ChronoUnit.DAYS));
            entity.setLearningProcess(savedLearningProcess);
        }

        List<WeeklyReportEntity> savedWeeklyReports = weeklyReportRepository.saveAll(weeklyReportEntities);

        // when
        weeklyReportService.generateInitialReportsForActiveProcess();

        List<WeeklyReportEntity> foundWeeklyReports = weeklyReportRepository.findAll();

        // then
        Assertions.assertEquals(savedWeeklyReports.size(), 2);
        Assertions.assertEquals(foundWeeklyReports.size(), 3);
        Assertions.assertNotEquals(savedWeeklyReports.size(), foundWeeklyReports.size());
    }

    @Test
    void findById_whenNotFound() {
        // given
        Integer notExistingId = 100;

        // when
        Exception exception = assertThrows(WeeklyReportNotFoundException.class,
                () -> weeklyReportService.findById(notExistingId));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }


    @Test
    void createMentorWeeklyReport_whenLearningProcessNotFound() {
        // given
        WeeklyReportEntity savedEntity = weeklyReportRepository.save(generateWeeklyReportWithLearningProcess());

        MentorWeeklyReportCreateDto createDto = generateMentorWeeklyCreateDtoWithLearningProcess();

        createDto.setId(savedEntity.getId());
        createDto.setLearningProcessId(0);

        // when
        Exception exception = assertThrows(LearningProcessNotFoundException.class,
                () -> weeklyReportService.create(createDto));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(createDto.getLearningProcessId())));
    }

    @Test
    void createStudentWeeklyReport_whenLearningProcessNotFound() {
        // given
        WeeklyReportEntity savedEntity = weeklyReportRepository.save(generateWeeklyReportWithLearningProcess());

        StudentWeeklyReportCreateDto createDto = generateStudentWeeklyCreateDtoWithLearningProcess();

        createDto.setId(savedEntity.getId());
        createDto.setLearningProcessId(0);

        // when
        Exception exception = assertThrows(LearningProcessNotFoundException.class,
                () -> weeklyReportService.create(createDto));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(createDto.getLearningProcessId())));
    }

    @Test
    void delete_whenNotFound() {
        // given
        Integer notExistingId = 100;

        // when
        Exception exception = Assertions.assertThrows(WeeklyReportNotFoundException.class,
                () -> weeklyReportService.deleteById(notExistingId));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void generateInitialReportsForActiveProcess_whenLastWeeklyReportNotFound() {
        // given
        LearningProcessEntity learningProcessEntity = generateLearningProcessWithMentorAndPlan();
        learningProcessEntity.setStatus(LearningProcessStatus.PROCESS);
        LearningProcessEntity savedLearningProcess = learningProcessRepository.save(learningProcessEntity);

        // when
        Exception exception = assertThrows(LastWeeklyReportNotFoundException.class,
                () -> weeklyReportService.generateInitialReportsForActiveProcess());

        // then
        Assertions.assertTrue(exception.getMessage().contains("Not found last weekReport"));
    }

    private WeeklyReportEntity generateWeeklyReportWithLearningProcess() {
        LearningProcessEntity learningProcessEntity = generateLearningProcess();
        learningProcessEntity.setPlan(planRepository.save(generatePlan()));
        learningProcessEntity.setMentor(mentorRepository.save(generateMentor()));

        WeeklyReportEntity weeklyReport = generateWeeklyReport();
        weeklyReport.setLearningProcess(learningProcessRepository.save(learningProcessEntity));

        return weeklyReport;
    }

    private List<WeeklyReportEntity> generateWeeklyReportsWithLearningProcess() {
        LearningProcessEntity learningProcessEntity = generateLearningProcess();
        learningProcessEntity.setPlan(planRepository.save(generatePlan()));
        learningProcessEntity.setMentor(mentorRepository.save(generateMentor()));

        LearningProcessEntity savedLearningProcess = learningProcessRepository.save(learningProcessEntity);

        List<WeeklyReportEntity> weeklyReports = generateWeeklyReports();
        for (WeeklyReportEntity weeklyReport : weeklyReports) {
            weeklyReport.setLearningProcess(savedLearningProcess);
        }
        return weeklyReports;
    }

    private MentorWeeklyReportCreateDto generateMentorWeeklyCreateDtoWithLearningProcess() {
        LearningProcessEntity learningProcessEntity = generateLearningProcess();
        learningProcessEntity.setPlan(planRepository.save(generatePlan()));
        learningProcessEntity.setMentor(mentorRepository.save(generateMentor()));

        LearningProcessEntity savedLearningProcess = learningProcessRepository.save(learningProcessEntity);

        MentorWeeklyReportCreateDto createDto = new MentorWeeklyReportCreateDto();
        createDto.setMarkForStudent(10);
        createDto.setCommentForStudent("zxc");
        createDto.setLearningProcessId(savedLearningProcess.getId());

        return createDto;
    }

    private StudentWeeklyReportCreateDto generateStudentWeeklyCreateDtoWithLearningProcess() {
        LearningProcessEntity learningProcessEntity = generateLearningProcess();
        learningProcessEntity.setPlan(planRepository.save(generatePlan()));
        learningProcessEntity.setMentor(mentorRepository.save(generateMentor()));

        LearningProcessEntity savedLearningProcess = learningProcessRepository.save(learningProcessEntity);

        StudentWeeklyReportCreateDto createDto = new StudentWeeklyReportCreateDto();
        createDto.setMarkForMentor(10);
        createDto.setCommentForMentor("zxc");
        createDto.setLearningProcessId(savedLearningProcess.getId());

        return createDto;
    }

    private LearningProcessEntity generateLearningProcessWithMentorAndPlan() {
        LearningProcessEntity entity = generateLearningProcess();
        entity.setMentor(mentorRepository.save(generateMentor()));
        entity.setPlan(planRepository.save(generatePlan()));

        return entity;
    }
}
