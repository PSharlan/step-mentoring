package itstep.mentoringservice.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import itstep.mentoringservice.dto.becomeMentorRequest.BecomeMentorRequestCreateDto;
import itstep.mentoringservice.dto.becomeMentorRequest.BecomeMentorRequestFullDto;
import itstep.mentoringservice.dto.becomeMentorRequest.BecomeMentorRequestUpdateDto;
import itstep.mentoringservice.entity.enums.RequestStatus;
import itstep.mentoringservice.service.BecomeMentorRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@Api(description = "Controller dedicated to manage becomeMentorRequests")
@RequestMapping("/becomeMentorRequests")
@RestController
public class BecomeMentorRequestController {

    @Autowired
    private BecomeMentorRequestService becomeMentorRequestService;

    @GetMapping("/{id}")
    @ApiOperation(value = "Find one becomeMentorRequest by id", notes = "Existing id must be specified")
    public BecomeMentorRequestFullDto getById(@PathVariable Integer id) {
        return becomeMentorRequestService.findById(id);
    }

    @GetMapping("")
    @ApiOperation(value = "Find all becomeMentorRequests")
    public Page<BecomeMentorRequestFullDto> findAll(@RequestParam Integer page, @RequestParam Integer size) {
        return becomeMentorRequestService.findAll(page, size);
    }

    @GetMapping("/status/{status}")
    @ApiOperation(value = "Find all becomeMentorRequests by status", notes = "Existing status must be specified")
    public List<BecomeMentorRequestFullDto> findAllByStatus(@PathVariable RequestStatus status) {
        return becomeMentorRequestService.findAllByStatus(status);
    }

    @GetMapping("/plans/{planId}")
    @ApiOperation(value = "Find all becomeMentorRequests by planId", notes = "Existing planId must be specified")
    public List<BecomeMentorRequestFullDto> findAllByPlanId(@PathVariable Integer planId) {
        return becomeMentorRequestService.findAllByPlanId(planId);
    }

    @PostMapping("")
    @ApiOperation(value = "Create becomeMentorRequest")
    public BecomeMentorRequestFullDto create(@Valid @RequestBody BecomeMentorRequestCreateDto createDto) {
        return becomeMentorRequestService.create(createDto);
    }

    @PutMapping("")
    @ApiOperation(value = "Update becomeMentorRequest")
    public BecomeMentorRequestFullDto update(@Valid @RequestBody BecomeMentorRequestUpdateDto updateDto) {
        return becomeMentorRequestService.update(updateDto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete becomeMentorRequest by id", notes = "Existing id must be specified")
    public void delete(@PathVariable Integer id) {
        becomeMentorRequestService.deleteById(id);
    }



}
