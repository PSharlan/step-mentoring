package itstep.mentoringservice.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import itstep.mentoringservice.dto.learningProcess.LearningProcessCreateDto;
import itstep.mentoringservice.dto.learningProcess.LearningProcessFullDto;
import itstep.mentoringservice.dto.learningProcess.LearningProcessUpdateDto;
import itstep.mentoringservice.entity.enums.LearningProcessStatus;
import itstep.mentoringservice.service.LearningProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(description = "Controller dedicated to manage learningProcesses")
@RequestMapping("/learningProcesses")
@RestController
public class LearningProcessController {

    @Autowired
    private LearningProcessService learningProcessService;

    @GetMapping("/{id}")
    @ApiOperation(value = "Find one learningProcess by id", notes = "Existing id must be specified")
    public LearningProcessFullDto getById(@PathVariable Integer id) {
        return learningProcessService.findById(id);
    }

    @GetMapping("")
    @ApiOperation(value = "Find all learningProcesses")
    public Page<LearningProcessFullDto> findAll(@RequestParam Integer page, Integer size) {
        return learningProcessService.findAll(page, size);
    }

    @GetMapping("/status/{status}")
    @ApiOperation(value = "Find all learningProcesses by status", notes = "Existing status must be specified")
    public List<LearningProcessFullDto> findAllByStatus(@PathVariable LearningProcessStatus status) {
        return learningProcessService.findAllByStatus(status);
    }

    @PostMapping("")
    @ApiOperation(value = "Create learningProcess")
    public LearningProcessFullDto create(@Valid @RequestBody LearningProcessCreateDto createDto) {
        return learningProcessService.create(createDto);
    }

    @PutMapping("")
    @ApiOperation(value = "Update learningProcess")
    public LearningProcessFullDto update(@Valid @RequestBody LearningProcessUpdateDto updateDto) {
        return learningProcessService.update(updateDto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete learningProcess by id", notes = "Existing id must be specified")
    public void delete(@PathVariable Integer id) {
        learningProcessService.deleteById(id);
    }

}
