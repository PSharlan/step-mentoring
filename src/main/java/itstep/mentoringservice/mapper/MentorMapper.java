package itstep.mentoringservice.mapper;

import itstep.mentoringservice.dto.mentor.MentorCreateDto;
import itstep.mentoringservice.dto.mentor.MentorFullDto;
import itstep.mentoringservice.entity.MentorEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = {PlanMapper.class, LearningProcessMapper.class, BecomeMentorRequestMapper.class})
public interface MentorMapper {

    MentorMapper MENTOR_MAPPER = Mappers.getMapper(MentorMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "plans", ignore = true)
    @Mapping(target = "learningProcesses", ignore = true)
    @Mapping(target = "subscribeMentorRequests", ignore = true)
    MentorEntity toEntity(MentorCreateDto createDto);

    MentorFullDto toFullDto(MentorEntity entity);

    List<MentorFullDto> toFullDtoList(List<MentorEntity> entities);

}
