package itstep.mentoringservice.repository;

import itstep.mentoringservice.EntityGenerationUtils;
import itstep.mentoringservice.entity.LearningProcessEntity;
import itstep.mentoringservice.entity.MentorEntity;
import itstep.mentoringservice.entity.PlanEntity;
import itstep.mentoringservice.entity.WeeklyReportEntity;
import itstep.mentoringservice.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static itstep.mentoringservice.EntityGenerationUtils.*;

public class LearningProcessRepositoryTests extends AbstractIntegrationTest {

    @Test
    void create_happyPath(){
        //given
        LearningProcessEntity learningProcess = generateLearningProcess();
        MentorEntity mentor = generateMentor();
        PlanEntity planEntity = generatePlan();

        MentorEntity savedMentor = mentorRepository.save(mentor);
        PlanEntity savedPlan = planRepository.save(planEntity);

        learningProcess.setPlan(savedPlan);
        learningProcess.setMentor(savedMentor);

        //when
        LearningProcessEntity savedProcess = learningProcessRepository.save(learningProcess);

        //then
        Assertions.assertNotNull(savedProcess);
    }

    @Test
    void findById_happyPath(){
        //given
        LearningProcessEntity learningProcess = generateLearningProcess();
        MentorEntity mentor = generateMentor();
        PlanEntity planEntity = generatePlan();

        MentorEntity savedMentor = mentorRepository.save(mentor);
        PlanEntity savedPlan = planRepository.save(planEntity);

        learningProcess.setPlan(savedPlan);
        learningProcess.setMentor(savedMentor);

        LearningProcessEntity savedProcess = learningProcessRepository.save(learningProcess);
        //when
        LearningProcessEntity foundProcess = learningProcessRepository.findOneById(savedProcess.getId());

        //then
        Assertions.assertEquals(savedProcess.getId(), foundProcess.getId());
    }

    @Test
    void findAll_happyPath(){
        //given
        List<LearningProcessEntity> learningProcesses = generateLearningProcesses();
        MentorEntity mentor = generateMentor();
        PlanEntity planEntity = generatePlan();

        MentorEntity savedMentor = mentorRepository.save(mentor);
        PlanEntity savedPlan = planRepository.save(planEntity);

        for(LearningProcessEntity learningProcess : learningProcesses){
            learningProcess.setMentor(savedMentor);
            learningProcess.setPlan(savedPlan);
        }

        List<LearningProcessEntity> savedProcesses = learningProcessRepository.saveAll(learningProcesses);
        
        //when
        List<LearningProcessEntity> foundProcesses = learningProcessRepository.findAll();

        //then
        Assertions.assertEquals(savedProcesses.size(), foundProcesses.size());
    }

    @Test
    void deleteById_happyPath_mentorWillRemain_WeeklyRatesWillBeDeleted(){
        //given
        LearningProcessEntity learningProcess = generateLearningProcess();
        List<WeeklyReportEntity> reports = generateWeeklyReports();
        MentorEntity mentor = generateMentor();
        PlanEntity planEntity = generatePlan();

        MentorEntity savedMentor = mentorRepository.save(mentor);
        PlanEntity savedPlan = planRepository.save(planEntity);

        learningProcess.setPlan(savedPlan);
        learningProcess.setMentor(savedMentor);
        LearningProcessEntity savedProcess = learningProcessRepository.save(learningProcess);

        for(WeeklyReportEntity report : reports){
            report.setLearningProcess(savedProcess);
        }
        List<WeeklyReportEntity> savedReports = weeklyReportRepository.saveAll(reports);
        learningProcess.setWeeklyReports(savedReports);

        Assertions.assertNotNull(savedProcess);

        //when
        learningProcessRepository.deleteById(savedProcess.getId());

        //then
        Assertions.assertNull(learningProcessRepository.findOneById(savedProcess.getId()));
        Assertions.assertNotNull(mentorRepository.findOneById(savedMentor.getId()));
        for(WeeklyReportEntity report : savedReports){
            Assertions.assertNull(weeklyReportRepository.findOneById(report.getId()));
        }
    }

    @Test
    void update_happyPath(){
        LearningProcessEntity learningProcess = generateLearningProcess();
        MentorEntity mentor = generateMentor();
        PlanEntity planEntity = generatePlan();

        MentorEntity savedMentor = mentorRepository.save(mentor);
        PlanEntity savedPlan = planRepository.save(planEntity);

        learningProcess.setPlan(savedPlan);
        learningProcess.setMentor(savedMentor);

        LearningProcessEntity savedProcess = learningProcessRepository.save(learningProcess);

        //when
        PlanEntity newPlan = generatePlan();
        String newDesc = "NEW PLAN";
        newPlan.setDescription(newDesc);
        PlanEntity newSavedPlan = planRepository.save(newPlan);

        savedProcess.setPlan(newPlan);

        LearningProcessEntity updatedProcess = learningProcessRepository.save(savedProcess);

        //then
        Assertions.assertNotNull(updatedProcess);
        Assertions.assertEquals(newDesc, updatedProcess.getPlan().getDescription());
        Assertions.assertEquals(savedProcess.getId(), updatedProcess.getId());
    }

    @Test
    void deleteAll_happyPath_mentorWillRemain_WeeklyRatesWillBeDeleted(){
        //given
        List<LearningProcessEntity> learningProcesses = generateLearningProcesses();
        List<WeeklyReportEntity> reports = generateWeeklyReports();
        MentorEntity mentor = generateMentor();
        PlanEntity planEntity = generatePlan();

        MentorEntity savedMentor = mentorRepository.save(mentor);
        PlanEntity savedPlan = planRepository.save(planEntity);

        for(LearningProcessEntity learningProcess : learningProcesses){
            learningProcess.setMentor(savedMentor);
            learningProcess.setPlan(savedPlan);
        }
        List<LearningProcessEntity> savedProcesses = learningProcessRepository.saveAll(learningProcesses);

        for(WeeklyReportEntity report : reports){
            report.setLearningProcess(savedProcesses.get(0));
        }

        List<WeeklyReportEntity> savedReports = weeklyReportRepository.saveAll(reports);

        Assertions.assertNotNull(savedProcesses);

        //when
        learningProcessRepository.deleteAll();

        //then
        Assertions.assertEquals(0, learningProcessRepository.findAll().size());
        Assertions.assertNotNull(mentorRepository.findOneById(savedMentor.getId()));
        for(WeeklyReportEntity report : savedReports){
            Assertions.assertNull(weeklyReportRepository.findOneById(report.getId()));
        }
    }
}
