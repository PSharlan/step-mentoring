package itstep.mentoringservice.service.impl;

import itstep.mentoringservice.dto.weeklyReport.MentorWeeklyReportCreateDto;
import itstep.mentoringservice.dto.weeklyReport.StudentWeeklyReportCreateDto;
import itstep.mentoringservice.dto.weeklyReport.WeeklyReportFullDto;
import itstep.mentoringservice.entity.LearningProcessEntity;
import itstep.mentoringservice.entity.WeeklyReportEntity;
import itstep.mentoringservice.entity.enums.LearningProcessStatus;
import itstep.mentoringservice.exception.LastWeeklyReportNotFoundException;
import itstep.mentoringservice.exception.LearningProcessNotFoundException;
import itstep.mentoringservice.exception.WeeklyReportNotFoundException;
import itstep.mentoringservice.repository.LearningProcessRepository;
import itstep.mentoringservice.repository.WeeklyReportRepository;
import itstep.mentoringservice.service.WeeklyReportService;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import java.util.Arrays;
import java.util.List;

import static itstep.mentoringservice.mapper.WeeklyReportMapper.WEEKLY_REPORT_MAPPER;

@Slf4j
@Service
public class WeeklyReportServiceImpl implements WeeklyReportService {

    @Autowired
    private WeeklyReportRepository weeklyReportRepository;

    @Autowired
    private LearningProcessRepository learningProcessRepository;

    @Value("${scheduler.sending-reports-interval-minutes}")
    private Long sendingReportsIntervalMinutes;

    @Override
    @Transactional(readOnly = true)
    public WeeklyReportFullDto findById(Integer id) {
        WeeklyReportEntity foundEntity = weeklyReportRepository.findById(id)
                .orElseThrow(() -> new WeeklyReportNotFoundException(id));

        log.info("WeeklyReportServiceImpl -> found weeklyReport: {} by id {}", foundEntity, id);
        return WEEKLY_REPORT_MAPPER.toFullDto(foundEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<WeeklyReportFullDto> findAll(Integer page, Integer size) {
        Page<WeeklyReportEntity> foundEntities = weeklyReportRepository.findAll(PageRequest.of(page, size));

        Page<WeeklyReportFullDto> dto = foundEntities.map(entity -> WEEKLY_REPORT_MAPPER.toFullDto(entity));

        log.info("WeeklyReportServiceImpl -> found {} weeklyReports", dto.getNumberOfElements());
        return dto;
    }

    @Override
    @Transactional
    public WeeklyReportFullDto create(MentorWeeklyReportCreateDto createDto) {
        WeeklyReportEntity entityToSave = weeklyReportRepository.findById(createDto.getId())
                .orElseThrow(() -> new WeeklyReportNotFoundException(createDto.getId()));

        LearningProcessEntity learningProcessEntity = learningProcessRepository.findById(createDto.getLearningProcessId())
                .orElseThrow(() -> new LearningProcessNotFoundException(createDto.getLearningProcessId()));

        entityToSave.setLearningProcess(learningProcessEntity);
        entityToSave.setCreateTime(Instant.now());
        entityToSave.setMarkForStudent(createDto.getMarkForStudent());
        entityToSave.setCommentForStudent(createDto.getCommentForStudent());

        WeeklyReportEntity savedEntity = weeklyReportRepository.save(entityToSave);

        log.info("WeeklyReportServiceImpl -> weeklyReport for mentor {} successfully saved", savedEntity);
        return WEEKLY_REPORT_MAPPER.toFullDto(savedEntity);
    }

    @Override
    @Transactional
    public WeeklyReportFullDto create(StudentWeeklyReportCreateDto createDto) {
        WeeklyReportEntity entityToSave = weeklyReportRepository.findById(createDto.getId())
                .orElseThrow(() -> new WeeklyReportNotFoundException(createDto.getId()));

        LearningProcessEntity learningProcessEntity = learningProcessRepository.findById(createDto.getLearningProcessId())
                .orElseThrow(() -> new LearningProcessNotFoundException(createDto.getLearningProcessId()));

        entityToSave.setLearningProcess(learningProcessEntity);
        entityToSave.setCreateTime(Instant.now());
        entityToSave.setMarkForMentor(createDto.getMarkForMentor());
        entityToSave.setCommentForMentor(createDto.getCommentForMentor());

        WeeklyReportEntity savedEntity = weeklyReportRepository.save(entityToSave);

        log.info("WeeklyReportServiceImpl -> weeklyReport for student {} successfully saved", savedEntity);
        return WEEKLY_REPORT_MAPPER.toFullDto(savedEntity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        weeklyReportRepository.findById(id)
                .orElseThrow(() -> new WeeklyReportNotFoundException(id));

        weeklyReportRepository.deleteById(id);
    }

    @Override
    @Transactional
    @Scheduled(cron = "${scheduler.generate-initial-reports-scanning-interval}")
    public void generateInitialReportsForActiveProcess() {
        Instant currentDate = Instant.now();
        List<LearningProcessEntity> activeProcesses = learningProcessRepository.findAllByStatus(LearningProcessStatus.PROCESS);

        for (LearningProcessEntity entity : activeProcesses) {
            List<WeeklyReportEntity> weeklyReports = entity.getWeeklyReports();
            WeeklyReportEntity lastWeekReport = findLastWeeklyReport(weeklyReports);

            Instant endTimeWeekReport = lastWeekReport.getCreateTime().plus(sendingReportsIntervalMinutes, ChronoUnit.MINUTES);

            if(currentDate.isAfter(endTimeWeekReport)) {
                WeeklyReportEntity weeklyReportToSave = new WeeklyReportEntity();
                weeklyReportToSave.setLearningProcess(entity);
                weeklyReportToSave.setCreateTime(Instant.now());
                weeklyReportToSave.setMarkForStudent(0);
                weeklyReportToSave.setMarkForMentor(0);

                weeklyReportRepository.save(weeklyReportToSave);
            }
        }
    }

    private WeeklyReportEntity findLastWeeklyReport(List<WeeklyReportEntity> weeklyReports) {
        return weeklyReports
                .stream()
                .sorted((r1, r2) -> r1.getCreateTime().compareTo(r2.getCreateTime()))
                .findFirst()
                .orElseThrow(() -> new LastWeeklyReportNotFoundException("Not found last weekReport"));
    }
}
