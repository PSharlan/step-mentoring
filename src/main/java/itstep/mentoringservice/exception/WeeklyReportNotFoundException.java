package itstep.mentoringservice.exception;

public class WeeklyReportNotFoundException extends EntityIsNotFoundException{

    public WeeklyReportNotFoundException(Integer id) {
        super("WeeklyReport ", id);
    }
}
