package itstep.mentoringservice.exception;

public class EntityIsNotFoundException extends RuntimeException {

    public EntityIsNotFoundException(String entity, Integer id) {
        super(entity + " was not found by id: " + id);
    }

}
