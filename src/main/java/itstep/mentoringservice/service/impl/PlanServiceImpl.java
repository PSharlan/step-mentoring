package itstep.mentoringservice.service.impl;

import itstep.mentoringservice.dto.plan.PlanCreateDto;
import itstep.mentoringservice.dto.plan.PlanFullDto;
import itstep.mentoringservice.dto.plan.PlanUpdateDto;
import itstep.mentoringservice.entity.PlanEntity;
import itstep.mentoringservice.exception.PlanNotFoundException;
import itstep.mentoringservice.repository.PlanRepository;
import itstep.mentoringservice.service.PlanService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static itstep.mentoringservice.mapper.PlanMapper.PLAN_MAPPER;

@Slf4j
@Service
public class PlanServiceImpl implements PlanService {

    @Autowired
    private PlanRepository planRepository;

    @Override
    @Transactional(readOnly = true)
    public PlanFullDto findById(Integer id) {
        PlanEntity foundEntity = planRepository.findById(id)
                .orElseThrow(() -> new PlanNotFoundException(id));

        log.info("PlanServiceImpl -> found plan: {} by id {}", foundEntity, id);
        return PLAN_MAPPER.toFullDto(foundEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PlanFullDto> findAll(Integer page, Integer size) {
        Page<PlanEntity> foundEntities = planRepository.findAll(PageRequest.of(page, size));

        Page<PlanFullDto> dto = foundEntities.map(entity -> PLAN_MAPPER.toFullDto(entity));

        log.info("PlanServiceImpl -> found {} plans", dto.getNumberOfElements());
        return dto;
    }

    @Override
    @Transactional
    public PlanFullDto create(PlanCreateDto createDto) {
        PlanEntity entityToSave = PLAN_MAPPER.toEntity(createDto);

        PlanEntity savedEntity = planRepository.save(entityToSave);

        log.info("PlanServiceImpl -> plan {} successfully saved", savedEntity);
        return PLAN_MAPPER.toFullDto(savedEntity);
    }

    @Override
    @Transactional
    public PlanFullDto update(PlanUpdateDto updateDto) {
        PlanEntity entityToUpdate = planRepository.findById(updateDto.getId())
                .orElseThrow(() -> new PlanNotFoundException(updateDto.getId()));

        entityToUpdate.setDescription(updateDto.getDescription());
        entityToUpdate.setEstimatedHours(updateDto.getEstimatedHours());
        entityToUpdate.setFileUrl(updateDto.getFileUrl());

        PlanEntity updatedEntity = planRepository.save(entityToUpdate);

        log.info("PlanServiceImpl -> plan {} successfully updated", updatedEntity);
        return PLAN_MAPPER.toFullDto(updatedEntity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        planRepository.findById(id)
                .orElseThrow(() -> new PlanNotFoundException(id));

        planRepository.deleteById(id);
    }
}
