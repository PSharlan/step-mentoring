package itstep.mentoringservice.service;

import itstep.mentoringservice.EntityGenerationUtils;
import itstep.mentoringservice.dto.plan.PlanCreateDto;
import itstep.mentoringservice.dto.plan.PlanFullDto;
import itstep.mentoringservice.dto.plan.PlanUpdateDto;
import itstep.mentoringservice.entity.PlanEntity;
import itstep.mentoringservice.exception.PlanNotFoundException;
import itstep.mentoringservice.integration.AbstractIntegrationTest;
import itstep.mentoringservice.mapper.MapperGenerationUtils;
import itstep.mentoringservice.repository.PlanRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;

import java.util.List;

import static itstep.mentoringservice.EntityGenerationUtils.*;
import static itstep.mentoringservice.mapper.MapperGenerationUtils.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PlanServiceTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() {
        // given
        PlanEntity entity = generatePlan();

        PlanEntity saved = planRepository.save(entity);

        // when
        PlanFullDto foundDto = planService.findById(saved.getId());

        // then
        Assertions.assertNotNull(foundDto);
    }

    @Test
    void findAll_happyPath() {
        // given
        List<PlanEntity> toSave = generatePlans();

        List<PlanEntity> saved = planRepository.saveAll(toSave);

        // when
        Page<PlanFullDto> foundPage = planService.findAll(0, 3);
        List<PlanFullDto> plans = foundPage.getContent();

        // then
        Assertions.assertNotNull(foundPage);
        Assertions.assertEquals(saved.size(), plans.size());
    }

    @Test
    void create_happyPath() {
        // given
        PlanCreateDto createDto = generatePlanCreateDto();

        // when
        PlanFullDto saved = planService.create(createDto);

        // then
        Assertions.assertNotNull(saved.getId());
        Assertions.assertEquals(saved.getDescription(), createDto.getDescription());
        Assertions.assertEquals(saved.getFileUrl(), createDto.getFileUrl());
        Assertions.assertEquals(saved.getEstimatedHours(), createDto.getEstimatedHours());
    }

    @Test
    void update_happyPath() {
        // given
        PlanCreateDto createDto = generatePlanCreateDto();

        PlanFullDto saved = planService.create(createDto);

        PlanUpdateDto updateDto = generatePlanUpdateDto();
        updateDto.setId(saved.getId());

        // when
        PlanFullDto updated = planService.update(updateDto);

        // then
        Assertions.assertNotNull(updated);
        Assertions.assertEquals(saved.getId(), updated.getId());
        Assertions.assertNotEquals(saved.getDescription(), updated.getDescription());
        Assertions.assertNotEquals(saved.getFileUrl(), updated.getFileUrl());
        Assertions.assertNotEquals(saved.getEstimatedHours(), updated.getEstimatedHours());
    }

    @Test
    void deleteById_happyPath() {
        // given
        PlanFullDto saved = planService.create(generatePlanCreateDto());

        // when
        planService.deleteById(saved.getId());

        Exception exception = assertThrows(PlanNotFoundException.class,
                () -> planService.findById(saved.getId()));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(saved.getId())));
    }

    @Test
    void findById_whenNotFound() {
        // given
        Integer notExistingId = 100;

        // when
        Exception exception = assertThrows(PlanNotFoundException.class,
                () -> planService.findById(notExistingId));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void update_whenNotFound() {
        // given
        PlanUpdateDto updateDto = generatePlanUpdateDto();

        Integer notExistingId = 100;
        updateDto.setId(notExistingId);

        // when
        Exception exception = Assertions.assertThrows(PlanNotFoundException.class,
                () -> planService.update(updateDto));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(updateDto.getId())));
    }

    @Test
    void delete_whenNotFound() {
        // given
        Integer notExistingId = 100;

        // when
        Exception exception = Assertions.assertThrows(PlanNotFoundException.class,
                () -> planService.deleteById(notExistingId));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

}
