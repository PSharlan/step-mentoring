package itstep.mentoringservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MentoringServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MentoringServiceApplication.class, args);
	}

}
