package itstep.mentoringservice.dto.learningProcess;

import io.swagger.annotations.ApiModelProperty;
import itstep.mentoringservice.dto.mentor.MentorFullDto;
import itstep.mentoringservice.dto.plan.PlanFullDto;
import itstep.mentoringservice.dto.weeklyReport.WeeklyReportFullDto;
import itstep.mentoringservice.entity.enums.LearningProcessStatus;
import lombok.Data;

import java.time.Instant;
import java.util.List;

@Data
public class LearningProcessFullDto {

    @ApiModelProperty(example = "1", notes = "Id of the current learningProcess")
    private Integer id;

    @ApiModelProperty(notes = "Plan for learningProcess")
    private PlanFullDto plan;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    private Integer userId;

    @ApiModelProperty(notes = "List of weeklyReports for learningProcess")
    private List<WeeklyReportFullDto> weeklyReports;

    @ApiModelProperty(example = "PROCESS", notes = "Status of learningProcess")
    private LearningProcessStatus status;

    @ApiModelProperty(example = "2021-01-01T17:07:00.290Z", notes = "StartDate of learningProcess")
    private Instant startDate;
}
