package itstep.mentoringservice.dto.weeklyReport;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class StudentWeeklyReportCreateDto {

    @ApiModelProperty(example = "1", notes = "Id of the current weeklyReport")
    @NotNull(message = "weeklyReportId can't be null!")
    private Integer id; //Already exists in DB

    @ApiModelProperty(example = "5", notes = "MarkForMentor for weeklyReport" )
    @NotNull(message = "MarkForMentor can't be null!")
    private Integer markForMentor;

    @ApiModelProperty(notes = "CommentForMentor for weeklyReport")
    private String commentForMentor;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    @NotNull(message = "learningProcessId can't be null")
    private Integer learningProcessId;

}
