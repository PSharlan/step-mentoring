package itstep.mentoringservice.exception;

public class WrongNumbersOfPlanException extends RuntimeException{

    public WrongNumbersOfPlanException(String message) {
        super(message);
    }
}
