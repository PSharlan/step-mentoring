package itstep.mentoringservice.dto.weeklyReport;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class MentorWeeklyReportCreateDto {

    @ApiModelProperty(example = "1", notes = "Id of the current weeklyReport")
    @NotNull(message = "weeklyReportId can't be null!")
    private Integer id; //Already exists in DB

    @ApiModelProperty(example = "5", notes = "MarkForStudent for weeklyReport")
    @NotNull(message = "MarkForStudent can't be null!")
    private Integer markForStudent;

    @ApiModelProperty(notes = "CommentForStudent for weeklyReport")
    private String commentForStudent;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    @NotNull(message = "learningProcessId can't be null")
    private Integer learningProcessId;

}
