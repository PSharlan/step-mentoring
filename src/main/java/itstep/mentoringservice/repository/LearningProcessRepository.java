package itstep.mentoringservice.repository;

import itstep.mentoringservice.entity.LearningProcessEntity;
import itstep.mentoringservice.entity.enums.LearningProcessStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LearningProcessRepository extends JpaRepository<LearningProcessEntity, Integer> {

    LearningProcessEntity findOneById(Integer id);

    List<LearningProcessEntity> findAllByStatus(LearningProcessStatus learningProcessStatus);

}
