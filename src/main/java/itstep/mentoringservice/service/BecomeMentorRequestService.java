package itstep.mentoringservice.service;

import itstep.mentoringservice.dto.becomeMentorRequest.BecomeMentorRequestCreateDto;
import itstep.mentoringservice.dto.becomeMentorRequest.BecomeMentorRequestFullDto;
import itstep.mentoringservice.dto.becomeMentorRequest.BecomeMentorRequestUpdateDto;
import itstep.mentoringservice.entity.BecomeMentorRequestEntity;
import itstep.mentoringservice.entity.PlanEntity;
import itstep.mentoringservice.entity.enums.RequestStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BecomeMentorRequestService {

    BecomeMentorRequestFullDto findById(Integer id);

    Page<BecomeMentorRequestFullDto> findAll(Integer page, Integer size);

    List<BecomeMentorRequestFullDto> findAllByStatus(RequestStatus requestStatus);

    List<BecomeMentorRequestFullDto> findAllByPlanId(Integer planId);

    BecomeMentorRequestFullDto create(BecomeMentorRequestCreateDto createDto);

    BecomeMentorRequestFullDto update(BecomeMentorRequestUpdateDto updateDto);

    void deleteById(Integer id);

}
