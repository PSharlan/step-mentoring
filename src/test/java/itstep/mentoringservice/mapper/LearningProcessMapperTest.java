package itstep.mentoringservice.mapper;

import itstep.mentoringservice.dto.learningProcess.LearningProcessCreateDto;
import itstep.mentoringservice.dto.learningProcess.LearningProcessFullDto;
import itstep.mentoringservice.entity.LearningProcessEntity;
import itstep.mentoringservice.entity.WeeklyReportEntity;
import itstep.mentoringservice.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.Collections;

import static itstep.mentoringservice.mapper.LearningProcessMapper.*;
import static itstep.mentoringservice.mapper.MapperGenerationUtils.*;

public class LearningProcessMapperTest extends AbstractIntegrationTest {

    @Test
    void mapToEntity_happyPath() {
        // given
        LearningProcessCreateDto createDto = generateLearningProcessCreateDto();

        // when
        LearningProcessEntity entity = LEARNING_PROCESS_MAPPER.toEntity(createDto);

        // then
        Assertions.assertNull(entity.getId());
        Assertions.assertEquals(createDto.getUserId(), entity.getUserId());
    }

    @Test
    void mapToFullDto_happyPath() {
        // given
        LearningProcessEntity learningProcess = generateLearningProcessWithId();
        WeeklyReportEntity weeklyReport = generateWeeklyReportWithId();

        learningProcess.setPlan(generatePlanWithId());
        learningProcess.setMentor(generateMentorWithId());
        learningProcess.setWeeklyReports(Collections.singletonList(weeklyReport));

        // when
        LearningProcessFullDto learningProcessFullDto = LEARNING_PROCESS_MAPPER.toFullDto(learningProcess);

        // then
        Assertions.assertEquals(learningProcess.getId(), learningProcessFullDto.getId());
        Assertions.assertEquals(learningProcess.getUserId(), learningProcessFullDto.getUserId());
        Assertions.assertEquals(learningProcess.getPlan().getId(), learningProcessFullDto.getPlan().getId());
    }

}
