FROM openjdk:11

ADD target/*.jar /app_mentoring-service.jar

ENTRYPOINT ["java", "-jar", "app_mentoring-service.jar"]
