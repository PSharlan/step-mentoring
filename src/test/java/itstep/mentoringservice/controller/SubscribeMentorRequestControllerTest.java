package itstep.mentoringservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import itstep.mentoringservice.dto.becomeMentorRequest.BecomeMentorRequestFullDto;
import itstep.mentoringservice.dto.becomeMentorRequest.BecomeMentorRequestUpdateDto;
import itstep.mentoringservice.dto.subscribeMentorRequest.SubscribeMentorRequestCreateDto;
import itstep.mentoringservice.dto.subscribeMentorRequest.SubscribeMentorRequestFullDto;
import itstep.mentoringservice.dto.subscribeMentorRequest.SubscribeMentorRequestUpdateDto;
import itstep.mentoringservice.entity.BecomeMentorRequestEntity;
import itstep.mentoringservice.entity.MentorEntity;
import itstep.mentoringservice.entity.PlanEntity;
import itstep.mentoringservice.entity.SubscribeMentorRequestEntity;
import itstep.mentoringservice.entity.enums.RequestStatus;
import itstep.mentoringservice.integration.AbstractIntegrationTest;
import itstep.mentoringservice.repository.MentorRepository;
import itstep.mentoringservice.repository.PlanRepository;
import itstep.mentoringservice.repository.SubscribeMentorRequestRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static itstep.mentoringservice.EntityGenerationUtils.*;
import static itstep.mentoringservice.EntityGenerationUtils.generatePlan;
import static itstep.mentoringservice.entity.enums.RequestStatus.ACCEPTED;
import static itstep.mentoringservice.mapper.MapperGenerationUtils.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class SubscribeMentorRequestControllerTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() throws Exception {
        // given
        SubscribeMentorRequestEntity saved = subscribeMentorRequestRepository.save(generateSubscribeMentorRequestWithMentorAndPlan());

        // when
        MvcResult result = mockMvc.perform(get("/subscribeMentorRequests/{id}", saved.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        SubscribeMentorRequestFullDto foundDto = objectMapper.readValue(bytes, SubscribeMentorRequestFullDto.class);

        // then
        Assertions.assertNotNull(foundDto);
        Assertions.assertNotNull(foundDto.getId());
        Assertions.assertEquals(saved.getId(), foundDto.getId());
        Assertions.assertEquals(saved.getStatus(), foundDto.getStatus());
        Assertions.assertEquals(saved.getPlan().getId(), foundDto.getPlan().getId());
    }

    @Test
    void findById_whenNotFound() throws Exception {
        // given
        Integer notExistingId = 10000;

        // when
        mockMvc.perform(get("/subscribeMentorRequests/{id}", notExistingId))
                .andExpect(status().isNotFound());

        // then
    }

    @Test
    void findAll_happyPath() throws Exception {
        // given
        List<SubscribeMentorRequestEntity> saved = subscribeMentorRequestRepository.saveAll(generateSubscribeMentorRequestsWithMentorAndPlan());

        // when
        mockMvc.perform(get("/subscribeMentorRequests?page=0&size=10"))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    void findAllByStatus_happyPath() throws Exception {
        // given
        List<SubscribeMentorRequestEntity> toSave = generateSubscribeMentorRequestsWithMentorAndPlan();
        toSave.get(0).setStatus(ACCEPTED);

        List<SubscribeMentorRequestEntity> saved = subscribeMentorRequestRepository.saveAll(toSave);

        // when
        mockMvc.perform(get("/subscribeMentorRequests/status/ACCEPTED"))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    void create_happyPath() throws Exception {
        // given
        SubscribeMentorRequestCreateDto createDto = generateSubscribeMentorRequestCreateDtoWithMentorAndPlan();

        // when
        MvcResult result = mockMvc.perform(post("/subscribeMentorRequests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        SubscribeMentorRequestFullDto saved = objectMapper.readValue(bytes, SubscribeMentorRequestFullDto.class);

        // then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
        Assertions.assertEquals(saved.getPlan().getId(), createDto.getPlanId());
        Assertions.assertEquals(saved.getUserId(), createDto.getUserId());
    }

    @Test
    void update_happyPath() throws Exception {
        // given
        SubscribeMentorRequestEntity saved = subscribeMentorRequestRepository.save(generateSubscribeMentorRequestWithMentorAndPlan());

        SubscribeMentorRequestUpdateDto updateDto = generateSubscribeMentorRequestUpdateDto();
        updateDto.setId(saved.getId());

        // when
        MvcResult result = mockMvc.perform(put("/subscribeMentorRequests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        SubscribeMentorRequestFullDto updated = objectMapper.readValue(bytes, SubscribeMentorRequestFullDto.class);

        // then
        Assertions.assertNotNull(updated);
        Assertions.assertEquals(saved.getId(), updated.getId());
        Assertions.assertNotEquals(saved.getStatus(), updated.getStatus());
    }

    @Test
    void update_whenNotFound() throws Exception {
        // given
        BecomeMentorRequestUpdateDto updateDto = generateBecomeMentorRequestUpdateDto();
        updateDto.setId(0);

        // when
        mockMvc.perform(put("/subscribeMentorRequests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isNotFound());

        // then
    }

    @Test
    void delete_happyPath() throws Exception {
        // given
        SubscribeMentorRequestEntity saved = subscribeMentorRequestRepository.save(generateSubscribeMentorRequestWithMentorAndPlan());

        // when
        mockMvc.perform(delete("/subscribeMentorRequests/{id}", saved.getId()))
                .andExpect(status().isOk());

        // then
        mockMvc.perform(get("/subscribeMentorRequests/{id}", saved.getId()))
                .andExpect(status().isNotFound());
    }

    @Test
    void create_whenUserIdNull() throws Exception {
        // given
        SubscribeMentorRequestCreateDto createDto = generateSubscribeMentorRequestCreateDtoWithMentorAndPlan();
        createDto.setUserId(null);

        // when
        mockMvc.perform(post("/subscribeMentorRequests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    @Test
    void create_whenMentorIdNull() throws Exception {
        // given
        SubscribeMentorRequestCreateDto createDto = generateSubscribeMentorRequestCreateDtoWithMentorAndPlan();
        createDto.setMentorId(null);

        // when
        mockMvc.perform(post("/subscribeMentorRequests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    @Test
    void create_whenPlanIdNull() throws Exception {
        // given
        SubscribeMentorRequestCreateDto createDto = generateSubscribeMentorRequestCreateDtoWithMentorAndPlan();
        createDto.setPlanId(null);

        // when
        mockMvc.perform(post("/subscribeMentorRequests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    @Test
    void update_whenStatusNull() throws Exception {
        // given
        SubscribeMentorRequestEntity saved = subscribeMentorRequestRepository.save(generateSubscribeMentorRequestWithMentorAndPlan());

        SubscribeMentorRequestUpdateDto updateDto = generateSubscribeMentorRequestUpdateDto();
        updateDto.setId(saved.getId());
        updateDto.setStatus(null);

        // when
        mockMvc.perform(put("/subscribeMentorRequests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    private SubscribeMentorRequestEntity generateSubscribeMentorRequestWithMentorAndPlan() {
        SubscribeMentorRequestEntity subscribeMentorRequest = generateSubscribeMentorRequest();

        subscribeMentorRequest.setMentor(mentorRepository.save(generateMentor()));
        subscribeMentorRequest.setPlan(planRepository.save(generatePlan()));

        return subscribeMentorRequest;
    }

    private List<SubscribeMentorRequestEntity> generateSubscribeMentorRequestsWithMentorAndPlan() {
        List<SubscribeMentorRequestEntity> subscribeMentorRequests = generateSubscribeMentorRequests();

        MentorEntity savedMentor = mentorRepository.save(generateMentor());
        PlanEntity savedPlan = planRepository.save(generatePlan());

        for (SubscribeMentorRequestEntity subscribeMentorRequest : subscribeMentorRequests) {
            subscribeMentorRequest.setMentor(savedMentor);
            subscribeMentorRequest.setPlan(savedPlan);
        }

        return subscribeMentorRequests;
    }

    private SubscribeMentorRequestCreateDto generateSubscribeMentorRequestCreateDtoWithMentorAndPlan() {
        SubscribeMentorRequestCreateDto createDto = generateSubscribeMentorRequestCreateDto();
        createDto.setMentorId(mentorRepository.save(generateMentor()).getId());
        createDto.setPlanId(planRepository.save(generatePlan()).getId());

        return createDto;
    }
}
