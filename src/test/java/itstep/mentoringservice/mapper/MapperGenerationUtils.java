package itstep.mentoringservice.mapper;

import itstep.mentoringservice.dto.becomeMentorRequest.BecomeMentorRequestCreateDto;
import itstep.mentoringservice.dto.becomeMentorRequest.BecomeMentorRequestUpdateDto;
import itstep.mentoringservice.dto.learningProcess.LearningProcessCreateDto;
import itstep.mentoringservice.dto.learningProcess.LearningProcessUpdateDto;
import itstep.mentoringservice.dto.mentor.MentorCreateDto;
import itstep.mentoringservice.dto.plan.PlanCreateDto;
import itstep.mentoringservice.dto.plan.PlanUpdateDto;
import itstep.mentoringservice.dto.subscribeMentorRequest.SubscribeMentorRequestCreateDto;
import itstep.mentoringservice.dto.subscribeMentorRequest.SubscribeMentorRequestUpdateDto;
import itstep.mentoringservice.entity.*;
import itstep.mentoringservice.entity.enums.LearningProcessStatus;
import itstep.mentoringservice.entity.enums.RequestStatus;
import liquibase.pro.packaged.L;

import java.time.Instant;

public class MapperGenerationUtils {

    public static BecomeMentorRequestEntity generateBecomeMentorRequestWithId() {
        BecomeMentorRequestEntity entity = new BecomeMentorRequestEntity();
        entity.setId(1);
        entity.setUserId(2);
        entity.setStatus(RequestStatus.ACCEPTED);

        return entity;
    }

    public static MentorEntity generateMentorWithId() {
        MentorEntity entity = new MentorEntity();
        entity.setId(1);
        entity.setUserId(2);

        return entity;
    }

    public static PlanEntity generatePlanWithId() {
        PlanEntity entity = new PlanEntity();
        entity.setId(1);
        entity.setDescription("qwe");
        entity.setEstimatedHours(2);
        entity.setFileUrl("zxc");

        return entity;
    }

    public static BecomeMentorRequestCreateDto generateBecomeMentorRequestCreateDto() {
        BecomeMentorRequestCreateDto dto = new BecomeMentorRequestCreateDto();
        dto.setUserId(1);

        return dto;
    }

    public static LearningProcessEntity generateLearningProcessWithId() {
        LearningProcessEntity entity = new LearningProcessEntity();
        entity.setId(1);
        entity.setUserId(2);

        return entity;
    }

    public static WeeklyReportEntity generateWeeklyReportWithId() {
        WeeklyReportEntity entity = new WeeklyReportEntity();
        entity.setId(1);
        entity.setMarkForStudent(8);
        entity.setMarkForMentor(9);
        entity.setCommentForStudent("zxc");
        entity.setCommentForMentor("qwe");
        entity.setCreateTime(Instant.now());

        return entity;
    }

    public static LearningProcessCreateDto generateLearningProcessCreateDto() {
        LearningProcessCreateDto createDto = new LearningProcessCreateDto();
        createDto.setUserId(1);

        return createDto;
    }

    public static MentorCreateDto generateMentorCreateDto() {
        MentorCreateDto createDto = new MentorCreateDto();
        createDto.setUserId(1);

        return createDto;
    }

    public static PlanCreateDto generatePlanCreateDto() {
        PlanCreateDto createDto = new PlanCreateDto();
        createDto.setDescription("qwe");
        createDto.setEstimatedHours(12);
        createDto.setFileUrl("zxc");

        return createDto;
    }

    public static SubscribeMentorRequestEntity generateSubscribeMentorRequestWithId() {
        SubscribeMentorRequestEntity entity = new SubscribeMentorRequestEntity();

        entity.setId(1);
        entity.setStatus(RequestStatus.PENDING);
        entity.setUserId(2);

        return entity;
    }

    public static SubscribeMentorRequestCreateDto generateSubscribeMentorRequestCreateDto() {
        SubscribeMentorRequestCreateDto createDto = new SubscribeMentorRequestCreateDto();
        createDto.setUserId(1);

        return createDto;
    }

    public static BecomeMentorRequestUpdateDto generateBecomeMentorRequestUpdateDto() {
        BecomeMentorRequestUpdateDto updateDto = new BecomeMentorRequestUpdateDto();

        updateDto.setStatus(RequestStatus.ACCEPTED);
        return updateDto;
    }

    public static PlanUpdateDto generatePlanUpdateDto() {
        PlanUpdateDto updateDto = new PlanUpdateDto();

        updateDto.setDescription("qwerty");
        updateDto.setEstimatedHours(1);
        updateDto.setFileUrl("asd");
        return updateDto;
    }

    public static LearningProcessUpdateDto generateLearningProcessUpdateDto() {
        LearningProcessUpdateDto updateDto = new LearningProcessUpdateDto();

        updateDto.setStatus(LearningProcessStatus.FINISHED);
        return updateDto;
    }

    public static SubscribeMentorRequestUpdateDto generateSubscribeMentorRequestUpdateDto() {
        SubscribeMentorRequestUpdateDto updateDto = new SubscribeMentorRequestUpdateDto();

        updateDto.setStatus(RequestStatus.ACCEPTED);
        return updateDto;
    }


}
