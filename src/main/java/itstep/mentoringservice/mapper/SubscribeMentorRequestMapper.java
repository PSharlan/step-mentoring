package itstep.mentoringservice.mapper;

import itstep.mentoringservice.dto.subscribeMentorRequest.SubscribeMentorRequestCreateDto;
import itstep.mentoringservice.dto.subscribeMentorRequest.SubscribeMentorRequestFullDto;
import itstep.mentoringservice.entity.SubscribeMentorRequestEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface SubscribeMentorRequestMapper {

    SubscribeMentorRequestMapper SUBSCRIBE_MENTOR_REQUEST_MAPPER = Mappers.getMapper(SubscribeMentorRequestMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "mentor", ignore = true)
    @Mapping(target = "plan", ignore = true)
    SubscribeMentorRequestEntity toEntity(SubscribeMentorRequestCreateDto createDto);

    SubscribeMentorRequestFullDto toFullDto(SubscribeMentorRequestEntity entity);

    List<SubscribeMentorRequestFullDto> toFullDtoList(List<SubscribeMentorRequestEntity> entities);
}
