package itstep.mentoringservice.entity.enums;

public enum RequestStatus {

    PENDING,
    ACCEPTED,
    REJECTED

}
