package itstep.mentoringservice.mapper;

import itstep.mentoringservice.dto.becomeMentorRequest.BecomeMentorRequestCreateDto;
import itstep.mentoringservice.dto.becomeMentorRequest.BecomeMentorRequestFullDto;
import itstep.mentoringservice.dto.plan.PlanFullDto;
import itstep.mentoringservice.entity.BecomeMentorRequestEntity;
import itstep.mentoringservice.entity.PlanEntity;
import itstep.mentoringservice.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.Collections;

import static itstep.mentoringservice.mapper.BecomeMentorRequestMapper.BECOME_MENTOR_REQUEST_MAPPER;
import static itstep.mentoringservice.mapper.MapperGenerationUtils.*;

public class BecomeMentorRequestMapperTest extends AbstractIntegrationTest {

    @Test
    void mapToEntity_happyPath() {
        // given
        BecomeMentorRequestCreateDto createDto = generateBecomeMentorRequestCreateDto();

        // when
        BecomeMentorRequestEntity entity = BECOME_MENTOR_REQUEST_MAPPER.toEntity(createDto);

        // then
        Assertions.assertNull(entity.getId());
        Assertions.assertEquals(createDto.getUserId(), entity.getUserId());
        Assertions.assertNull(entity.getStatus());
    }

    @Test
    void mapToFullDto_happyPath() {
        // given
        BecomeMentorRequestEntity becomeMentorRequest = generateBecomeMentorRequestWithId();
        becomeMentorRequest.setPlans(Collections.singletonList(generatePlanWithId()));

        // when
        BecomeMentorRequestFullDto becomeMentorRequestFullDto = BECOME_MENTOR_REQUEST_MAPPER.toFullDto(becomeMentorRequest);

        // then
        Assertions.assertEquals(becomeMentorRequest.getId(), becomeMentorRequestFullDto.getId());
        Assertions.assertEquals(becomeMentorRequest.getStatus(), becomeMentorRequestFullDto.getStatus());
        Assertions.assertEquals(becomeMentorRequest.getUserId(), becomeMentorRequestFullDto.getUserId());
        Assertions.assertEquals(becomeMentorRequest.getPlans().size(), becomeMentorRequestFullDto.getPlans().size());

        for (int i = 0; i < becomeMentorRequest.getPlans().size(); i++) {
            PlanEntity entity = becomeMentorRequest.getPlans().get(i);
            PlanFullDto dto = becomeMentorRequestFullDto.getPlans().get(i);
            Assertions.assertEquals(entity.getId(), dto.getId());
            Assertions.assertEquals(entity.getDescription(), dto.getDescription());
            Assertions.assertEquals(entity.getEstimatedHours(), dto.getEstimatedHours());
            Assertions.assertEquals(entity.getFileUrl(), dto.getFileUrl());
        }

    }

}
