package itstep.mentoringservice.exception;

public class LastWeeklyReportNotFoundException extends RuntimeException{

    public LastWeeklyReportNotFoundException(String message) {
        super(message);
    }
}
