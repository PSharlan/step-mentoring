package itstep.mentoringservice.mapper;

import itstep.mentoringservice.dto.weeklyReport.MentorWeeklyReportCreateDto;
import itstep.mentoringservice.dto.weeklyReport.WeeklyReportFullDto;
import itstep.mentoringservice.entity.WeeklyReportEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface WeeklyReportMapper {

    WeeklyReportMapper WEEKLY_REPORT_MAPPER = Mappers.getMapper(WeeklyReportMapper.class);

    WeeklyReportFullDto toFullDto(WeeklyReportEntity weeklyReportEntity);

    List<WeeklyReportFullDto> toFullDtoList(List<WeeklyReportEntity> entities);

}
