package itstep.mentoringservice.exception;

public class MentorNotFoundException extends EntityIsNotFoundException{

    public MentorNotFoundException(Integer id) {
        super("Mentor ", id);
    }
}
