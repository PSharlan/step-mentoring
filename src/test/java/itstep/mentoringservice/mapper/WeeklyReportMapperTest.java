package itstep.mentoringservice.mapper;

import itstep.mentoringservice.dto.weeklyReport.WeeklyReportFullDto;
import itstep.mentoringservice.entity.WeeklyReportEntity;
import itstep.mentoringservice.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static itstep.mentoringservice.mapper.MapperGenerationUtils.generateLearningProcessWithId;
import static itstep.mentoringservice.mapper.MapperGenerationUtils.generateWeeklyReportWithId;
import static itstep.mentoringservice.mapper.WeeklyReportMapper.WEEKLY_REPORT_MAPPER;

public class WeeklyReportMapperTest extends AbstractIntegrationTest {

    @Test
    void mapToFullDto_happyPath() {
        // given
        WeeklyReportEntity entity = generateWeeklyReportWithId();
        entity.setLearningProcess(generateLearningProcessWithId());

        // when
        WeeklyReportFullDto fullDto = WEEKLY_REPORT_MAPPER.toFullDto(entity);

        // then
        Assertions.assertEquals(entity.getId(), fullDto.getId());
        Assertions.assertEquals(entity.getCreateTime(), fullDto.getCreateTime());
        Assertions.assertEquals(entity.getMarkForStudent(), fullDto.getMarkForStudent());
        Assertions.assertEquals(entity.getMarkForMentor(), fullDto.getMarkForMentor());
        Assertions.assertEquals(entity.getCommentForStudent(), fullDto.getCommentForStudent());
        Assertions.assertEquals(entity.getCommentForMentor(), fullDto.getCommentForMentor());
    }

}
