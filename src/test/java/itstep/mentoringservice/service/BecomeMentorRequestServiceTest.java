package itstep.mentoringservice.service;

import itstep.mentoringservice.EntityGenerationUtils;
import itstep.mentoringservice.dto.becomeMentorRequest.BecomeMentorRequestCreateDto;
import itstep.mentoringservice.dto.becomeMentorRequest.BecomeMentorRequestFullDto;
import itstep.mentoringservice.dto.becomeMentorRequest.BecomeMentorRequestUpdateDto;
import itstep.mentoringservice.entity.BecomeMentorRequestEntity;
import itstep.mentoringservice.entity.LearningProcessEntity;
import itstep.mentoringservice.entity.PlanEntity;
import itstep.mentoringservice.entity.enums.RequestStatus;
import itstep.mentoringservice.exception.BecomeMentorRequestNotFoundException;
import itstep.mentoringservice.exception.WrongNumbersOfPlanException;
import itstep.mentoringservice.integration.AbstractIntegrationTest;
import itstep.mentoringservice.mapper.MapperGenerationUtils;
import itstep.mentoringservice.repository.BecomeMentorRequestRepository;
import itstep.mentoringservice.repository.PlanRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

import static itstep.mentoringservice.EntityGenerationUtils.*;
import static itstep.mentoringservice.mapper.MapperGenerationUtils.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BecomeMentorRequestServiceTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() {
        // given
        BecomeMentorRequestEntity entity = generateBecomeMentorRequest();

        BecomeMentorRequestEntity saved = becomeMentorRequestRepository.save(entity);

        // when
        BecomeMentorRequestFullDto foundDto = becomeMentorRequestService.findById(saved.getId());

        // then
        Assertions.assertNotNull(foundDto);
        Assertions.assertEquals(saved.getId(), foundDto.getId());
        Assertions.assertEquals(saved.getStatus(), foundDto.getStatus());
        Assertions.assertEquals(saved.getUserId(), foundDto.getUserId());
    }

    @Test
    void findAll_happyPath() {
        // given
        List<BecomeMentorRequestEntity> toSave = generateBecomeMentorRequests();

        List<BecomeMentorRequestEntity> saved = becomeMentorRequestRepository.saveAll(toSave);

        // when
        Page<BecomeMentorRequestFullDto> foundPage = becomeMentorRequestService.findAll(0, 10);
        List<BecomeMentorRequestFullDto> becomeMentorRequests = foundPage.getContent();

        // then
        Assertions.assertNotNull(foundPage);
        Assertions.assertEquals(saved.size(), becomeMentorRequests.size());
    }

    @Test
    void findAllByStatus_happyPath() {
        // given
        List<BecomeMentorRequestEntity> toSave = generateBecomeMentorRequests();
        toSave.get(0).setStatus(RequestStatus.REJECTED);
        toSave.get(1).setStatus(RequestStatus.REJECTED);
        toSave.get(2).setStatus(RequestStatus.PENDING);

        List<BecomeMentorRequestEntity> saved = becomeMentorRequestRepository.saveAll(toSave);

        // when
        List<BecomeMentorRequestFullDto> foundDto = becomeMentorRequestService.findAllByStatus(RequestStatus.REJECTED);

        // then
        Assertions.assertNotNull(foundDto);
        Assertions.assertEquals(saved.get(0).getStatus(), foundDto.get(0).getStatus());
        Assertions.assertEquals(saved.get(1).getStatus(), foundDto.get(1).getStatus());
        Assertions.assertNotEquals(saved.size(), foundDto.size());
    }

    @Test
    void findAllByPlanId_happyPath() {
        // given
        List<BecomeMentorRequestEntity> toSave = generateBecomeMentorRequestWithPlans();

        List<BecomeMentorRequestEntity> saved = becomeMentorRequestRepository.saveAll(toSave);

        // when
        List<BecomeMentorRequestFullDto> foundDto = becomeMentorRequestService.findAllByPlanId(1);

        // then
        Assertions.assertNotNull(foundDto);
        Assertions.assertNotEquals(foundDto.size(), saved.size());
    }

    @Test
    void create_happyPath() {
        // given
        BecomeMentorRequestCreateDto createDto = generateBecomeMentorRequestDtoWithPlans();

        // when
        BecomeMentorRequestFullDto saved = becomeMentorRequestService.create(createDto);

        // then
        Assertions.assertNotNull(saved.getId());
        Assertions.assertNotNull(saved.getStatus());
        Assertions.assertNotNull(saved.getPlans());
        Assertions.assertEquals(saved.getStatus(), RequestStatus.PENDING);
        Assertions.assertEquals(saved.getUserId(), createDto.getUserId());
        Assertions.assertEquals(saved.getPlans().size(), createDto.getPlanIds().size());
    }

    @Test
    void update_happyPath() {
        // given
        BecomeMentorRequestCreateDto createDto = generateBecomeMentorRequestDtoWithPlans();

        BecomeMentorRequestFullDto saved = becomeMentorRequestService.create(createDto);

        BecomeMentorRequestUpdateDto updateDto = generateBecomeMentorRequestUpdateDto();
        updateDto.setId(saved.getId());

        // when
        BecomeMentorRequestFullDto updated = becomeMentorRequestService.update(updateDto);

        // then
        Assertions.assertNotNull(updated);
        Assertions.assertEquals(saved.getId(), updated.getId());
        Assertions.assertEquals(saved.getUserId(), updated.getUserId());
        Assertions.assertNotEquals(saved.getStatus(), updated.getStatus());
        Assertions.assertEquals(updated.getStatus(), RequestStatus.ACCEPTED);
    }

    @Test
    void deleteById_happyPath() {
        // given
        BecomeMentorRequestFullDto saved = becomeMentorRequestService.create(generateBecomeMentorRequestDtoWithPlans());

        // when
        becomeMentorRequestService.deleteById(saved.getId());

        Exception exception = assertThrows(BecomeMentorRequestNotFoundException.class,
                () -> becomeMentorRequestService.findById(saved.getId()));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(saved.getId())));
    }

    @Test
    void findById_whenNotFound() {
        // given
        Integer notExistingId = 100;

        // when
        Exception exception = assertThrows(BecomeMentorRequestNotFoundException.class,
                () -> becomeMentorRequestService.findById(notExistingId));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void create_whenWrongNumbersOfPlan() {
        // given
        BecomeMentorRequestCreateDto createDto = generateBecomeMentorRequestCreateDto();
        createDto.setPlanIds(List.of(1, 2, 3));

        // when
        Exception exception = Assertions.assertThrows(WrongNumbersOfPlanException.class,
                () -> becomeMentorRequestService.create(createDto));

        // then
        Assertions.assertTrue(exception.getMessage().contains("Wrong numbers of plan"));
    }

    @Test
    void update_whenNotFound() {
        // given
        BecomeMentorRequestUpdateDto updateDto = generateBecomeMentorRequestUpdateDto();

        Integer notExistingId = 100;
        updateDto.setId(notExistingId);

        // when
        Exception exception = Assertions.assertThrows(BecomeMentorRequestNotFoundException.class,
                () -> becomeMentorRequestService.update(updateDto));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(updateDto.getId())));
    }

    @Test
    void delete_whenNotFound() {
        // given
        Integer notExistingId = 100;

        // when
        Exception exception = Assertions.assertThrows(BecomeMentorRequestNotFoundException.class,
                () -> becomeMentorRequestService.deleteById(notExistingId));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    private BecomeMentorRequestCreateDto generateBecomeMentorRequestDtoWithPlans() {
        BecomeMentorRequestCreateDto createDto = generateBecomeMentorRequestCreateDto();

        List<PlanEntity> plans = planRepository.saveAll(generatePlans());

        for (PlanEntity entity : plans) {
            List<Integer> plansIds = new ArrayList<>();
            plansIds.add(entity.getId());
            createDto.setPlanIds(plansIds);
        }
        return createDto;
    }

    private List<BecomeMentorRequestEntity> generateBecomeMentorRequestWithPlans() {
        List<BecomeMentorRequestEntity> becomeMentorRequests = generateBecomeMentorRequests();

        List<PlanEntity> plans = planRepository.saveAll(generatePlans());

        for (BecomeMentorRequestEntity entity : becomeMentorRequests) {
            entity.setPlans(plans);
        }

        return becomeMentorRequests;
    }
}
