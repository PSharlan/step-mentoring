package itstep.mentoringservice.exception;

public class PlanNotFoundException extends EntityIsNotFoundException {

    public PlanNotFoundException(Integer id) {
        super("Plan ", id);
    }
}
