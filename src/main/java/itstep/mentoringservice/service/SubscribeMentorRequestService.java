package itstep.mentoringservice.service;

import itstep.mentoringservice.dto.subscribeMentorRequest.SubscribeMentorRequestCreateDto;
import itstep.mentoringservice.dto.subscribeMentorRequest.SubscribeMentorRequestFullDto;
import itstep.mentoringservice.dto.subscribeMentorRequest.SubscribeMentorRequestUpdateDto;
import itstep.mentoringservice.entity.enums.RequestStatus;
import org.springframework.data.domain.Page;

import java.util.List;

public interface SubscribeMentorRequestService {

    SubscribeMentorRequestFullDto findById(Integer id);

    Page<SubscribeMentorRequestFullDto> findAll(Integer page, Integer size);

    List<SubscribeMentorRequestFullDto> findAllByStatus(RequestStatus status);

    SubscribeMentorRequestFullDto create(SubscribeMentorRequestCreateDto createDto);

    SubscribeMentorRequestFullDto update(SubscribeMentorRequestUpdateDto updateDto);

    void deleteById(Integer id);
}
