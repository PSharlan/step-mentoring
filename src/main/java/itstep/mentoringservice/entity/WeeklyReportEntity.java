package itstep.mentoringservice.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.time.Instant;

@Data
@Entity
@Table(name = "weekly_report")
public class WeeklyReportEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "learning_process_id", nullable = false)
    private LearningProcessEntity learningProcess;

    @Min(1)
    @Max(10)
    @Column(name = "mark_for_student", nullable = false)
    private Integer markForStudent;

    @Min(1)
    @Max(10)
    @Column(name = "mark_for_mentor", nullable = false)
    private Integer markForMentor;

    @Column(name = "comment_for_student" )
    private String commentForStudent;

    @Column(name = "comment_for_mentor")
    private String commentForMentor;

    @Column(name = "create_time", nullable = false)
    private Instant createTime;

}
