package itstep.mentoringservice.dto.subscribeMentorRequest;

import io.swagger.annotations.ApiModelProperty;
import itstep.mentoringservice.entity.enums.RequestStatus;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SubscribeMentorRequestUpdateDto {

    @ApiModelProperty(example = "1", notes = "Id of the current subscribeMentorRequest")
    @NotNull(message = "subscribeMentorRequestId can't be null!")
    private Integer id;

    @ApiModelProperty(example = "ACCEPTED", notes = "Status of subscribeMentorRequest")
    @NotNull(message = "Status can't be null!")
    private RequestStatus status;

    @ApiModelProperty(example = "1", notes = "MentorId for subscribeMentorRequest")
    private Integer mentorId;

}
