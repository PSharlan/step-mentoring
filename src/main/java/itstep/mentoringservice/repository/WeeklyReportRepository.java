package itstep.mentoringservice.repository;

import itstep.mentoringservice.entity.WeeklyReportEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WeeklyReportRepository extends JpaRepository<WeeklyReportEntity, Integer> {

    WeeklyReportEntity findOneById(Integer id);

}
