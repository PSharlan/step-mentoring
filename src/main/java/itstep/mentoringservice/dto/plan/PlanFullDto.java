package itstep.mentoringservice.dto.plan;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PlanFullDto {

    @ApiModelProperty(example = "1", notes = "Id of the current plan")
    private Integer id;

    @ApiModelProperty(notes = "Description for plan")
    private String description;

    @ApiModelProperty(example = "1", notes = "EstimatedHours for plan")
    private Integer estimatedHours;

    @ApiModelProperty(example = "http://file", notes = "Link to a file with a detailed plan")
    private String fileUrl;

}