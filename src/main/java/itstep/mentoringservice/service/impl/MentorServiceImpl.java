package itstep.mentoringservice.service.impl;

import itstep.mentoringservice.dto.mentor.MentorCreateDto;
import itstep.mentoringservice.dto.mentor.MentorFullDto;
import itstep.mentoringservice.entity.MentorEntity;
import itstep.mentoringservice.entity.PlanEntity;
import itstep.mentoringservice.exception.MentorNotFoundException;
import itstep.mentoringservice.exception.WrongNumbersOfPlanException;
import itstep.mentoringservice.repository.MentorRepository;
import itstep.mentoringservice.repository.PlanRepository;
import itstep.mentoringservice.service.MentorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static itstep.mentoringservice.mapper.MentorMapper.*;

@Slf4j
@Service
public class MentorServiceImpl implements MentorService {

    @Autowired
    private MentorRepository mentorRepository;

    @Autowired
    private PlanRepository planRepository;

    @Override
    @Transactional(readOnly = true)
    public MentorFullDto findById(Integer id) {
        MentorEntity foundEntity = mentorRepository.findById(id)
                .orElseThrow(() -> new MentorNotFoundException(id));

        log.info("MentorServiceImpl -> found mentor {} by id: {}", foundEntity, id);
        return MENTOR_MAPPER.toFullDto(foundEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<MentorFullDto> findAll(Integer page, Integer size) {
        Page<MentorEntity> foundEntities = mentorRepository.findAll(PageRequest.of(page, size));

        Page<MentorFullDto> dto = foundEntities.map(entity -> MENTOR_MAPPER.toFullDto(entity));

        log.info("MentorServiceImpl -> found {} mentors", dto.getNumberOfElements());
        return dto;
    }

    @Override
    @Transactional
    public MentorFullDto create(MentorCreateDto createDto) {
        MentorEntity entityToSave = MENTOR_MAPPER.toEntity(createDto);

        List<PlanEntity> plans = planRepository.findAllById(createDto.getPlanIds());
        validateRequestedPlansExist(createDto);

        entityToSave.setPlans(plans);

        MentorEntity savedEntity = mentorRepository.save(entityToSave);

        log.info("MentorServiceImpl -> mentor {} successfully saved", savedEntity);
        return MENTOR_MAPPER.toFullDto(savedEntity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        mentorRepository.findById(id)
                .orElseThrow(() -> new MentorNotFoundException(id));

        mentorRepository.deleteById(id);
    }


    private void validateRequestedPlansExist(MentorCreateDto createDto) {
        List<PlanEntity> existingPlans = planRepository.findAllById(createDto.getPlanIds());

        if (existingPlans.size() != createDto.getPlanIds().size()) {
            throw new WrongNumbersOfPlanException("Wrong numbers of plans, mentors plans: " + createDto.getPlanIds().size() + " found plans: " + existingPlans.size());
        }
    }
}
