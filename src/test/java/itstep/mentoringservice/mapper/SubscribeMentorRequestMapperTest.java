package itstep.mentoringservice.mapper;

import itstep.mentoringservice.dto.subscribeMentorRequest.SubscribeMentorRequestCreateDto;
import itstep.mentoringservice.dto.subscribeMentorRequest.SubscribeMentorRequestFullDto;
import itstep.mentoringservice.entity.SubscribeMentorRequestEntity;
import itstep.mentoringservice.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static itstep.mentoringservice.mapper.MapperGenerationUtils.*;
import static itstep.mentoringservice.mapper.SubscribeMentorRequestMapper.SUBSCRIBE_MENTOR_REQUEST_MAPPER;

public class SubscribeMentorRequestMapperTest extends AbstractIntegrationTest {

    @Test
    void mapToEntity_happyPath() {
        // given
        SubscribeMentorRequestCreateDto createDto = generateSubscribeMentorRequestCreateDto();

        // when
        SubscribeMentorRequestEntity entity = SUBSCRIBE_MENTOR_REQUEST_MAPPER.toEntity(createDto);

        // then
        Assertions.assertNull(entity.getId());
        Assertions.assertEquals(createDto.getUserId(), entity.getUserId());
    }

    @Test
    void mapToFullDto_happyPath() {
        // given
        SubscribeMentorRequestEntity entity = generateSubscribeMentorRequestWithId();
        entity.setMentor(generateMentorWithId());
        entity.setPlan(generatePlanWithId());

        // when
        SubscribeMentorRequestFullDto fullDto = SUBSCRIBE_MENTOR_REQUEST_MAPPER.toFullDto(entity);

        // then
        Assertions.assertEquals(entity.getId(), fullDto.getId());
        Assertions.assertEquals(entity.getStatus(), fullDto.getStatus());
        Assertions.assertEquals(entity.getUserId(), fullDto.getUserId());
        Assertions.assertEquals(entity.getPlan().getId(), fullDto.getPlan().getId());
    }

}
