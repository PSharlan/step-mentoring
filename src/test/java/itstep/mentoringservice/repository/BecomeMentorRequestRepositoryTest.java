package itstep.mentoringservice.repository;

import itstep.mentoringservice.entity.BecomeMentorRequestEntity;
import itstep.mentoringservice.entity.PlanEntity;
import itstep.mentoringservice.entity.enums.RequestStatus;
import itstep.mentoringservice.integration.AbstractIntegrationTest;
import itstep.mentoringservice.repository.BecomeMentorRequestRepository;
import itstep.mentoringservice.repository.PlanRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.Period;
import java.util.List;

import static itstep.mentoringservice.EntityGenerationUtils.*;

public class BecomeMentorRequestRepositoryTest extends AbstractIntegrationTest {

    @Test
    void findAllByStatus_happyPath() {
        // given
        List<BecomeMentorRequestEntity> becomeMentorRequests = generateBecomeMentorRequests();
        List<PlanEntity> plans = generatePlans();

        for (BecomeMentorRequestEntity entity :
                becomeMentorRequests) {
            entity.setPlans(plans);
        }

        List<PlanEntity> savedPlans = planRepository.saveAll(plans);
        List<BecomeMentorRequestEntity> saved = becomeMentorRequestRepository.saveAll(becomeMentorRequests);

        // when
        List<BecomeMentorRequestEntity> found = becomeMentorRequestRepository.findAllByStatus(RequestStatus.ACCEPTED);

        // then
        for (BecomeMentorRequestEntity entity :
                found) {
            Assertions.assertEquals(entity.getStatus(), RequestStatus.ACCEPTED);
        }
    }

    @Test
    void findAll_happyPath() {
        // given
        List<BecomeMentorRequestEntity> becomeMentorRequests = generateBecomeMentorRequests();

        List<BecomeMentorRequestEntity> saved = becomeMentorRequestRepository.saveAll(becomeMentorRequests);

        // when
        List<BecomeMentorRequestEntity> found = becomeMentorRequestRepository.findAll();

        // then
        Assertions.assertEquals(found.size(), 10);
    }

    //TODO сделать проверку на то, что планы подгружаются
    @Test
    void findOneById_happyPath() {
        // given
        BecomeMentorRequestEntity becomeMentorRequest = generateBecomeMentorRequest();

        BecomeMentorRequestEntity saved = becomeMentorRequestRepository.save(becomeMentorRequest);

        // when
        BecomeMentorRequestEntity found = becomeMentorRequestRepository.findOneById(saved.getId());

        // then
        Assertions.assertNotNull(found);
        Assertions.assertEquals(saved.getId(), found.getId());
    }

    @Test
    void update_happyPath(){
        //given
        BecomeMentorRequestEntity becomeMentorRequest = generateBecomeMentorRequest();
        BecomeMentorRequestEntity savedRequest = becomeMentorRequestRepository.save(becomeMentorRequest);

        //when
        savedRequest.setStatus(RequestStatus.ACCEPTED);
        BecomeMentorRequestEntity updatedRequest = becomeMentorRequestRepository.save(savedRequest);

        //then
        Assertions.assertEquals(RequestStatus.ACCEPTED, updatedRequest.getStatus());
        Assertions.assertEquals(savedRequest.getId(), updatedRequest.getId());
    }

    @Test
    void save_happyPath() {
        // given
        BecomeMentorRequestEntity becomeMentorRequest = generateBecomeMentorRequest();
        List<PlanEntity> plans = generatePlans();

        becomeMentorRequest.setPlans(plans);

        List<PlanEntity> savedPlans = planRepository.saveAll(plans);

        // when
        BecomeMentorRequestEntity saved = becomeMentorRequestRepository.save(becomeMentorRequest);

        // then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
    }


    @Test
    void delete_happyPath_planWillNotBeDeleted() {
        // given
        BecomeMentorRequestEntity becomeMentorRequest = generateBecomeMentorRequest();
        List<PlanEntity> plans = generatePlans();

        becomeMentorRequest.setPlans(plans);

        List<PlanEntity> savedPlans = planRepository.saveAll(plans);
        BecomeMentorRequestEntity saved = becomeMentorRequestRepository.save(becomeMentorRequest);

        // when
        becomeMentorRequestRepository.deleteById(saved.getId());

        // then
        Assertions.assertNull(becomeMentorRequestRepository.findOneById(saved.getId()));
        for (PlanEntity plan:
                savedPlans) {
            Assertions.assertNotNull(planRepository.findOneById(plan.getId()));
        }
    }

    @Test
    void findAllByPlan_happyPath(){
        //given
        List<BecomeMentorRequestEntity> requests = generateBecomeMentorRequests();
        List<PlanEntity> plans = generatePlans();

        int countOfRequestsWithPlans = requests.size();

        List<BecomeMentorRequestEntity> savedRequests = becomeMentorRequestRepository.saveAll(requests);

        for(PlanEntity plan : plans){
            plan.setBecomeMentorRequests(savedRequests);
        }
        requests.add(generateBecomeMentorRequest());

        List<PlanEntity> savedPlans = planRepository.saveAll(plans);

        //when
        List<BecomeMentorRequestEntity> foundRequests = becomeMentorRequestRepository.findAllByPlan(plans.get(0).getId());

        //then
        Assertions.assertEquals(countOfRequestsWithPlans, foundRequests.size());
    }

}
