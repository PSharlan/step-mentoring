package itstep.mentoringservice.service;

import itstep.mentoringservice.dto.weeklyReport.MentorWeeklyReportCreateDto;
import itstep.mentoringservice.dto.weeklyReport.StudentWeeklyReportCreateDto;
import itstep.mentoringservice.dto.weeklyReport.WeeklyReportFullDto;
import org.springframework.data.domain.Page;

import java.util.List;

public interface WeeklyReportService {

    WeeklyReportFullDto findById(Integer id);

    Page<WeeklyReportFullDto> findAll(Integer page, Integer size);

    WeeklyReportFullDto create(MentorWeeklyReportCreateDto createDto);

    WeeklyReportFullDto create(StudentWeeklyReportCreateDto createDto);

    void deleteById(Integer id);

    void generateInitialReportsForActiveProcess();

}
