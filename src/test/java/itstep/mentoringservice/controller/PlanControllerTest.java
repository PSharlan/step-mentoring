package itstep.mentoringservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import itstep.mentoringservice.EntityGenerationUtils;
import itstep.mentoringservice.dto.plan.PlanCreateDto;
import itstep.mentoringservice.dto.plan.PlanFullDto;
import itstep.mentoringservice.dto.plan.PlanUpdateDto;
import itstep.mentoringservice.entity.PlanEntity;
import itstep.mentoringservice.integration.AbstractIntegrationTest;
import itstep.mentoringservice.mapper.MapperGenerationUtils;
import itstep.mentoringservice.repository.PlanRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static itstep.mentoringservice.EntityGenerationUtils.*;
import static itstep.mentoringservice.mapper.MapperGenerationUtils.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class PlanControllerTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() throws Exception {
        // given
        PlanEntity saved = planRepository.save(generatePlan());

        // when
        MvcResult result = mockMvc.perform(get("/plans/{id}", saved.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        PlanFullDto foundDto = objectMapper.readValue(bytes, PlanFullDto.class);

        // then
        Assertions.assertNotNull(foundDto);
        Assertions.assertNotNull(foundDto.getId());
        Assertions.assertEquals(saved.getId(), foundDto.getId());
        Assertions.assertEquals(saved.getDescription(), foundDto.getDescription());
        Assertions.assertEquals(saved.getFileUrl(), foundDto.getFileUrl());
        Assertions.assertEquals(saved.getEstimatedHours(), foundDto.getEstimatedHours());
    }

    @Test
    void findById_whenNotFound() throws Exception {
        // given
        Integer notExistingId = 10000;

        // when
        mockMvc.perform(get("/plans/{id}", notExistingId))
                .andExpect(status().isNotFound());

        // then
    }

    @Test
    void findAll_happyPath() throws Exception {
        // given
        List<PlanEntity> saved = planRepository.saveAll(generatePlans());

        // when
        mockMvc.perform(get("/plans?page=0&size=3"))
                .andExpect(status().isOk())
                .andReturn();

        // then
    }

    @Test
    void create_happyPath() throws Exception {
        // given
        PlanCreateDto createDto = generatePlanCreateDto();

        // when
        MvcResult result = mockMvc.perform(post("/plans")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        PlanFullDto saved = objectMapper.readValue(bytes, PlanFullDto.class);

        // then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
        Assertions.assertEquals(saved.getEstimatedHours(), createDto.getEstimatedHours());
        Assertions.assertEquals(saved.getDescription(), createDto.getDescription());
        Assertions.assertEquals(saved.getFileUrl(), createDto.getFileUrl());
    }

    @Test
    void update_happyPath() throws Exception {
        // given
        PlanEntity saved = planRepository.save(generatePlan());

        PlanUpdateDto update = generatePlanUpdateDto();
        update.setId(saved.getId());

        // when
        MvcResult result = mockMvc.perform(put("/plans")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(update)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        PlanFullDto updated = objectMapper.readValue(bytes, PlanFullDto.class);

        // then
        Assertions.assertNotNull(updated);
        Assertions.assertEquals(saved.getId(), updated.getId());
        Assertions.assertNotEquals(saved.getDescription(), updated.getDescription());
        Assertions.assertNotEquals(saved.getFileUrl(), updated.getFileUrl());
    }

    @Test
    void update_whenNotFound() throws Exception {
        // given
        PlanUpdateDto updateDto = generatePlanUpdateDto();
        updateDto.setId(0);

        // when
        mockMvc.perform(put("/plans")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isNotFound());

        // then
    }

    @Test
    void delete_happyPath() throws Exception {
        // given
        PlanEntity saved = planRepository.save(generatePlan());

        // when
        mockMvc.perform(delete("/plans/{id}", saved.getId()))
                .andExpect(status().isOk());

        // then
        mockMvc.perform(get("/plans/{id}", saved.getId()))
                .andExpect(status().isNotFound());
    }

    @Test
    void delete_whenNotFound() throws Exception {
        // given
        Integer notExistingId = 0;

        // when
        mockMvc.perform(delete("/plans/{id}", notExistingId))
                .andExpect(status().isNotFound());

        // then
    }

    @Test
    void create_whenDescriptionEmpty() throws Exception {
        // given
        PlanCreateDto createDto = generatePlanCreateDto();
        createDto.setDescription("");

        // when
        mockMvc.perform(post("/plans")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    @Test
    void create_whenEstimatedHoursNull() throws Exception {
        // given
        PlanCreateDto createDto = generatePlanCreateDto();
        createDto.setEstimatedHours(null);

        // when
        mockMvc.perform(post("/plans")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    @Test
    void create_whenFileUrlEmpty() throws Exception {
        // given
        PlanCreateDto createDto = generatePlanCreateDto();
        createDto.setFileUrl("");

        // when
        mockMvc.perform(post("/plans")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    @Test
    void update_whenDescriptionEmpty() throws Exception {
        // given
        PlanEntity saved = planRepository.save(generatePlan());

        PlanUpdateDto updateDto = generatePlanUpdateDto();
        updateDto.setId(saved.getId());
        updateDto.setDescription("");

        // when
        mockMvc.perform(put("/plans")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    @Test
    void update_whenEstimatedHoursNull() throws Exception {
        // given
        PlanEntity saved = planRepository.save(generatePlan());

        PlanUpdateDto updateDto = generatePlanUpdateDto();
        updateDto.setId(saved.getId());
        updateDto.setEstimatedHours(null);

        // when
        mockMvc.perform(put("/plans")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isBadRequest());

        // then
    }

    @Test
    void update_whenFileUrlEmpty() throws Exception {
        // given
        PlanEntity saved = planRepository.save(generatePlan());

        PlanUpdateDto updateDto = generatePlanUpdateDto();
        updateDto.setId(saved.getId());
        updateDto.setFileUrl("");

        // when
        mockMvc.perform(put("/plans")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isBadRequest());

        // then
    }
}
