package itstep.mentoringservice.service;

import itstep.mentoringservice.EntityGenerationUtils;
import itstep.mentoringservice.dto.mentor.MentorCreateDto;
import itstep.mentoringservice.dto.mentor.MentorFullDto;
import itstep.mentoringservice.entity.MentorEntity;
import itstep.mentoringservice.entity.PlanEntity;
import itstep.mentoringservice.exception.MentorNotFoundException;
import itstep.mentoringservice.exception.WrongNumbersOfPlanException;
import itstep.mentoringservice.integration.AbstractIntegrationTest;
import itstep.mentoringservice.mapper.MapperGenerationUtils;
import itstep.mentoringservice.repository.MentorRepository;
import itstep.mentoringservice.repository.PlanRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

import static itstep.mentoringservice.EntityGenerationUtils.*;
import static itstep.mentoringservice.mapper.MapperGenerationUtils.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MentorServiceTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() {
        // given
        MentorEntity entity = generateMentor();

        MentorEntity saved = mentorRepository.save(entity);

        // when
        MentorFullDto foundDto = mentorService.findById(saved.getId());

        // then
        Assertions.assertNotNull(foundDto);
        Assertions.assertEquals(saved.getId(), foundDto.getId());
    }

    @Test
    void findAll_happyPath() {
        // given
        List<MentorEntity> entities = generateMentors();

        List<MentorEntity> saved = mentorRepository.saveAll(entities);

        // when
        Page<MentorFullDto> foundPage = mentorService.findAll(0, 3);
        List<MentorFullDto> mentors = foundPage.getContent();

        // then
        Assertions.assertNotNull(foundPage);
        Assertions.assertEquals(saved.size(), mentors.size());
    }

    @Test
    void create_happyPath() {
        // given
        MentorCreateDto createDto = generateMentorCreateDtoWithPlans();

        // when
        MentorFullDto saved = mentorService.create(createDto);

        // then
        Assertions.assertNotNull(saved.getId());
        Assertions.assertNotNull(saved.getPlans());
        Assertions.assertEquals(saved.getPlans().get(0).getId(), createDto.getPlanIds().get(0));
    }

    @Test
    void deleteById_happyPath() {
        // given
        MentorFullDto saved = mentorService.create(generateMentorCreateDtoWithPlans());

        // when
        mentorService.deleteById(saved.getId());

        Exception exception = assertThrows(MentorNotFoundException.class,
                () -> mentorService.findById(saved.getId()));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(saved.getId())));
    }

    @Test
    void findById_whenNotFound() {
        // given
        Integer notExistingId = 100;

        // when
        Exception exception = assertThrows(MentorNotFoundException.class,
                () -> mentorService.findById(notExistingId));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void create_whenWrongNumbersOfPlan() {
        // given
        MentorCreateDto createDto = generateMentorCreateDto();
        createDto.setPlanIds(List.of(1, 2, 3));

        // when
        Exception exception = Assertions.assertThrows(WrongNumbersOfPlanException.class,
                () -> mentorService.create(createDto));

        // then
        Assertions.assertTrue(exception.getMessage().contains("Wrong numbers of plan"));
    }

    @Test
    void delete_whenNotFound() {
        // given
        Integer notExistingId = 100;

        // when
        Exception exception = Assertions.assertThrows(MentorNotFoundException.class,
                () -> mentorService.deleteById(notExistingId));

        // then
        Assertions.assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }




    private MentorCreateDto generateMentorCreateDtoWithPlans() {
        MentorCreateDto createDto = generateMentorCreateDto();

        List<PlanEntity> plans = planRepository.saveAll(generatePlans());

        for (PlanEntity entity : plans) {
            List<Integer> plansIds = new ArrayList<>();
            plansIds.add(entity.getId());
            createDto.setPlanIds(plansIds);
        }
        return createDto;
    }


}
