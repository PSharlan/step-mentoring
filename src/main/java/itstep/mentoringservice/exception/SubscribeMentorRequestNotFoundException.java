package itstep.mentoringservice.exception;

public class SubscribeMentorRequestNotFoundException extends EntityIsNotFoundException{

    public SubscribeMentorRequestNotFoundException(Integer id) {
        super("SubscribeMentorRequest ", id);
    }
}
