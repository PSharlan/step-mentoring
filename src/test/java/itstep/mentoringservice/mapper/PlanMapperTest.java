package itstep.mentoringservice.mapper;

import itstep.mentoringservice.dto.plan.PlanCreateDto;
import itstep.mentoringservice.dto.plan.PlanFullDto;
import itstep.mentoringservice.entity.PlanEntity;
import itstep.mentoringservice.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static itstep.mentoringservice.mapper.MapperGenerationUtils.generatePlanCreateDto;
import static itstep.mentoringservice.mapper.MapperGenerationUtils.generatePlanWithId;
import static itstep.mentoringservice.mapper.PlanMapper.PLAN_MAPPER;

public class PlanMapperTest extends AbstractIntegrationTest {

    @Test
    void mapToEntity_happyPath() {
        // given
        PlanCreateDto createDto = generatePlanCreateDto();

        // when
        PlanEntity entity = PLAN_MAPPER.toEntity(createDto);

        // then
        Assertions.assertNull(entity.getId());
        Assertions.assertEquals(createDto.getDescription(), entity.getDescription());
        Assertions.assertEquals(createDto.getEstimatedHours(), entity.getEstimatedHours());
        Assertions.assertEquals(createDto.getFileUrl(), entity.getFileUrl());
    }

    @Test
    void mapToFullDto_happyPath() {
        // given
        PlanEntity plan = generatePlanWithId();

        // when
        PlanFullDto planFullDto = PLAN_MAPPER.toFullDto(plan);

        // then
        Assertions.assertEquals(plan.getId(), planFullDto.getId());
        Assertions.assertEquals(plan.getFileUrl(), plan.getFileUrl());
        Assertions.assertEquals(plan.getEstimatedHours(), planFullDto.getEstimatedHours());
        Assertions.assertEquals(plan.getDescription(), planFullDto.getDescription());
    }

}
