package itstep.mentoringservice.dto.weeklyReport;

import io.swagger.annotations.ApiModelProperty;
import itstep.mentoringservice.dto.learningProcess.LearningProcessFullDto;
import lombok.Data;
import java.time.Instant;

@Data
public class WeeklyReportFullDto {

    @ApiModelProperty(example = "1", notes = "Id of the current weeklyReport")
    private Integer id;

    @ApiModelProperty(example = "5", notes = "MarkForStudent for weeklyReport")
    private Integer markForStudent;

    @ApiModelProperty(example = "5", notes = "MarkForMentor for weeklyReport" )
    private Integer markForMentor;

    @ApiModelProperty(notes = "CommentForStudent for weeklyReport")
    private String commentForStudent;

    @ApiModelProperty(notes = "CommentForMentor for weeklyReport")
    private String commentForMentor;

    @ApiModelProperty(example = "2021-01-01T17:07:00.290Z", notes = "Create time of weeklyReport")
    private Instant createTime;

}
