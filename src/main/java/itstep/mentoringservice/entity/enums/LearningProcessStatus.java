package itstep.mentoringservice.entity.enums;

public enum LearningProcessStatus {

    PROCESS,
    FINISHED

}
